<?php $this->load->view("_include/header_inner"); ?>

    <div class="innerContent">

        <div class="container">
            <div class="row">
                <h2><span class="triangle"><img src="images/symbol_triangle.png" alt=""></span>About us</h2>

                <p>
                    The latest in the retail business has not yet been said.
                    Internet commerce did not produce what was expected of it in lowering prices.
                    Despite the possibility of a meeting of consumer manufacturers that reduces
                    the costs of distribution and marketing. The reason is that most of the sales
                    are made in the old marketing track, where the large marketing and distribution
                    expenses do not allow a price reduction.
                </p>
            </div>
        </div>
    </div>

<?php $this->load->view("_include/footer"); ?>
