<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>driver_assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>driver_assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>driver_assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/validationEngine.jquery.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
	<script src="<?php echo base_url(); ?>driver_assets/js/jquery-2.2.4.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.validationEngine-en.js"></script>
	<script src="<?php echo base_url(); ?>driver_assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>js/jscolor.js"></script>
	<script src="<?php echo base_url(); ?>driver_assets/js/touchSwipe.min.js"></script>

  <script type="text/javascript">
      $(function() {
          $(window).scroll(function() {

              if ($(this).scrollTop() > 200) {
                  if (Number($(window).width()) > 1200) {
                      $('#drop-nav').slideDown('1500');
                      $('#drop-nav').css('display', 'block');
                  } else {
                      $('#drop-nav').css('display', 'none');
                  }
              } else {
                  $('#drop-nav').slideUp('1500');
              }
          });
      });
  </script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
