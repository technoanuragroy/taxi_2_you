<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/g3_club_front_style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/thumbnail-slider.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/fontawesome-all.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/owl.theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/owl.transitions.css">


        <script src="<?php echo base_url(); ?>js/jquery-3.3.1.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>js/thumbnail-slider.js"></script>
        <script src="<?php echo base_url(); ?>js/jscolor.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>

        <title><?php echo SITE_NAME; ?></title>

        <script type="text/javascript">

            $(document).ready(function(){
                $("#testimonial-slider").owlCarousel({
                    items:1,
                    itemsDesktop:[1000,1],
                    itemsDesktopSmall:[979,1],
                    itemsTablet:[768,1],
                    pagination:true,
                    navigation:true,
                    slideSpeed:1000,
                    singleItem:true,
                    transitionStyle:"goDown",
                    navigationText:["",""],
                    autoPlay:true
                });
            });

            $(function() {
                $(window).scroll(function() {

                    if ($(this).scrollTop() > 200) {

                        if (Number($(window).width()) > 1200) {
                            $('#drop-nav').slideDown('1500');
                            $('#drop-nav').css('display', 'block');
                        } else {

                            $('#drop-nav').css('display', 'none');
                        }

                    } else {

                        $('#drop-nav').slideUp('1500');
                    }
                });
            });

            $(document).ready(function() {

                var hash_value = window.location.hash;

                if(hash_value == '#contactus'){
                    $('html,body').animate({scrollTop: ($("#contact").offset().top - 60)}, 1000);
                }

                $(".contact-us-link").click(function(event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: ($(this.hash).offset().top - 80)}, 1000);
                });

                // Show cart items count on page load
                var cart_items = "<?php echo $this->my_custom_functions->get_total_cart_items(); ?>";
                if(cart_items != 0) {
                    $(".cart_items_number").text("("+cart_items+")");
                }
            });
        </script>
    </head>

    <body>
        <div id="drop-nav">
            <div class="drop-nav-container">
                <div class="header">
                    <nav class="navbar navbar-expand-xl navbar-light">

                        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/old-logo.png" alt=""/></a>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo04" aria-controls="navbarTogglerDemo04" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarTogglerDemo04">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo base_url(); ?>main/about">About</a>
                                </li>
                                <?php if ($this->session->userdata('club_member_id')) {/////// club member menu ?>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url(); ?>club_member/user/club_member_dashboard">My Account</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="javascript:;">My Group</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url(); ?>shopping/cart">My Cart<span class="cart_items_number"></span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url(); ?>club_member/main/club_member_logout">Logout</a>
                                        </li>
                                <?php } elseif($this->session->userdata('group_admin_id')) {///////  group admin menu ?>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url(); ?>group_admin/user/group_admin_dashboard">My Account</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url(); ?>group_admin/main/group_admin_logout">Logout</a>
                                        </li>
                                <?php } else { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url(); ?>club_member/main">Joining the club</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url(); ?>club_member/main/club_member_login">Login to Club members</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url(); ?>group_admin/main/group_admin_login">Login to Group admins</a>
                                        </li>
                                <?php } ?>
                            </ul>

                            <div class="my-2 my-lg-0">
                                <?php if ($this->session->userdata('club_member_id')) { ?>
                                          <a class="login_btn" href="<?php echo base_url(); ?>shopping/dashboard">Go to gig shopping</a>
                                <?php } else { ?>
                                          <a class="login_btn" href="<?php echo base_url(); ?>shopping/login">Login to gig shopping</a>
                                <?php } ?>
                            </div>

                            <span class="social_links_container">
                                <span class="social_links"><a href="#"><i class="fab fa-facebook-f"></i></a></span>
                                <span class="social_links"><a href="#"><i class="fab fa-twitter"></i></a></span>
                                <span class="social_links"><a href="#"><i class="fab fa-google-plus-g"></i></a></span>
                                <span class="social_links"><a href="#"><i class="fab fa-youtube"></i></a></span>
                            </span>
                        </div>
                    </nav>
                </div>
            </div>
        </div>

        <div class="header">
            <nav class="navbar navbar-expand-xl navbar-light">

                <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/logo.png" alt=""/></a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo base_url(); ?>main/about">About</a>
                        </li>
                        <?php if ($this->session->userdata('club_member_id')) { //////  club member menu?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url(); ?>club_member/user/club_member_dashboard">My Account</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:;">My Group</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url(); ?>shopping/cart">My Cart<span class="cart_items_number"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url(); ?>club_member/main/club_member_logout">Logout</a>
                                </li>
                        <?php } elseif($this->session->userdata('group_admin_id')) {///////  group member menu ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url(); ?>group_admin/user/group_admin_dashboard">My Account</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url(); ?>group_admin/main/group_admin_logout">Logout</a>
                                </li>
                        <?php } else { ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url(); ?>club_member/main">Joining the club</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url(); ?>club_member/main/club_member_login">Login to Club members</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url(); ?>group_admin/main/group_admin_login">Login to Group admins</a>
                                </li>
                        <?php } ?>
                    </ul>

                    <div class="my-2 my-lg-0">
                        <?php if ($this->session->userdata('club_member_id')) { ?>
                                <a class="login_btn" href="<?php echo base_url(); ?>shopping/dashboard">Go to gig shopping</a>
                        <?php } else { ?>
                                <a class="login_btn" href="<?php echo base_url(); ?>shopping/login">Login to gig shopping</a>
                        <?php } ?>
                    </div>

                    <span class="social_links_container">
                        <span class="social_links"><a href="#"><i class="fab fa-facebook-f"></i></a></span>
                        <span class="social_links"><a href="#"><i class="fab fa-twitter"></i></a></span>
                        <span class="social_links"><a href="#"><i class="fab fa-google-plus-g"></i></a></span>
                        <span class="social_links"><a href="#"><i class="fab fa-youtube"></i></a></span>
                    </span>
                </div>
            </nav>
        </div>
