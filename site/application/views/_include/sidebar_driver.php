<aside class="side_slide">
  <ul class="nav_side_link">
    <li><i class="fa fa-home"></i><a href="<?php echo base_url(); ?>driver/user/dashboard/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>">Dashboard</a></li>
    <li><i class="fa fa-clock-o"></i><a href="<?php echo base_url(); ?>driver/user/pendingTrips/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>">Pending trips</a></li>
    <li><i class="fa fa-tasks"></i><a href="<?php echo base_url(); ?>driver/user/completedTrips/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>">Completed trips</a></li>
    <li><i class="fa fa-ban"></i><a href="<?php echo base_url(); ?>driver/user/rejectedTrips/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>">Rejected trips</a>
      <!--<ul class="subMenu">
        <li><a href="#">sub link 01</a></li>
        <li><a href="#">sub link 02</a></li>
        <li><a href="#">sub link 03</a></li>
      </ul>-->
	</li> 
    <li>
      <?php if ($this->session->userdata('driver_id')) {?>
      <i class="fa fa-share"></i><a class="nav-link" href="<?php echo base_url(); ?>driver/main/driver_logout/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>">Logout</a>
      <?php }?>
    </li>
  </ul>
</aside>
