
                </div> <!-- End of <div id="content"> -->
            </div> <!-- End of <div class="wrapper1"> -->
        </div> <!-- End of <div class="innerContent"> -->

        <div class="overlay"></div>

        <div class="footer">
            <div class="footer_top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-md-4 col-lg-4 col-xl-4 footer_logo_details">
                            <span><img src="<?php echo base_url(); ?>images/old-logo.png" alt=""/></span>
                            <p>
                                The system is designed to serve the members/Group
                                administrators/Sellers and service providers - of the Go
                                Gig Go consumers club, as well as the Back office
                                operations and control.
                            </p>
                        </div>

                        <div class="col-md-8 col-lg-5 col-xl-5 footer_menu_details">
                            <h6>Navigation</h6>
                            <div class="under_line3"></div>
                            <div class="footer_nav_container">
                                <div class="footer_nav">
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                        <li><a href="<?php echo base_url(); ?>main/about">About Us</a></li>
                                        <li><a href="<?php echo base_url(); ?>club_member/main">Joining the club</a></li>
                                        <li><a href="<?php echo base_url(); ?>shopping/login">Login to gig shopping</a></li>
                                    </ul>
                                </div>

                                <div class="footer_nav">
                                   <ul>
                                        <li><a href="<?php echo base_url(); ?>club_member/main/club_member_login">Login to Club members </a></li>
                                        <li><a href="<?php echo base_url(); ?>group_admin/main/group_admin_login">Login to Group admins </a></li>

                                        <?php if($this->uri->segment(1) == "") { ?>
                                                <li><a href="<?php echo base_url(); ?>#contact" class="contact-us-link">Contact </a></li>
                                        <?php } else { ?>
                                                <li><a href="<?php echo base_url(); ?>#contactus" class="contact-us-link">Contact </a></li>
                                        <?php } ?>
                                   </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-3 col-xl-3 address_div">
                            <h6>Head Office</h6>
                            <span class="under_line3"></span>
                            <div class="address_row">
                                <span class="text_addrs">
                                    The GoGigGo Club Sevices<br>
                                    Shderot Yerushalayim<br>
                                    68021 Tel Aviv - Yafo<br>
                                    ISRAEL
                                </span>
                            </div>
                            <div class="web_row">
                                <span class="text_addrs">www.gogiggoclub.com</span>
                            </div>
                            <div class="mail_row">
                                <span class="text_addrs">info@gogiggoclub..com</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer_bottom">
                <div class="container">
                    <div class="row">
                        <h6>Get in Touch</h6>
                        <span class="footer-social-links"><a href="#" class="icon-facebook"></a></span>
                        <span class="footer-social-links"><a href="#" class="icon-twitter"></a></span>
                        <span class="footer-social-links"><a href="#" class="icon-google"></a></span>
                        <span class="footer-social-links"><a href="#" class="icon-youtube"></a></span>
                    </div>
                </div>
            </div>

            <div class="copy_text">© <?php echo date('Y'); ?> Go gig go club, All Right Reserved / Webdesign By Ablion</div>
        </div>

        <script src="<?php echo base_url(); ?>js/lightslider.js"></script>

                <script type="text/javascript">
                $('#lightSlider').lightSlider({
                 gallery: true,
                 item: 1,
                 loop:true,
                 slideMargin: 0,
                 thumbItem: 4,
                 pause: 2500,
                 speed: 800,
                 auto: true
             });
    </script>
    </body>
</html>
