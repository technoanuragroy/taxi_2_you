<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>driver_assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>driver_assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>driver_assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/validationEngine.jquery.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
	<script src="<?php echo base_url(); ?>driver_assets/js/jquery-2.2.4.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.validationEngine.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.validationEngine-en.js"></script>
	<script src="<?php echo base_url(); ?>driver_assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>js/jscolor.js"></script>
	<script src="<?php echo base_url(); ?>driver_assets/js/touchSwipe.min.js"></script>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

  <script type="text/javascript">
      var driver = '<?php echo $this->session->userdata('driver_id'); ?>';
			var key = '<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>';
      //alert(driver)
      $(document).ready(function () {

                var data = {
					 'driver_id' : driver,
				 };

				 var reqst = setInterval(function() { //alert();
					 $.ajax({
							type: "POST",
							url: "<?php echo base_url();?>driver/user/getDriverRequest/"+key,
							data: data,
							success: function(msg){
                                //alert(msg);
                                if(msg != "NA"){
								    $('.main_body').html(msg);
                                    if(request_available == 0) {
                                        call_request_display_timer();
                                    }
                                }else{
                                    //alert(msg);
                                    //clearTimeout(reqst);
                                }

							}
					});
				}, 5000);


                 $.fn.get_notification_driver = function(){

				 var data = {
					 'driver_id' : driver,
				 };
				 var reqst = setTimeout(function() {
					 $.ajax({
							type: "POST",
							url: "<?php echo base_url();?>driver/user/getDriverRequest/"+key,
							data: data,
							success: function(msg){
								//alert(msg)
                                if(msg != ""){
								$('.main_body').html(msg);
                                }
							    //alert(msg);
							}
					});
				}, 5000);

                }

        /*******switch*********/
        $(".onoffswitch-checkbox").on("click",function (e){
        var stat = $('.driver_status_hidden').val();
        //alert(stat)
        if(stat == 0){
        $('#map').show();
        initMap();
        $.fn.get_notification_driver();
        }else{
        $('#map').hide();
        clearTimeout(reqst);
        }
        });


            setTimeout(function() {
                $('.s_message').hide('slow');
            }, 4000);

            setTimeout(function() {
                $('.e_message').hide('slow');
            }, 4000);



        ///////////////ACCEPT REQUEST///////////////

        $(document).on("click",".address_bar",function (e){
        var rides_id = $(this).data('ridesid');
        //alert(rides_id);
        var data = {
                     'rides_id' : rides_id,
				    };
        $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>driver/user/accept_rider_request/"+key,
                    data: data,
                    success: function(msg){
                        //alert(msg);
                        var trip_urls = '<?php echo base_url();?>driver/user/customer_pickup/'+driver+'/'+rides_id+'/'+key;
                        window.location.href = trip_urls;
                    }
            });

        });


      });/////JQUERY END



        var timeLeft = 0;
        var timerId;
        var request_available = 0;

        function call_request_display_timer() {
            request_available = 1;
            timeLeft = 60;
            timerId = setInterval(countdown, 1000);
        }

        function countdown() {
            if (timeLeft == 0) {
                clearInterval(timerId);
                update_request_status($('#rides_id').val());
                request_available = 0;
                //$('#rides_id').val('');
            } else {
                //elem.innerHTML = timeLeft + ' seconds remaining';
                $('#timeleft_'+$('#rides_id').val()).text(timeLeft);
                timeLeft--;
                console.log(timeLeft);
            }
        }

        function update_request_status(rides_id){
            //alert(rides_id);
            var data = {
					 'driver_id' : driver,
                     'rides_id' : rides_id,
				    };
            $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>driver/user/driver_reject_request_status_update/"+key,
                    data: data,
                    success: function(msg){
                        //alert(msg);
                        //$('.main_body').html(msg);
                        if(msg !='notexist'){
                            window.location.reload();
                        }else{
                            $('.main_body').html($('.main_body').html());
                            //return false;
                        }
                    }
            });
        }
  </script>
	<style>
	    /* Always set the map height explicitly to define the size of the div
	     * element that contains the map. */
	    #map {
	      height: 100%;
	    }
	    /* Optional: Makes the sample page fill the window. */
	    html, body {
	      height: 100%;
	      margin: 0;
	      padding: 0;
	    }
	    .controls {
	      margin-top: 10px;
	      border: 1px solid transparent;
	      border-radius: 2px 0 0 2px;
	      box-sizing: border-box;
	      -moz-box-sizing: border-box;
	      height: 32px;
	      outline: none;
	      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
	    }

	    #origin-input,
	    #destination-input {
	      background-color: #fff;
	      font-family: Roboto;
	      font-size: 15px;
	      font-weight: 300;
	      margin-left: 12px;
	      padding: 0 11px 0 13px;
	      text-overflow: ellipsis;
	      width: 200px;
	    }

	    #origin-input:focus,
	    #destination-input:focus {
	      border-color: #4d90fe;
	    }

	    #mode-selector {
	      color: #fff;
	      background-color: #4d90fe;
	      margin-left: 12px;
	      padding: 5px 11px 0px 11px;
	    }

	    #mode-selector label {
	      font-family: Roboto;
	      font-size: 13px;
	      font-weight: 300;
	    }

	  </style>
</head>
