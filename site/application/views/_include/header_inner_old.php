<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/g3_club_front_style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/validationEngine.jquery.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/fontawesome-all.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/lightslider.css">


        <script src="<?php echo base_url(); ?>js/jquery-3.3.1.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.validationEngine.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.validationEngine-en.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jscolor.js"></script>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.js" type="text/javascript"></script>


        <title><?php echo SITE_NAME; ?> | Rider </title>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $(".fancybox").fancybox();
            });
			
            setTimeout(function() {
                $('.s_message').hide('slow');
            }, 4000);

            setTimeout(function() {
                $('.e_message').hide('slow');
            }, 4000);
        </script>
    </head>

    <body>
