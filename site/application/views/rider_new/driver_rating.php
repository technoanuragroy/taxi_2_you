<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/star-rating.css">
	<script src="<?php echo base_url(); ?>rider_assets/js/jquery-2.2.4.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/touchSwipe.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/star-rating.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/star-rating_main.js"></script>
	<script>
		$(document).ready(function(){
		  $('.nav_side_link li:has(".subMenu")').append("<i class='fa fa-chevron-right'></i>");
		  $(".side_bar_btn i").click(function(){
		    $(this).toggleClass('fa-bars fa-times');
			$("aside").toggleClass("active_aside");
			//$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
		  });
		  $(".nav_side_link li").click(function(){
		    $(this).find(".subMenu").slideToggle(300);
		    $(this).find("i").toggleClass("rotateIcon");
		  });
		  /***********swipe function**************/
		 $('body').append("<div class='swipe_to_open'></div>");
		 $('body').append("<div class='swipe_to_close'></div>");
         $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="right") {
                           $("aside").addClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                           return false;
                      }
                      if (phase=="move" && direction =="left") {
                           $("aside").removeClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                           return false;
                      }
                  }
          });

		});
	</script>

</head>

<body>
	<div class="cpmpleat_wrapper rider_wrap">
		<header>
										<section class="header_profile_inner">
												<span class="side_bar_btn">
																<i class="fa fa-bars"></i>
												</span>
										</section>

								</header>
				<?php $this->load->view("rider_new/include/rider_side_bar"); ?>
		<div class="drp_02_image">

			<?php
			$rides_id = $this->uri->segment(4);
			$driver_id =  $details['driver_id'];
			$first_name = $this->my_custom_functions->get_particular_field_value("tbl_driver", "first_name", " and id='".$driver_id."'");
			$last_name = $this->my_custom_functions->get_particular_field_value("tbl_driver", "last_name", " and id='".$driver_id."'");
			?>
			<?php
			$filename="uploads/driver/idproof/".$driver_id.".jpg";
			if (file_exists($filename)){ ?>
			<span><img src="<?php echo base_url(); ?>uploads/driver/idproof/<?php echo $driver_id;?>.jpg"></span>
			<?php }else{ ?>
			<span><img src="<?php echo base_url(); ?>uploads/driver/idproof/default_image.jpg"></span>
			<?php }?>

			<name><?php echo $first_name;?> <?php echo $last_name;?><name>
        </div>

		<div class="rate_star">
                <form action="<?php echo base_url();?>rider/user/driver_rating/<?php echo $rides_id;?>/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>" method="POST" enctype="multipart/form-data">
                    <input id="input-21b" name="rating" value="0" type="text" class="rating" data-min=0 data-max=5 data-step=1 data-size="lg" required title="">
                <a href="javascript:">Thank you note</a>
            </div>
            <!-- <div class="submit_content">

              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>


              </div> -->
							<input type="text" class="submit_content" name="rides_comment" placeholder="Review Of The Driver">
							<input type="hidden" name="driver_id" value="<?php echo $driver_id;?>">

              <input type="Submit" name="submit" class="drp_03_submit" value="submit Rating" />
            </form>

	</div>
</body>
</html>
