<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/responsive.css">
	<script src="<?php echo base_url(); ?>rider_assets/js/jquery-2.2.4.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/touchSwipe.min.js"></script>
	<script>
		$(document).ready(function(){
		  $('.nav_side_link li:has(".subMenu")').append("<i class='fa fa-chevron-right'></i>");
		  $(".side_bar_btn i").click(function(){
		    $(this).toggleClass('fa-bars fa-times');
			$("aside").toggleClass("active_aside");
			//$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
		  });
		  $(".nav_side_link li").click(function(){
		    $(this).find(".subMenu").slideToggle(300);
		    $(this).find("i").toggleClass("rotateIcon");
		  });
		  /***********swipe function**************/
		 $('body').append("<div class='swipe_to_open'></div>");
		 $('body').append("<div class='swipe_to_close'></div>");
         $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="right") {
                           $("aside").addClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                           return false;
                      }
                      if (phase=="move" && direction =="left") {
                           $("aside").removeClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                           return false;
                      }
                  }
          });
		  /*******switch*********/
		  $("#click_to_chenge").click( function (){
			if($(".chenge_text").hasClass("changeText_fild")){
			  $(".chenge_text").text("go offline");
			}
			else{
			  $(".chenge_text").text("go offline");
			}
			$("#click_to_chenge b").toggleClass("changeText_fild");
		  });

		});
	</script>
</head>

<body>
	<div class="cpmpleat_wrapper rider_wrap">
            <header>
                    <section class="header_profile_inner">
                        <span class="side_bar_btn">
                                <i class="fa fa-bars"></i>
                        </span>
                    </section>

                </header>
				<?php $this->load->view("rider_new/include/rider_side_bar"); ?>

        <div class="record_section_wrapper">
                <h2>Completed Request</h2>
        <div class="dashboard_inner">



					<?php
					foreach ($details as $value) {
						?>
           <div class="request_content">
               <div class="request_group">
                   <ul>
                       <li><a href=""><b>FROM</b><br>
												 <?php $pickup_adress=$value['pickup_adress'];?>
													<?php echo substr($pickup_adress,0,50);?></a>
                    </li>
                       <li class="circle"><a href="<?php echo base_url();?>rider/user/trip_confirmation/<?php echo $value['rides_id'];?>/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>"><i class="fa fa-long-arrow-right"></i></a></li>
                       <li class="group_rgt"><a href=""><b class="to_rgt">To</b><br>
												 		<?php $drop_address=$value['drop_address'];?>
                             <?php echo substr($drop_address,0,50);?></a></li>
                   </ul>

               </div>
               <div class="request_group1">
               <ul>
                    <li><a href=""><b>DATE</b><br></a>
                        <ul class="group1_inner">
                            <li><a href=""> 05:04:25 AM </a> </li>
                            <li><a href=""> 26th July 2019</a></li>
                        </ul>
                 </li>
                    <li class="group1_rgt"><a href=""><b>CUSTOMER NAME</b><br>
                         B.C Road Burdwan</a></li>
                </ul>
            </div>
           </div>
					 <?php
					 }?>

        </div>
        </div>

	</div>
</body>
</html>
