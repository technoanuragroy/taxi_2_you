<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/responsive.css">
	<script src="<?php echo base_url(); ?>rider_assets/js/jquery-2.2.4.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/touchSwipe.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/star-rating.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/star-rating_main.js"></script>
	<script>
		$(document).ready(function(){
		  $('.nav_side_link li').append("<i class='fa fa-chevron-right'></i>");
		  $(".side_bar_btn i").click(function(){
		    $(this).toggleClass('fa-bars fa-times');
			$("aside").toggleClass("active_aside");
			//$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
		  });
		  /***********swipe function**************/
		 $('body').append("<div class='swipe_to_open'></div>");
		 $('body').append("<div class='swipe_to_close'></div>");
         $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="right") {
                           $("aside").addClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                           return false;
                      }
                      if (phase=="move" && direction =="left") {
                           $("aside").removeClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                           return false;
                      }
                  }
          });

		});
	</script>

</head>
<style>
		/* Always set the map height explicitly to define the size of the div
		 * element that contains the map. */
		#map {
			height: 100%;
		}
		/* Optional: Makes the sample page fill the window. */
		html, body {
			height: 100%;
			margin: 0;
			padding: 0;
		}
		.controls {
			margin-top: 10px;
			border: 1px solid transparent;
			border-radius: 2px 0 0 2px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			height: 32px;
			outline: none;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		}

		#origin-input,
		#destination-input {
			background-color: #fff;
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			margin-left: 12px;
			padding: 0 11px 0 13px;
			text-overflow: ellipsis;
			width: 200px;
		}

		#origin-input:focus,
		#destination-input:focus {
			border-color: #4d90fe;
		}

		#mode-selector {
			color: #fff;
			background-color: #4d90fe;
			margin-left: 12px;
			padding: 5px 11px 0px 11px;
		}

		#mode-selector label {
			font-family: Roboto;
			font-size: 13px;
			font-weight: 300;
		}

	</style>

<body>
	<div class="cpmpleat_wrapper rider_wrap">
	<header>
                    <section class="header_profile_inner">
                        <span class="side_bar_btn">
                                <i class="fa fa-bars"></i>
                        </span>
                    </section>
    </header>
				<?php $this->load->view("rider_new/include/rider_side_bar"); ?>
		<div class="sticky_header">
			<p>
				<span>Automated Manufacturing Ltd.</span>
				<span>123 Herzl Street, 4th floor, Shaviv Quarter,46000</span>
			</p>
		</div>
		<div class="top_map_wrap">
			<div id="map"></div>
		</div>
		<div class="customer_pickup">
			<p class="arriving_time">
				<b>Arriving in - 6 mins approx</b>
            </p>

						<!-- <div class="rate_star">
							   <form action="<?php echo base_url();?>rider/user/driver_rating/<?php echo $rides_id;?>/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>" method="POST" enctype="multipart/form-data">
							   <input id="input-21b" name="rating" value="0" type="text" class="rating" data-min=0 data-max=5 data-step=1 data-size="lg" required title="">

								 </form>
					 </div> -->
			<div id="appnd_driver_detailss">
			<div style="text-align:center;margin-top:80px;width:100%;float:left;"><img src="<?php echo base_url(); ?>images/loader1.gif"></div>

			</div>




		</div>
	</div>
</body>
<script>
	// Note: This example requires that you consent to location sharing when
	// prompted by your browser. If you see the error "The Geolocation service
	// failed.", it means you probably did not give permission for the browser to
	var map, infoWindow, marker;
	var markerArray = [];
	var stepDisplay;
	var styles = [
			{
				stylers: [
					{ hue: "#00ffe6" },
					{ saturation: -20 }
				]
			},{
				featureType: "road",
				elementType: "geometry",
				stylers: [
					{ lightness: 100 },
					{ visibility: "simplified" }
				]
			},{
				featureType: "road",
				elementType: "labels",
				stylers: [
					{ visibility: "on" }
				]
			}
		];

	function initMap() {
		////Set From Address On Page Load////
		//GetAddress(31.0461,34.8516);

		//var directionsDisplay = new google.maps.DirectionsRenderer;
		var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: false});
		var directionsService = new google.maps.DirectionsService;

		map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: 31.0461, lng: 34.8516},
			zoom: 10,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: styles
		});

		infoWindow = new google.maps.InfoWindow();
		var iconBase = '<?php echo base_url();?>images/marker.png';
		marker = new google.maps.Marker({
		map: map,
		draggable: false,
		icon: iconBase,
		//animation: google.maps.Animation.DROP,
		position: {lat: 31.0461, lng: 34.8516}
		});
		//marker.addListener('click', toggleBounce);

		//////Geeolocation Tracking Code Starts here///////////
	 if (navigator.geolocation) {
		 navigator.geolocation.getCurrentPosition(function(position) {

			 ////If Geolocation Found Set Current Address From geo location////
			 GetAddress(position.coords.latitude,position.coords.longitude);

			 var pos = {
				 lat: position.coords.latitude,
				 lng: position.coords.longitude
			 };
			 infoWindow.setPosition(pos);
			 infoWindow.setContent('Location found.');
			 infoWindow.open(map);
			 map.setCenter(pos);
		 }, function() {
			 handleLocationError(true, infoWindow, map.getCenter());
		 });
		 } else {
			 // Browser doesn't support Geolocation
			 handleLocationError(false, infoWindow, map.getCenter());
		 }
		 //////Geeolocation Tracking Code Ends here///////////

		 ///////////Set Map Using Direction/////////////////
		 directionsDisplay.setMap(map);
		 directionsDisplay.setOptions({
			polylineOptions: {
									strokeWeight: 5,
									strokeOpacity: 1,
									strokeColor:  'black'
							}
			});

		/////////////////////AUTOCOMPLETE///////////////////////
		var originInput = document.getElementById('origin-input');
		var destinationInput = document.getElementById('destination-input');
		var from_places = new google.maps.places.Autocomplete(originInput);
		var to_places = new google.maps.places.Autocomplete(destinationInput);


		google.maps.event.addListener(marker, 'dragend', function () {
			//map.setCenter(this.getPosition()); // Set map center to marker position
			//GetAddress(this.getPosition().lat(),this.getPosition().lng());
			//updatePosition(this.getPosition().lat(), this.getPosition().lng()); // update position display
	});

		google.maps.event.addListener(map, 'dragend', function () {
				//marker.setPosition(this.getCenter()); // set marker position to map center
				//GetAddress(this.getCenter().lat(),this.getCenter().lng());
		});
		/////////////AUTOCOMPLETE PLACE CHANGE FROM ADDRESS//////////////////////
		google.maps.event.addListener(from_places, 'place_changed', function () {
				var from_place = from_places.getPlace();
				var from_address = from_place.formatted_address;
				var lat = from_place.geometry.location.lat();
				var lng = from_place.geometry.location.lng();
				//alert(lat);

				$('#frm_latitude').val(lat);
				$('#frm_longitude').val(lng);

				$('#origin').val(from_address);
				var origin = $('#origin').val();
				var destination = $('#destination').val();
				if(destination != ''){
				/////////CALCULATE DISTANCE///////////////////////////
				calculateDistance(origin,destination);
				calculateAndDisplayRoute(directionsService, directionsDisplay);
				}
		});

		/////////////AUTOCOMPLETE PLACE CHANGE TO ADDRESS//////////////////////
		google.maps.event.addListener(to_places, 'place_changed', function () {
				var to_place = to_places.getPlace();
				var to_address = to_place.formatted_address;

				var lat = to_place.geometry.location.lat();
				var lng = to_place.geometry.location.lng();

				$('#to_latitude').val(lat);
				$('#to_longitude').val(lng);

				$('#destination').val(to_address);
				var origin = $('#origin').val();
				var destination = $('#destination').val();
				if(origin !='' && destination != ''){
				/////////CALCULATE DISTANCE///////////////////////////
				calculateDistance(origin,destination);
				/////////CALCULATE ROUTE/////////////////////////////
				calculateAndDisplayRoute(directionsService, directionsDisplay);
			 }
			});


			///////////Save place Click function////////////
			$('.set_places').click(function(){
				var places = $(this).data('places');
				//alert(places)
				$('#origin-input').val(places);
				//$("#origin-input").focus();
				$('#origin').val(places);
				var start = $('#origin').val();
				var end = $('#destination').val();
				if(start != "" && end !=""){
				calculateDistance(start,end);
				calculateAndDisplayRoute(directionsService, directionsDisplay);
				}else{
				//alert()
				}
			});
	///////////Save place Click function////////////////////////

	////////////////End Init Function(Map)/////////////////////
	}

	///////////////GET ADDRESS FROM LAT LNG///////////////////
	function GetAddress(lat,lng) {
	 //marker.setMap(null);
	 var latlng = new google.maps.LatLng(lat, lng);
	 var geocoder = new google.maps.Geocoder();
	 geocoder.geocode({ 'latLng': latlng }, function (results, status) {
			 if (status == google.maps.GeocoderStatus.OK) {
					 if (results[1]) {
						 $('#origin-input').val(results[1].formatted_address);
						 $('#origin').val(results[1].formatted_address);
					 }
			 }
	 });
	}

	///////////Error Handler for Geolocation///////////////////
	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	 infoWindow.setPosition(pos);
	 infoWindow.setContent(browserHasGeolocation ?
												 'Error: The Geolocation service failed.' :
												 'Error: Your browser doesn\'t support geolocation.');
	 infoWindow.open(map);
 }
 ///////////Error Handler for Geolocation///////////////////

 /////////Gps Update if Current Location Changed//////////////
	 function autoUpdate() {
		 navigator.geolocation.getCurrentPosition(function(position) {
		 var newPoint = new google.maps.LatLng(position.coords.latitude,
																				position.coords.longitude);
		 if (marker) {
				// Marker already created - Move it
				marker.setPosition(newPoint);
			}
			else {
				// Marker does not exist - Create it
				marker = new google.maps.Marker({
					position: newPoint,
					map: map
				});
			}
			// Center the map on the new position
			map.setCenter(newPoint);
		});

	}
	// Call the autoUpdate() function every 5 seconds
	//setTimeout(autoUpdate, 5000);
	//setTimeout(function(){autoUpdate();}, 2000);


	function toggleBounce() {
	 if (marker.getAnimation() !== null) {
		 marker.setAnimation(null);
	 } else {
		 marker.setAnimation(google.maps.Animation.BOUNCE);
	 }
 }

	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
		infoWindow.setPosition(pos);
		infoWindow.setContent(browserHasGeolocation ?
			'Error: The Geolocation service failed.' :
			'Error: Your browser doesn\'t support geolocation.');
		infoWindow.open(map);
	}

	function calculateAndDisplayRoute(directionsService, directionsDisplay) {
	// for (i = 0; i < markerArray.length; i++) {
	//     markerArray[i].setMap(null);
	//   }

	//////////////FROM ICON & TWO ICON////////////////////////
	 var icons = {
	 start: new google.maps.MarkerImage(
		 'http://server1/webdev/taxi2u/site/images/location_starts.png',
		 new google.maps.Size(44, 32),
		 new google.maps.Point(0, 0),
		 new google.maps.Point(22, 32)),
	 end: new google.maps.MarkerImage(
		 'http://server1/webdev/taxi2u/site/images/location_ends.png',
		 new google.maps.Size(44, 32),
		 new google.maps.Point(0, 0),
		 new google.maps.Point(22, 32))
 };
 //////////////FROM ICON & TWO ICON////////////////////////

	 var start = $('#origin').val();
	 var end = $('#destination').val();
	 directionsService.route({
		 origin: start,
		 destination: end,
		 travelMode: google.maps.DirectionsTravelMode.DRIVING
	 }, function(response, status) {
		 if (status === 'OK') {

			 ////////TO CHANGE START & END MARKER ICON////////////
			 var leg = response.routes[0].legs[0];
			 //alert(leg.end_location)
			 marker.setMap(null);
			 //makeMarker( leg.start_location, icons.start, "Start", map);
			 //makeMarker( leg.end_location, icons.end, 'End', map );
			 directionsDisplay.setDirections(response);

		 } else {
			 window.alert('Directions request failed due to ' + status);
		 }
	 });
 }

 ////////TO CHANGE START & END MARKER ICON FUNCTION////////////
 function makeMarker(position, icon, title, map) {
		 marker = new google.maps.Marker({
				 position: position,
				 map: map,
				 icon: icon,
				 title: title
		 });
 }
////////TO CHANGE START & END MARKER ICON FUNCTION END////////////

 function showSteps(directionResult) {
	// For each step, place a marker, and add the text to the marker's
	// info window. Also attach the marker to an array so we
	// can keep track of it and remove it when calculating new
	// routes.
	var myRoute = directionResult.routes[0].legs[0];
	for (var i = 0; i < myRoute.steps.length; i++) {
			var marker = new google.maps.Marker({
				position: myRoute.steps[i].start_point,
				map: map
			});
			attachInstructionText(marker, myRoute.steps[i].instructions);
			markerArray[i] = marker;
		}
	}

	function attachInstructionText(marker, text) {
		//alert(text)
		google.maps.event.addListener(marker, 'click', function() {
		infoWindow.setContent(text);
		infoWindow.open(map, marker);
	});
}

///////////////Distance Calculation/////////////////
 function calculateDistance(origin,destination) {
		//alert(origin)
		var service = new google.maps.DistanceMatrixService();
		service.getDistanceMatrix(
				{
						origins: [origin],
						destinations: [destination],
						travelMode: google.maps.TravelMode.DRIVING,
						unitSystem: google.maps.UnitSystem.IMPERIAL, // miles and feet.
						// unitSystem: google.maps.UnitSystem.metric, // kilometers and meters.
						avoidHighways: false,
						avoidTolls: false
				}, callback);
	}

	// get distance results by callback
	function callback(response, status) {

			if (status != google.maps.DistanceMatrixStatus.OK) {
					$('#result').html(status);
			} else {
					var origin = response.originAddresses[0];
					//alert(origin);
					var destination = response.destinationAddresses[0];
					if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
							$('#result').html("There are no roads between "  + origin + " and " + destination);
							$('#fare_price').text('0');
					} else {
							var distance = response.rows[0].elements[0].distance;
							var duration = response.rows[0].elements[0].duration;
							console.log(response.rows[0].elements[0].distance);
							var distance_in_kilo = distance.value / 1000; // the kilom
							var distance_in_mile = distance.value / 1609.34; // the mile
							var duration_text = duration.text;
							var duration_value = duration.value;
							//alert(distance);
							$('#in_mile').text(distance_in_mile.toFixed(2));
							$('#in_kilo').text(distance_in_kilo.toFixed(2));
							$('#duration_text').text(duration_text);
							$('#duration_value').text(duration_value);
							$('#from').text(origin);
							$('#to').text(destination);
							//$('#result').html("");
							///////CALCULATE PRICE///////////////////
							$('#fare_price').text(Number(distance_in_kilo * 7).toFixed(2));
					}
			}
	}




</script>

<script type="text/javascript">
      var rides_id = '<?php echo $this->uri->segment(4); ?>';
      //alert(rides_id)
      $(document).ready(function () {

                var data = {
					 'rides_id' : rides_id,
				 };
				 var reqst = setInterval(function() { //alert();
					$('#appnd_driver_detailss').html('');
					 $.ajax({
							type: "POST",
							url: "<?php echo base_url();?>rider/user/getAcceptDriverdetails/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>",
							data: data,
							success: function(msg){
                            //alert(msg);
                                //console.log('hii');
								//alert($("#driver_infrmtion").length);
								$('#appnd_driver_detailss').html(msg);
								if($("#driver_infrmtion").length){
									clearInterval(reqst);
								}

							}
					});

				}, 5000);




				////////////////CANCEL RIDE/////////////////////////
				$(document).on('click','#cancel_rides',function () {
					var rides_id = $(this).data('rides-id');
					//alert(rides_id);
					var data = {
					 'rides_id' : rides_id,
				 	};
					$.ajax({
							type: "POST",
							url: "<?php echo base_url();?>rider/user/CancelRidesRider/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>",
							data: data,
							success: function(msg){
                                //alert(msg);
                                //console.log('hii');
								var urls = '<?php echo base_url();?>';
								window.location.href = urls;

							}
					});
				});





	  });

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY;?>&libraries=places&callback=initMap"
		async defer></script>
</html>
