<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/star-rating.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/g3_club_front_style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/fontawesome-all.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>css/lightslider.css">
	<script src="<?php echo base_url(); ?>rider_assets/js/jquery-2.2.4.min.js"></script>
  <!-- <script src="<?php echo base_url(); ?>js/jquery-3.3.1.min.js"></script> -->
	<script src="<?php echo base_url(); ?>rider_assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/touchSwipe.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/star-rating.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/star-rating_main.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  <script src="<?php echo base_url(); ?>js/jscolor.js"></script>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.js" type="text/javascript"></script>
  <script type="text/javascript">
      $(function() {
          $(window).scroll(function() {

              if ($(this).scrollTop() > 200) {
                  if (Number($(window).width()) > 1200) {
                      $('#drop-nav').slideDown('1500');
                      $('#drop-nav').css('display', 'block');
                  } else {
                      $('#drop-nav').css('display', 'none');
                  }
              } else {
                  $('#drop-nav').slideUp('1500');
              }
          });
      });

  </script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
	<script>
		$(document).ready(function(){
		  $('.nav_side_link li:has(".subMenu")').append("<i class='fa fa-chevron-right'></i>");
		  $(".side_bar_btn i").click(function(){
		    $(this).toggleClass('fa-bars fa-times');
			$("aside").toggleClass("active_aside");
			//$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
		  });
		  $(".nav_side_link li").click(function(){
		    $(this).find(".subMenu").slideToggle(300);
		    $(this).find("i").toggleClass("rotateIcon");
		  });
		  /***********swipe function**************/
		 $('body').append("<div class='swipe_to_open'></div>");
		 $('body').append("<div class='swipe_to_close'></div>");
         $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="right") {
                           $("aside").addClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                           return false;
                      }
                      if (phase=="move" && direction =="left") {
                           $("aside").removeClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                           return false;
                      }
                  }
          });
		  /*******switch*********/
		  $("#click_to_chenge").click( function (){
			if($(".chenge_text").hasClass("changeText_fild")){
			  $(".chenge_text").text("go offline");
			}
			else{
			  $(".chenge_text").text("go offline");
			}
			$("#click_to_chenge b").toggleClass("changeText_fild");
		  });

		});
	</script>

</head>

<body>

  <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #origin-input,
      #destination-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 200px;
      }

      #origin-input:focus,
      #destination-input:focus {
        border-color: #4d90fe;
      }

      #mode-selector {
        color: #fff;
        background-color: #4d90fe;
        margin-left: 12px;
        padding: 5px 11px 0px 11px;
      }

      #mode-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

    </style>
  <?php //echo '<pre>';print_r($this->session->all_userdata());?>
    <div class="cpmpleat_wrapper rider_wrap">
    <header>
                    <section class="header_profile_inner">
                        <span class="side_bar_btn">
                                <i class="fa fa-bars"></i>
                        </span>
                    </section>

                </header>
				<?php $this->load->view("rider_new/include/rider_side_bar"); ?>
   <div class="sticky_header">
    <p>
      <span>Automated Manufacturing Ltd.</span>
      <span>123 Herzl Street, 4th floor, Shaviv Quarter,46000</span>
    </p>
  </div>
  <div class="top_map_wrap">
    <div id="map"></div>
  </div>
	<div class="customer_pickup">
		<p class="trip_complete">
			<name>Trip Completed</name>
		</p>
	</div>

	<div class="client_info">

		<?php
		$rides_id = $this->uri->segment(4);
		$driver_id =  $details['driver_id'];
		$first_name = $this->my_custom_functions->get_particular_field_value("tbl_driver", "first_name", " and id='".$driver_id."'");
		$last_name = $this->my_custom_functions->get_particular_field_value("tbl_driver", "last_name", " and id='".$driver_id."'");
		?>
		<?php
    $filename="uploads/driver/idproof/".$driver_id.".jpg";
		if (file_exists($filename)){ ?>
		<span><img src="<?php echo base_url(); ?>uploads/driver/idproof/<?php echo $driver_id;?>.jpg"></span>
	  <?php }else{ ?>
    <span><img src="<?php echo base_url(); ?>uploads/driver/idproof/default_image.jpg"></span>
	  <?php }?>
		<name><?php echo $first_name;?> <?php echo $last_name;?></name>
	</div>



	<div class="rating_star_wrap">
		<b>good</b>
		<form action="<?php echo base_url();?>rider/user/rides_rating/<?php echo $rides_id;?>/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>" method="POST" enctype="multipart/form-data">
			<input id="input-21b" name="rating" value="0" type="text" class="rating" data-min=0 data-max=5 data-step=1 data-size="lg" required title="">
			<input type="submit" name="submit" value="How was your ride?">
		</form>
		<!-- <a href="#">How was your ride?</a> -->
	</div>
</div>


</body>


<script>
  // Note: This example requires that you consent to location sharing when
  // prompted by your browser. If you see the error "The Geolocation service
  // failed.", it means you probably did not give permission for the browser to
  var map, infoWindow, marker;
  var markerArray = [];
  var stepDisplay;
  var styles = [
      {
        stylers: [
          { hue: "#00ffe6" },
          { saturation: -20 }
        ]
      },{
        featureType: "road",
        elementType: "geometry",
        stylers: [
          { lightness: 100 },
          { visibility: "simplified" }
        ]
      },{
        featureType: "road",
        elementType: "labels",
        stylers: [
          { visibility: "on" }
        ]
      }
    ];

  /////////////////LOAD MAP///////////////

  function initMap() {
  var directionsService = new google.maps.DirectionsService();
  var directionsRenderer = new google.maps.DirectionsRenderer({ polylineOptions: { strokeColor: "#062847" } });

  var origin = '<?php echo $this->session->userdata('frm_latitude');?>';
  var destination = '<?php echo $this->session->userdata('frm_longitude');?>';

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 7,
    center: {lat: parseFloat(origin), lng: parseFloat(destination)},
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: styles,
    draggable:false,
    zoomControl: false,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: true
  });

  directionsRenderer.setMap(map);
  directionsRenderer.setOptions({ suppressMarkers: true });

  calculateAndDisplayRoute(directionsService, directionsRenderer,map);
}

function calculateAndDisplayRoute(directionsService, directionsRenderer, map) {

var icons = {
    start: new google.maps.MarkerImage(
      'http://server1/webdev/taxi2u/site/images/location_starts.png',
      new google.maps.Size(44, 32),
      new google.maps.Point(0, 0),
      new google.maps.Point(22, 32)),
    end: new google.maps.MarkerImage(
      'http://server1/webdev/taxi2u/site/images/location_ends.png',
      new google.maps.Size(44, 32),
      new google.maps.Point(0, 0),
      new google.maps.Point(22, 32))
  };

  var origin = '<?php echo $this->session->userdata('ses_origin');?>';
  var destination = '<?php echo $this->session->userdata('ses_destination');?>';
  //alert(origin)
  directionsService.route(
      {
        origin: {query: origin},
        destination: {query: destination},
        travelMode: google.maps.DirectionsTravelMode.DRIVING
      },
      function(response, status) {
        if (status === 'OK') {
          var leg = response.routes[0].legs[0];
          //alert(icons.start)
          //marker.setMap(null);
          makeMarker( leg.start_location, icons.start, "Start", map);
          makeMarker( leg.end_location, icons.end, 'End', map );
          directionsRenderer.setDirections(response);
          calculateDistance(origin,destination);////////////Calculate fare Price
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
}

////////TO CHANGE START & END MARKER ICON FUNCTION////////////
function makeMarker(position, icon, title, map) {
  //alert(position)
         marker = new google.maps.Marker({
             position: position,
             map: map,
             icon: icon,
             title: title
         });
     }
    ////////TO CHANGE START & END MARKER ICON FUNCTION END////////////
  function toggleBounce() {
   if (marker.getAnimation() !== null) {
     marker.setAnimation(null);
   } else {
     marker.setAnimation(google.maps.Animation.BOUNCE);
   }
 }

///////////////Distance Calculation/////////////////
 function calculateDistance(origin,destination) {
    //alert(origin)
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
        {
            origins: [origin],
            destinations: [destination],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.IMPERIAL, // miles and feet.
            // unitSystem: google.maps.UnitSystem.metric, // kilometers and meters.
            avoidHighways: false,
            avoidTolls: false
        }, callback);
  }

  // get distance results by callback
  function callback(response, status) {

      if (status != google.maps.DistanceMatrixStatus.OK) {
          $('#result').html(status);
      } else {
          var origin = response.originAddresses[0];
          //alert(origin);
          var destination = response.destinationAddresses[0];
          if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
              $('#result').html("There are no roads between "  + origin + " and " + destination);
              $('#fare_price').text('0');
          } else {
              var distance = response.rows[0].elements[0].distance;
              var duration = response.rows[0].elements[0].duration;
              console.log(response.rows[0].elements[0].distance);
              var distance_in_kilo = distance.value / 1000; // the kilom
              var distance_in_mile = distance.value / 1609.34; // the mile
              var duration_text = duration.text;
              var duration_value = duration.value;
              //alert(distance);
              $('#in_mile').text(distance_in_mile.toFixed(2));
              $('#in_kilo').text(distance_in_kilo.toFixed(2)+"km");
              $('#duration_text').text(duration_text);
              $('#duration_value').text(duration_value);
              $('#from').text(origin);
              $('#to').text(destination);
              //$('#result').html("");
              ///////CALCULATE PRICE///////////////////
              $('#fare_price').text(Number(distance_in_kilo * 7).toFixed(2));
          }
      }
  }
	///////////////SEND Data Request///////////////////



</script>



<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY;?>&libraries=places&callback=initMap"
    async defer></script>
    <!-- <div id="result">
    <ul class="list-group">
      <li class="list-group-item d-flex justify-content-between align-items-center">Distance In Mile :<span id="in_mile"></span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">Distance is Kilo:<span id="in_kilo"></span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">IN MINUTES:<span id="duration_value"></span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">IN Text:<span id="duration_text"></span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">FROM:<span id="from"></span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">TO:<span id="to"></span></li>
    </ul>
    </div>
    <div>Fare Price</div><span id="fare_price"></span> -->
    <input id="origin" name="origin" type="hidden"/></div>
    <input id="destination" name="destination" type="hidden"/>
</html>
