<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/responsive.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/validationEngine.jquery.css">
	<script src="<?php echo base_url(); ?>js/jquery-2.2.4.min.js"></script>
	<script src="<?php echo base_url(); ?>js/jquery.validationEngine.js"></script>
	<script src="<?php echo base_url(); ?>js/jquery.validationEngine-en.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/bootstrap.min.js"></script>

</head>
