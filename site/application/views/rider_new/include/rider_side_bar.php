<script>
$(document).ready(function(){
	$(".refarelPopTriger").click(function(){
		$(".side_bar_btn i").removeClass('fa-times').addClass('fa-bars');
		$("aside").removeClass("active_aside");
		$(".refarelPopWrap").fadeIn(400);
	});
	$(".refarelPopMain .closePOp").click(function(){
		$(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
		$("aside").addClass("active_aside");
		$(".refarelPopWrap").fadeOut(400);
	});


	$("#save_referral").click(function(){
		$('.span_err_msg').text('');
		var email_id = $('#referral_email').val();
		if (!ValidateEmail(email_id)){
			$('.span_err_msg').text('Enter a valid email..');
			return false;
		}else{
			//console.log('jj');
			$.ajax({
				 url: "<?php echo base_url(); ?>rider/user/save_referral_details/<?php echo TAXI_APP_DRIVER_SECURITY_KEY; ?>",
				 type: 'POST',
				 data: {email_id:email_id},
				 error: function() {
						console.log('Something is wrong');
				 },
				 success: function(data) {
					 //alert(data);
					 $('#referral_email').val('');
					 $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
			 		 $("aside").addClass("active_aside");
					 $(".refarelPopWrap").fadeOut(400);
				 }

			});
	}

	});




});

function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
};
</script>
<aside class="side_slide riderAsideWrap">
        <div class="profile_image">
                <!--span>
                    <img src="<?php echo base_url(); ?>rider_assets/images/client_image_03.jpg">
                </span-->
                <name><?php $rider_id = $this->session->userdata('rider_id');
                echo $this->my_custom_functions->get_particular_field_value("tbl_rider", "first_name", " and id='".$rider_id."'");?> <?php echo $this->my_custom_functions->get_particular_field_value("tbl_rider", "last_name", " and id='".$rider_id."'");?><name>
                   <h5><?php $country_id=$this->my_custom_functions->get_particular_field_value("tbl_rider", "country_id", " and id='".$rider_id."'");
                   echo $this->my_custom_functions->get_particular_field_value("tbl_country", "name", " and country_id='".$country_id."'");?></h5>
            </div>
    <ul class="nav_side_link">
        <li><a href="<?php echo base_url(); ?>rider/user/rider_dashboard/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>"><i class="fa fa-home"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url(); ?>rider/user/past_trip_records/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>"><i class="fa fa-history"></i> Past trip records</a></li>
        <li><a href="#"><i class="fa fa-dollar-sign"></i> Add payment method</a></li>
        <li><a href="javascript:;" class="refarelPopTriger"><i class="fa fa-user-plus"></i> Referral system</a></li>
        <!-- <li><a href="javascript:">link 04</a>
            <ul class="subMenu">
                <li><a href="#">sub link 01</a></li>
                <li><a href="#">sub link 02</a></li>
                <li><a href="#">sub link 03</a></li>
            </ul></li> -->
        <li><a href="<?php echo base_url(); ?>rider/user/currency_setting/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>"><i class="fa fa-cog"></i> Settings</a></li>
        <li><a href="<?php echo base_url(); ?>rider/main/rider_logout/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>"><i class="fa fa-share"></i> Logout</a></li>
    </ul>
	<!---refarel_pop_start--->
	<section class="refarelPopWrap">
		<div class="refarelPopInner">
			<div class="refarelPopMain">
				<button type="button" class="closePOp">X</button>
				<h2>Enter Referral Email</h2>
					<input type="email" placeholder="Referral Email" id="referral_email">
					<span style="color:red;font-size:14px;" class="span_err_msg"></span>
					<input type="submit" value="submit" id="save_referral">
			</div>
		</div>
	</section>

    <div class="driver_app">
            <a href="#" class="Taxi2u_Driver_app">Taxi2u Driver app</a>
            <a href="#" class="Taxi2u_Driver_app">Legal Notices</a>
        </div>
</aside>
