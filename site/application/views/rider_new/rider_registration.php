
<?php $this->load->view("rider_new/include/header"); ?>

<body>
	<script type="text/javascript" >

			$(document).ready(function() {
					$("#rider_registration").validationEngine({promptPosition : "bottomRight", scroll: true});

					setTimeout(function() {
							$('.s_message').hide('slow');
					}, 5000);

					setTimeout(function() {
							$('.e_message').hide('slow');
					}, 5000);

					$('#datepicker').datepicker({
							changeMonth: true,
							changeYear: true,
							yearRange:"-100:+0",
							dateFormat: 'yy-mm-dd'
					});

					//////DISABLE DATEPICKER KEYPRESS ENTRY  /////
					$('#datepicker').keypress(function(event) {
							event.preventDefault();
					});

					////////  ------  #END  #END  #END  ------   /////////
			});
	</script>
	<div class="cpmpleat_wrapper">
		<div class="login_section_wrapper">
			<div class="logo_wrapper">
				<a href="#"><img src="<?php echo base_url(); ?>rider_assets/images/main-logo.png" /></a>
			</div>

			<div class="registration_compleate_inner">
				<h2>Sign up to Ride</h2>
				<?php echo form_open_multipart('', array('id' => 'rider_registration', 'class' => 'formFields')); ?>

								<div class="invalid">
										<?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
										<?php if($this->session->flashdata("e_message")) { echo '<p class="">'.$this->session->flashdata("e_message").'</p>'; } ?>
								</div>
								<div class="sucess">
										<?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
								</div>
					<?php echo form_input(array('name'=> 'first_name','id' => 'first_name','value'=>  set_value('name'),'class'=>'register_field validate[required]','placeholder'=>'First Name')); ?>
					<!-- <input type="text" class="register_field" name="fname" placeholder="First Name" /> -->
					<?php echo form_input(array('name'=> 'last_name','id' => 'last_name','value'=>  set_value('name'),'class'=>'register_field validate[required]','placeholder'=>'Last Name')); ?>
					<?php echo form_input(array('name'=> 'phone','id' => 'phone','value'=> set_value('mobile'),'class'=>'register_field validate[required]','placeholder'=>'Phone Number')); ?>
					<?php
							$countries = $this->my_custom_functions->get_country_dropdown_data();
							echo form_dropdown('country', $countries, "", 'class="register_field validate[required]" id="country"');
					?>

					<?php echo form_input(array('type' => 'email', 'name'=> 'email','id' => 'email','value'=> set_value('email'),'class'=>'register_field validate[required,custom[email]]','placeholder'=>'Email')); ?>
					<?php echo form_input(array('type' => 'password', 'name'=> 'password','id' => 'password','value'=> set_value('password'),'class'=>'register_field validate[required]','placeholder'=>'Password'));?>
					<?php echo form_input(array('type' => 'password', 'name'=> 'confirm_password','id' => 'confirm_password','value'=> set_value('confirm_password'),'class'=>'register_field validate[required,equals[password]]','placeholder'=>'Confirm Password'));?>
					<?php echo form_upload(array('name'=> 'formal_id','id' => 'formal_id','class'=>'register_field form-control-file')); ?>
					<?php echo form_input(array('type' => 'text', 'name'=> 'promocode','id' => 'promocode','value'=> set_value('promocode'),'class'=>'register_field','placeholder'=>'Promocode'));?>
					<input type="submit" name="submit" class="register_submit" value="REGISTER" />
				<?php echo form_close(); ?>
				<a href="<?php echo base_url(); ?>rider/main/rider_login/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>" class="new_account">Have an account?</a>
			</div>
		</div>

	</div>
</body>
</html>
