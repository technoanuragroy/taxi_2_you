<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/responsive.css">
	<script src="<?php echo base_url(); ?>rider_assets/js/jquery-2.2.4.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>rider_assets/js/touchSwipe.min.js"></script>
	<script>
		$(document).ready(function(){
		  $('.nav_side_link li:has(".subMenu")').append("<i class='fa fa-chevron-right'></i>");
		  $(".side_bar_btn i").click(function(){
		    $(this).toggleClass('fa-bars fa-times');
			$("aside").toggleClass("active_aside");
			//$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
		  });
		  $(".nav_side_link li").click(function(){
		    $(this).find(".subMenu").slideToggle(300);
		    $(this).find("i").toggleClass("rotateIcon");
		  });
		  /***********swipe function**************/
		 $('body').append("<div class='swipe_to_open'></div>");
		 $('body').append("<div class='swipe_to_close'></div>");
         $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="right") {
                           $("aside").addClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                           return false;
                      }
                      if (phase=="move" && direction =="left") {
                           $("aside").removeClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                           return false;
                      }
                  }
          });

		});
	</script>
</head>

<body>
	<div class="cpmpleat_wrapper rider_wrap">
        <header>
            <section class="header_profile_inner">
				<span class="side_bar_btn">
					<h2>currency select</h2>
					<i class="fa fa-bars"></i>
				</span>
            </section>
        </header>
		<?php $this->load->view("rider_new/include/rider_side_bar"); ?>
		<section class="curencySelectWrap">
			<div class="curency_main">
				<div class="sucess">
						<?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
				</div>
				<div class="curency_main_inner">
					<?php
		      $rider_id = $this->session->userdata('rider_id');
		      $currency_id = $this->my_custom_functions->get_particular_field_value("tbl_rider", "currency_id", " and id='".$rider_id."'");
					$data = $this->my_custom_functions->get_details_from_id("", "tbl_currency", array("id" => $currency_id));
					$currency_details = $this->my_custom_functions->get_multiple_data($table = "tbl_currency", $where = "");
					?>
					<?php echo form_open_multipart(''); ?>
						<select name="currency_id">
							<option value="1">Select Currency</option>
							<?php
							foreach ($currency_details as $value) {
                 ?>
							<option value="<?=$value['id'];?>" <?php if ($data['id']==$value['id']) {
								echo "selected";
							}?>><?=$value['currency_name'];?></option>
	             <?php
	               }
	             ?>
						</select>
						<input type="submit" name="submit" value="save">
					<?php echo form_close(); ?>
				</div>
				<div class="curency_main_inner">
					<div class="table-responsive">
						<table class="table">
							<thead>
							  <tr>
								<th>Currency Name</th>
								<th>Currency Symbol</th>
								<th>Currency Amount</th>
							  </tr>
							</thead>
							<tbody>
							  <tr>
								<td><?php echo $data['currency_name'];?></td>
								<td><?php echo $data['currency_symbol'];?></td>
								<td><?php echo $data['currency_rate'];?></td>
							  </tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</div>
</body>
</html>
