<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/responsive.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>rider_assets/css/star-rating.css">
		<script src="<?php echo base_url(); ?>rider_assets/js/jquery-2.2.4.min.js"></script>
		<script src="<?php echo base_url(); ?>rider_assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>rider_assets/js/touchSwipe.min.js"></script>
		<script src="<?php echo base_url(); ?>rider_assets/js/star-rating.js"></script>
		<script src="<?php echo base_url(); ?>rider_assets/js/star-rating_main.js"></script>
		<script>
			$(document).ready(function(){
			  $('.nav_side_link li').append("<i class='fa fa-chevron-right'></i>");
			  $(".side_bar_btn i").click(function(){
				$(this).toggleClass('fa-bars fa-times');
				$("aside").toggleClass("active_aside");
				//$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
			  });
			  /***********swipe function**************/
			 $('body').append("<div class='swipe_to_open'></div>");
			 $('body').append("<div class='swipe_to_close'></div>");
			 $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
				  swipeStatus:function(event, phase, direction, distance, duration, fingers)
					  {
						  if (phase=="move" && direction =="right") {
							   $("aside").addClass("active_aside");
							   $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
							   return false;
						  }
						  if (phase=="move" && direction =="left") {
							   $("aside").removeClass("active_aside");
							   $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
							   return false;
						  }
					  }
			  });

			});
		</script>
	</head>

	<body>
		<div class="cpmpleat_wrapper rider_wrap">
			<?php $this->load->view("rider_new/include/rider_side_bar"); ?>
			<div class="payment_wrap">
                <!-- <a href="#" class="request">Pay with via</a>
                <div class="paypal_payment">
                <img src="images/paypal.png" />
            </div> -->
								<?php
								$rides_id =  $this->uri->segment(4);
								$amount = $this->my_custom_functions->get_particular_field_value("tbl_rides", "amount", " and rides_id='".$rides_id."'");


					      $rider_id = $this->session->userdata('rider_id');
					      $currency_id = $this->my_custom_functions->get_particular_field_value("tbl_rider", "currency_id", " and id='".$rider_id."'");
					      $currency_symbol = $this->my_custom_functions->get_particular_field_value("tbl_currency", "currency_symbol", " and id='".$currency_id."'");
					      $currency_rate = $this->my_custom_functions->get_particular_field_value("tbl_currency", "currency_rate", " and id='".$currency_id."'");
					      if($currency_id != 0 ){$currencysymbol = $currency_symbol;}else{$currencysymbol = '$';}
								?>

                <a href="javascript:" class="amount">Amount- <?php echo $currencysymbol;?><?php echo $amount;?></a>
                <div class="payment_submit">
                <a href="<?php echo base_url();?>rider/user/payment_confirm/<?php echo $rides_id ?>/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>" class="s_btn">Submit</a>
            </div>
            </div>

		</div>
	</body>
	</html>
