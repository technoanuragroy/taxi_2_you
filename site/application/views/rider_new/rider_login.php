
<?php $this->load->view("rider_new/include/header"); ?>

<body>
	<script type="text/javascript" >
			$(document).ready(function(){
					$("#rider_login").validationEngine({promptPosition : "bottomRight", scroll: true});
			});

			setTimeout(function() {
					$('.s_message').hide('slow');
			}, 5000);

			setTimeout(function() {
					$('.e_message').hide('slow');
			}, 5000);

			function isNumber(evt) {

					evt = (evt) ? evt : window.event;
					var charCode = (evt.which) ? evt.which : evt.keyCode;
					if (charCode > 31 && (charCode < 48 || charCode > 57)) {
							return false;
					}
					return true;
			}
	</script>
	<div class="cpmpleat_wrapper">
		<div class="login_section_wrapper">
			<div class="logo_wrapper">
				<a href="#"><img src="<?php echo base_url(); ?>rider_assets/images/main-logo.png" /></a>
			</div>
				<h4 class="headMain">rider login</h4>
			<div class="login_form-section">
				<?php echo form_open('', array('id' => 'rider_login', 'class' => 'formFields')); ?>

								<div class="invalid">
										<?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
										<?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
								</div>
								<div class="sucess">
										<?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
								</div>
					<!-- <input type="text" class="user_name user validate[required, custom[email]]" placeholder="User name" name="username" id="username"> -->
					<?php echo form_input(array('name'=> 'username','id' => 'username','class'=>'user_name user form-control validate[required, custom[email]]')); ?>
					<?php echo form_input(array('type' => 'password', 'name'=> 'password','id' => 'password','value'=> '','class'=>'pass user form-control validate[required]')); ?>
					<!-- <input type="password" class="pass user validate[required]" placeholder="Password" name="password" id="password"> -->
					<input type="submit" name="submit" class="submit_Btn" value="Login">
					<div class="register_wrap">
						<a href="<?php echo base_url(); ?>rider/main/index/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>" class="new_account">Register</a>
						<a class="Forget_pass" href="<?php echo base_url(); ?>rider/main/forgot_password/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>"> Forgot Password? </a>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>




	<div class="rider_version_enter">
		<a href="<?php echo base_url(); ?>driver/main/login/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>">Enter to Driver Version <img src="<?php echo base_url(); ?>rider_assets/images/right_arrow.png"></a>
	</div>

	</div>
</body>
</html>
