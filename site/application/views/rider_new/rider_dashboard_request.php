<?php $this->load->view("rider_new/include/header_dashboard"); ?>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
	<script>
		$(document).ready(function(){
		  $('.nav_side_link li:has(".subMenu")').append("<i class='fa fa-chevron-right'></i>");
		  $(".side_bar_btn i").click(function(){
		    $(this).toggleClass('fa-bars fa-times');
			$("aside").toggleClass("active_aside");
			//$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
		  });
		  $(".nav_side_link li").click(function(){
		    $(this).find(".subMenu").slideToggle(300);
		    $(this).find("i").toggleClass("rotateIcon");
		  });
		  /***********swipe function**************/
		 $('body').append("<div class='swipe_to_open'></div>");
		 $('body').append("<div class='swipe_to_close'></div>");
         $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="right") {
                           $("aside").addClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                           return false;
                      }
                      if (phase=="move" && direction =="left") {
                           $("aside").removeClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                           return false;
                      }
                  }
          });
		  /*******switch*********/
		  $("#click_to_chenge").click( function (){
			if($(".chenge_text").hasClass("changeText_fild")){
			  $(".chenge_text").text("go offline");
			}
			else{
			  $(".chenge_text").text("go offline");
			}
			$("#click_to_chenge b").toggleClass("changeText_fild");
		  });

		});
	</script>

</head>

<body>

  <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #origin-input,
      #destination-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 200px;
      }

      #origin-input:focus,
      #destination-input:focus {
        border-color: #4d90fe;
      }

      #mode-selector {
        color: #fff;
        background-color: #4d90fe;
        margin-left: 12px;
        padding: 5px 11px 0px 11px;
      }

      #mode-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
    </style>
  <?php //echo '<pre>';print_r($this->session->all_userdata());?>
    <div class="cpmpleat_wrapper rider_wrap">
    <header>
        <section class="header_profile_inner">
            <span class="side_bar_btn">
				<div class="fromTo">
				  <?php //echo '<pre>';print_r($this->session->userdata());die;?>
				  <span><b>Form: </b><?php echo $this->session->userdata('ses_origin');?></span>
				  <span><b>To: </b><?php echo $this->session->userdata('ses_destination');?></span>
				</div>
                <i class="fa fa-bars"></i>
            </span>
        </section>
    </header>
				<?php $this->load->view("rider_new/include/rider_side_bar"); ?>

  <div class="top_map_wrap">
    <div id="map"></div>
  </div>
  <div class="customer_pickup">
    <p class="fare_price">
      <?php
      $rider_id = $this->session->userdata('rider_id');
      $currency_id = $this->my_custom_functions->get_particular_field_value("tbl_rider", "currency_id", " and id='".$rider_id."'");
      $currency_symbol = $this->my_custom_functions->get_particular_field_value("tbl_currency", "currency_symbol", " and id='".$currency_id."'");
      $currency_rate = $this->my_custom_functions->get_particular_field_value("tbl_currency", "currency_rate", " and id='".$currency_id."'");

      $wallet_balance = $this->my_custom_functions->get_particular_field_value("tbl_wallet", "wallet_balance", " and rider_id='".$rider_id."'");

      if($currency_id != 0 ){$currencysymbol = $currency_symbol;}else{$currencysymbol = '$';}
      ?>
      <input type="hidden" class="currencyrate" value="<?php echo $currency_rate; ?>">
      <input type="hidden" class="currencysymbol" value="<?php echo $currency_symbol; ?>">
      <input type="hidden" class="fare_price" id="fare_price">
      <input type="hidden" class="final_wallet_balance" id="final_wallet_balance">
      <b>Fare Price:<?php echo $currencysymbol;?><span id="fare_price_html" name="fare_price_html"></span></b>
      <b>Distance : <span id="in_kilo" name="in_kilo"></span></b><br>
      <b>Duration : <span id="duration_text" name="duration_text"></span></b>
      <?php if($wallet_balance != "" && $wallet_balance != 0){?>
    </p>
    <div class="wollet_contain">
      <label class="Wcontainer">
        <input type="checkbox" name="wallet_balance" class="add_wallet_balance" value="<?php echo $wallet_balance;?>" >
        <i class="fa fa-tags"></i> Wallet balance  <?php echo $currency_symbol;?><?php echo $wallet_balance;?>
        <span class="checkmark"></span>
      </label>
      <br>
    <?php }?>
    </div>
    <span id="fare_price_box" style="display:none;"><b>Final Fare Price </b>  <span id="final_fare_currency"><?php echo $currencysymbol;?></span><span id="final_fare"></span><span id="final_fare_wallet"></span></span>

    <div class="confirm_btn_grup">
      <div class="payment_group">
      <!-- <a href="#" class="add_payment"> Add payment method</a> -->
      <span id="success" ></span>
      <div class="travel_mode_select">
      <!--<input type="radio" name="travel_mode" class="travel_mode" value="1" checked>:Taxi
      <input type="radio" name="travel_mode" class="travel_mode" value="2">:Mini bus
      <input type="radio" name="travel_mode" class="travel_mode" value="3">:Bus
      <input type="radio" name="travel_mode" class="travel_mode" value="4">:Ambulance
      -->
		<label class="car_container"><i class="fa fa-taxi"></i>
		  <input type="radio" name="travel_mode" class="travel_mode" value="1" checked>
		  <span class="checkmark"></span>
		</label>
		<label class="car_container"><i class="fa fa-bus"></i>
		  <input type="radio" name="travel_mode" class="travel_mode" value="2">
		  <span class="checkmark"></span>
		</label>
		<label class="car_container"><i class="fa fa-car"></i>
		  <input type="radio" name="travel_mode" class="travel_mode" value="3">
		  <span class="checkmark"></span>
		</label>
		<label class="car_container"><i class="fa fa-ambulance"></i>
		  <input type="radio" name="travel_mode" class="travel_mode" value="4">
		  <span class="checkmark"></span>
		</label>
    <label class="car_container"><i class="fa fa-motorcycle" ></i>
      <input type="radio" name="travel_mode" class="travel_mode" value="4">
      <span class="checkmark"></span>
    </label>
	  </div>
      <!-- <a href="#" class="add_payment_paypal"><img src="<?php echo base_url(); ?>rider_assets/images/paypal.png"></a> -->
    </div>

    <a onclick="trip_confirm_send()" class="payment_btn_long send_rqst_btn">Request Taxi2u</a>

    <a href="<?php echo base_url();?>rider/user/pending_request/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>" class="payment_btn_long pnding_rqst_btn">Go To Pending request</a>

	  <!-- <a class="W_logOut" href="<?php echo base_url(); ?>rider/main/rider_logout">Logout</a> -->
    <input type="hidden" name="request_id" id="request_id">
    </div>
  </div>
</div>



</body>

<script>
  // Note: This example requires that you consent to location sharing when
  // prompted by your browser. If you see the error "The Geolocation service
  // failed.", it means you probably did not give permission for the browser to
  var map, infoWindow, marker;
  var markerArray = [];
  var stepDisplay;
  var styles = [
      {
        stylers: [
          { hue: "#00ffe6" },
          { saturation: -20 }
        ]
      },{
        featureType: "road",
        elementType: "geometry",
        stylers: [
          { lightness: 100 },
          { visibility: "simplified" }
        ]
      },{
        featureType: "road",
        elementType: "labels",
        stylers: [
          { visibility: "on" }
        ]
      }
    ];

  /////////////////LOAD MAP///////////////

  function initMap() {
  var directionsService = new google.maps.DirectionsService();
  var directionsRenderer = new google.maps.DirectionsRenderer({ polylineOptions: { strokeColor: "#062847" } });

  var origin = '<?php echo $this->session->userdata('frm_latitude');?>';
  var destination = '<?php echo $this->session->userdata('frm_longitude');?>';

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 7,
    center: {lat: parseFloat(origin), lng: parseFloat(destination)},
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: styles,
    draggable:false,
    zoomControl: false,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: true
  });

  directionsRenderer.setMap(map);
  directionsRenderer.setOptions({ suppressMarkers: true });

  calculateAndDisplayRoute(directionsService, directionsRenderer,map);
}

function calculateAndDisplayRoute(directionsService, directionsRenderer, map) {

var icons = {
    start: new google.maps.MarkerImage(
      'http://server1/webdev/taxi2u/site/images/location_starts.png',
      new google.maps.Size(44, 32),
      new google.maps.Point(0, 0),
      new google.maps.Point(22, 32)),
    end: new google.maps.MarkerImage(
      'http://server1/webdev/taxi2u/site/images/location_ends.png',
      new google.maps.Size(44, 32),
      new google.maps.Point(0, 0),
      new google.maps.Point(22, 32))
  };

  var origin = '<?php echo $this->session->userdata('ses_origin');?>';
  var destination = '<?php echo $this->session->userdata('ses_destination');?>';
  //alert(origin)
  directionsService.route(
      {
        origin: {query: origin},
        destination: {query: destination},
        travelMode: google.maps.DirectionsTravelMode.DRIVING
      },
      function(response, status) {
        if (status === 'OK') {
          var leg = response.routes[0].legs[0];
          //alert(icons.start)
          //marker.setMap(null);
          makeMarker( leg.start_location, icons.start, "Start", map);
          makeMarker( leg.end_location, icons.end, 'End', map );
          directionsRenderer.setDirections(response);
          calculateDistance(origin,destination);////////////Calculate fare Price
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
}

////////TO CHANGE START & END MARKER ICON FUNCTION////////////
function makeMarker(position, icon, title, map) {
  //alert(position)
         marker = new google.maps.Marker({
             position: position,
             map: map,
             icon: icon,
             title: title
         });
     }
    ////////TO CHANGE START & END MARKER ICON FUNCTION END////////////
  function toggleBounce() {
   if (marker.getAnimation() !== null) {
     marker.setAnimation(null);
   } else {
     marker.setAnimation(google.maps.Animation.BOUNCE);
   }
 }

///////////////Distance Calculation/////////////////
 function calculateDistance(origin,destination) {
    //alert(origin)
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
        {
            origins: [origin],
            destinations: [destination],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.IMPERIAL, // miles and feet.
            // unitSystem: google.maps.UnitSystem.metric, // kilometers and meters.
            avoidHighways: false,
            avoidTolls: false
        }, callback);
  }

  // get distance results by callback
  function callback(response, status) {

      if (status != google.maps.DistanceMatrixStatus.OK) {
          $('#result').html(status);
      } else {
          var origin = response.originAddresses[0];
          //alert(origin);
          var destination = response.destinationAddresses[0];
          if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
              $('#result').html("There are no roads between "  + origin + " and " + destination);
              $('#fare_price').val('0');
              $('#fare_price_html').text('');
          } else {
              var distance = response.rows[0].elements[0].distance;
              var duration = response.rows[0].elements[0].duration;
              console.log(response.rows[0].elements[0].distance);
              var distance_in_kilo = parseFloat(distance.value / 1000); // the kilom
              var distance_in_kilo_fixed = parseFloat(distance_in_kilo.toFixed(2));
              var distance_in_mile = distance.value / 1609.34; // the mile
              var duration_text = duration.text;
              var duration_value = duration.value;
              //alert(distance);
              $('#in_mile').text(distance_in_mile.toFixed(2));
              $('#in_kilo').text(distance_in_kilo.toFixed(2)+"km");
              $('#duration_text').text(duration_text);
              $('#duration_value').text(duration_value);
              $('#from').text(origin);
              $('#to').text(destination);
              //$('#result').html("");
              ///////CALCULATE PRICE///////////////////
              var currencyrate = Number($('.currencyrate').val());

              var rate_per_km = Number('<?php echo RATE_PER_KM; ?>');
              //alert(distance_in_kilo_fixed);
              $('#fare_price').val(parseFloat(distance_in_kilo_fixed * currencyrate * rate_per_km).toFixed(2));
              $('#fare_price_html').text(parseFloat(distance_in_kilo_fixed * currencyrate * rate_per_km).toFixed(2));

          }
      }
  }
	///////////////SEND Data Request///////////////////

	function trip_confirm_send() {
		var distance = $("#in_kilo").text();
		var amount = $("#fare_price").val();
    var travel_mode = $(".travel_mode:checked").val();
    var final_wallet_balance = '';
      if ($('.add_wallet_balance').is(':checked')) {
         final_wallet_balance = $("#final_wallet_balance").val();
    }else{
         final_wallet_balance = '';
    }
    //var travel_mode = $("input[name='travel_mode']:checked"). val();
    //alert(final_wallet_balance)
		$.ajax({
			 url: "<?php echo base_url(); ?>rider/User/trip_confirm/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>",
			 type: 'POST',
			 data: {distance:distance,amount:amount,travel_mode:travel_mode,final_wallet_balance:final_wallet_balance},
			 error: function() {
					alert('No Driver Found');
			 },
			 success: function(data) {
				 //alert(data)
				 $('#success').html("Request Send Successfully");
         $('.send_rqst_btn').hide();
         $('.travel_mode_select').hide();
         $('.pnding_rqst_btn').show();

			 }

		});
	}

</script>
<script type="text/javascript">
$(document).ready(function () {

$(".add_wallet_balance").attr("disabled", true);///disable checkbox wallet

var act_rides_id = "<?php echo $this->session->userdata('act_rides_id');?>";
  //alert(act_rides_id);
  if(act_rides_id != ""){
    $('.send_rqst_btn').hide();
    $('.pnding_rqst_btn').show();
  }else{
    $('.send_rqst_btn').show();
    $('.pnding_rqst_btn').hide();
  }

  $('.add_wallet_balance').change(function() {
    var fare_price = parseFloat($("#fare_price").val());
    var fare_price_html = parseFloat($("#fare_price_html").text());
    var wallet_balance = parseFloat($(this).val());

    if(wallet_balance > fare_price){
    var final_price = fare_price
    var walletblnc = $('.final_wallet_balance').val(parseFloat(wallet_balance - fare_price));
    }else{
    var final_price = parseFloat(fare_price - wallet_balance);
    var walletblnc = $('.final_wallet_balance').val(0);
    }

    if ($(this).prop('checked')) {
    //blah bla
    $('#fare_price_box').show();
    if(wallet_balance > fare_price){
    $('#final_fare').hide();
    $('#final_fare_wallet').text(0.00);
    }else{
    $('#final_fare').show();
    $('#final_fare_wallet').hide() ;
    $('#final_fare').text(final_price.toFixed(2));
    }
    //$('#final_fare_currency').text(final_price.toFixed(2));
    $('#fare_price').val(final_price.toFixed(2));
  }
  else{
    $('#fare_price_box').hide();
    $('#final_fare').text('');
    //$('#fare_price_html').text(final_price.toFixed(2));
    $('#fare_price').val(fare_price_html.toFixed(2));
  }
  });

});

/////Enable Checkbox After readyState=complete
document.onreadystatechange = function () {
  if (document.readyState === 'complete') {
     $(".add_wallet_balance").attr("disabled", false);///enable checkbox wallet
  }
}
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY;?>&libraries=places&callback=initMap"
    async defer></script>
    <!-- <div id="result">
    <ul class="list-group">
      <li class="list-group-item d-flex justify-content-between align-items-center">Distance In Mile :<span id="in_mile"></span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">Distance is Kilo:<span id="in_kilo"></span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">IN MINUTES:<span id="duration_value"></span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">IN Text:<span id="duration_text"></span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">FROM:<span id="from"></span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">TO:<span id="to"></span></li>
    </ul>
    </div>
    <div>Fare Price</div><span id="fare_price"></span> -->
    <input id="origin" name="origin" type="hidden"/></div>
    <input id="destination" name="destination" type="hidden"/>
</html>
