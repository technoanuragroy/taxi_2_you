<?php $this->load->view('_include/header_inner_driver'); ?>
<script>
  $(document).ready(function(){
    $('.nav_side_link li:has(".subMenu")').append("<i class='fa fa-chevron-right'></i>");
    $(".side_bar_btn i").click(function(){
    $(this).toggleClass('fa-bars fa-times');
    $("aside").toggleClass("active_aside");
    //$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
    });
    $(".nav_side_link li").click(function(){
      $(this).find(".subMenu").slideToggle(300);
      $(this).find("i").toggleClass("rotateIcon");
    });
    /***********swipe function**************/
   $('body').append("<div class='swipe_to_open'></div>");
   $('body').append("<div class='swipe_to_close'></div>");
       $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
            swipeStatus:function(event, phase, direction, distance, duration, fingers)
                {
                    if (phase=="move" && direction =="right") {
                         $("aside").addClass("active_aside");
                         $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                         return false;
                    }
                    if (phase=="move" && direction =="left") {
                         $("aside").removeClass("active_aside");
                         $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                         return false;
                    }
                }
        });

  });
</script>
<body class="main_body">
  <div class="cpmpleat_wrapper">
    <header>
      <section class="header_inner">
        <span class="side_bar_btn">
          <i class="fa fa-bars"></i>
        </span>
        <!-- <h2>current location</h2> -->
      </section>
    </header>
    <?php $this->load->view('_include/sidebar_driver'); ?>
    <div class="record_section_wrapper">
      <h2>Pending Trips</h2>
    <div class="full_page_map_wrap">
      <?php
      if(!empty($get_rides)){
      foreach ($get_rides as $value) {?>

        <?php //print_r($value); ?>

      <div class="request_content">
          <div class="request_group">
              <ul>
                  <li><a href=""><b>FROM</b><br>
                       <?php echo substr($value['pickup_adress'],0,50);
                        ?></a>
               </li><?php $driver_id = $value['driver_id'];
                    $rides_id = $value['rides_id'];
                ?>
                  <li class="circle"><a href="<?php echo base_url();?>driver/user/customer_pickup/<?php echo $driver_id ?>/<?php echo $rides_id ?>"><i class="fa fa-long-arrow-right"></i></a></li>
                  <li class="group_rgt"><a href=""><b class="to_rgt">To</b><br>
                        <?php echo substr($value['drop_address'],0,50); ?></a></li>
              </ul>

          </div>
          <div class="request_group1">
          <ul>
               <li><a href=""><b>DATE</b><br></a>
                   <ul class="group1_inner">
                       <li><a href=""> 05:04:25 AM</a> </li>
                       <li><a href=""> 26th July 2019</a></li>
                   </ul>
            </li>
               <li class="group1_rgt"><a href="">
                 <b><?php echo $this->my_custom_functions->get_particular_field_value('tbl_rider', 'first_name', 'and id="'.$value['rider_id'].'"'); ?>
                   <?php echo $this->my_custom_functions->get_particular_field_value('tbl_rider', 'last_name', 'and id="'.$value['rider_id'].'"'); ?>
                 </b><br>
                    B.C Road Burdwan</a></li>
           </ul>
       </div>
      </div>
    <?php } }else{?>
      <div class="request_content">

              <h2>No record found...</h2>

      </div>
    <?php }?>
    </div>
  </div>
</div>
</body>
