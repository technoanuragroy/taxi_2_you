<?php $this->load->view("_include/header_sign_up"); ?>
<script type="text/javascript" >

		$(document).ready(function() {
				$("#driver_registration").validationEngine({promptPosition : "bottomRight", scroll: true});

				setTimeout(function() {
						$('.s_message').hide('slow');
				}, 5000);

				setTimeout(function() {
						$('.e_message').hide('slow');
				}, 5000);

				$('#datepicker').datepicker({
						changeMonth: true,
						changeYear: true,
						yearRange:"-100:+0",
						dateFormat: 'yy-mm-dd'
				});

				//////DISABLE DATEPICKER KEYPRESS ENTRY  /////
				$('#datepicker').keypress(function(event) {
						event.preventDefault();
				});

				////////  ------  #END  #END  #END  ------   /////////
		});
</script>



<body>
	<div class="cpmpleat_wrapper">
		<div class="login_section_wrapper">
			<div class="logo_wrapper">
				<a href="#"><img src="<?php echo base_url(); ?>driver_assets/images/main-logo.png" /></a>
			</div>

			<div class="registration_compleate_inner">

				<?php echo form_open_multipart('', array('id' => 'driver_registration', 'class' => 'formFields')); ?>

				<div class="invalid">
						<?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
						<?php if($this->session->flashdata("e_message")) { echo '<p class="">'.$this->session->flashdata("e_message").'</p>'; } ?>
				</div>
				<div class="sucess">
						<?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
				</div>

				<?php if (($this->uri->segment(4) != 2) && ($this->uri->segment(4) !=3)) {?>

					<h2>Sign up to Driver</h2>

					<select name="car_type" class="reg_fild">
						<option>Select
					<?php $car_type = $this->config->item('travel_mode');
						//print_r($car_type);
						foreach($car_type as $key=>$ctype){?>
							<option value="<?php echo $key;?>"><?php echo $ctype;?></option>
						<?php }
						?>
					</select>
					<!-- <input type="text" class="reg_fild" placeholder="First Name" /> -->
					<?php echo form_input(array('name'=> 'first_name','id' => 'first_name','value'=>  set_value('first_name'),'placeholder' => 'First Name','class'=>'reg_fild validate[required]')); ?>
					<!-- <input type="text" class="reg_fild" placeholder="Last Name" /> -->
					<?php echo form_input(array('name'=> 'last_name','id' => 'last_name','value'=>  set_value('last_name'),'placeholder' => 'Last Name','class'=>'reg_fild validate[required]')); ?>
					<!-- <input type="number" class="reg_fild" placeholder="Phone Number" /> -->
					<?php echo form_input(array('name'=> 'mobile','id' => 'mobile','value'=> set_value('mobile'),'placeholder' => 'Phone Number','class'=>'reg_fild validate[required]')); ?>
					<!-- <input type="text" class="reg_fild" placeholder="Country" /> -->
					<?php
							$countries = $this->my_custom_functions->get_country_dropdown_data();
							echo form_dropdown('country', $countries, "", 'class="reg_fild validate[required]" id="country"');
					?>
					<!-- <input type="eamail" class="reg_fild" placeholder="Email" /> -->
					<?php echo form_input(array('type' => 'email', 'name'=> 'email','id' => 'email','value'=> set_value('email'),'placeholder' => 'E-Mail','class'=>'reg_fild validate[required,custom[email]]')); ?>
					<!-- <input type="password" class="reg_fild" placeholder="Password" /> -->
					<?php echo form_input(array('type' => 'password', 'name'=> 'password','id' => 'password','value'=> set_value('password'),'placeholder' => 'Password','class'=>'reg_fild validate[required]'));?>
					<!-- <input type="text" class="reg_fild" placeholder="Promocode" /> -->
					<?php echo form_input(array('name'=> 'promocode','id' => 'promocode','value'=> set_value('promocode'),'placeholder' => 'Promocode','class'=>'reg_fild')); ?>
					 <input type="submit" name="submit" class="reg_sub" value="sign up" />
					<?php } ?>

					<?php if ($this->uri->segment(4) == 2) {?>
						<?php $id = $this->uri->segment(3); ?>
						<div class="cpmpleat_wrapper">
							<div class="drp_02_image">
								<span>
									<img src="<?php echo base_url(); ?>driver_assets/images/client_image_03.jpg">
								</span>
								<?php $name=$this->my_custom_functions->get_particular_field_value('tbl_driver','first_name','and id='.$id);?>
								<name><?php echo $name; ?><name>
							</div>
							<div class="drp_02_wrap">
									<div class="drp_02_inputs">
										<div class="drp_02_inputs_container">
											<span>
												<p>Identificatoin Proof</p>
												<label>add image
													<!-- <input type="file"> -->
													<?php echo form_upload(array('name'=> 'id_proof','id' => 'id_proof')); ?>
												</label>
												<!--<section>-->
												<!--	<img src="<?php echo base_url(); ?>driver_assets/images/client_image_03.jpg">-->
												<!--</section>-->
											</span>

											<span>
												<p>Driving Lisence</p>
												<label>add image
													<!-- <input type="file"> -->
													<?php echo form_upload(array('name'=> 'driving_licence','id' => 'driving_licence')); ?>
												</label>
												<!--<section>-->
												<!--	<img src="<?php echo base_url(); ?>driver_assets/images/client_image_03.jpg">-->
												<!--</section>-->
											</span>
										</div>
									</div>
									<input type="submit" name="submit" class="drp_02_submit" value="upload" />
							</div>
						</div>

					<?php } ?>
					<?php if ($this->uri->segment(4) == 3) {?>

						<div class="cpmpleat_wrapper">
							<div class="login_section_wrapper">


								<div class="registration_compleate_inner">
									<h2>Enter vehicle details</h2>
									
										<!-- <input type="text" class="reg_fild" placeholder="Vehicle name" /> -->
										<?php echo form_input(array('name'=> 'vehicle_name','placeholder' =>'Vehicle Name','id' => 'vehicle_name','value'=> set_value('promocode'),'class'=>'reg_fild validate[required]')); ?>
										<!-- <input type="text" class="reg_fild" placeholder="Vehicle maker" /> -->
										<?php echo form_input(array('name'=> 'vehicle_make','id' => 'vehicle_make','placeholder' =>'Vehicle Maker','value'=> set_value('vehicle_make'),'class'=>'reg_fild validate[required]')); ?>
										<!-- <input type="text" class="reg_fild" placeholder="Vehicle model" /> -->
										<?php echo form_input(array('name'=> 'vehicle_model','id' => 'vehicle_name','placeholder' =>'Vehicle Model','value'=> set_value('vehicle_model'),'class'=>'reg_fild validate[required]')); ?>
										<!-- <input type="text" class="reg_fild" placeholder="Vehicle color" /> -->
										<?php echo form_input(array('name'=> 'vehicle_color','id' => 'vehicle_color','placeholder' =>'Vehicle Colour','value'=> set_value('vehicle_color'),'class'=>'reg_fild validate[required] jscolor')); ?>
										<!-- <input type="text" class="reg_fild" placeholder="Vehicle plate no" /> -->
										<?php echo form_input(array('name'=> 'vehicle_plate_no','id' => 'vehicle_plate_no','placeholder' =>'Vehicle Plate No.','value'=> set_value('vehicle_plate_no'),'class'=>'reg_fild validate[required]')); ?>
										<label>
											Vehicle Picture
											<!-- <input type="file" class="reg_fild" placeholder="Vehicle plate no" /> -->
											<?php echo form_upload(array('name'=> 'vehicle_photo','id' => 'vehicle_photo','placeholder' =>'Vehicle Photo','class'=>'reg_fild picture')); ?>
										</label>
										<label class="file_size_declare">
											Vehicle Inside video
											<!-- <input type="file" class="reg_fild" placeholder="Vehicle plate no" /> -->
											<?php echo form_upload(array('name'=> 'vehicle_video_1','id' => 'vehicle_video_1','placeholder' =>'Vehicle video','class'=>'reg_fild video')); ?>
										</label>
										<label class="file_size_declare">
											Vehicle Outside video
											<?php echo form_upload(array('name'=> 'vehicle_video_2','id' => 'vehicle_video_2','placeholder' =>'Vehicle video','class'=>'reg_fild video')); ?>
										</label>
										<input type="submit" name="submit" class="reg_sub" value="register" />

								</div>
							</div>

						</div>



					<?php } ?>

					<!-- <input type="submit" name="submit" value="Sign Up" class="reg_sub"> -->



				<?php echo form_close(); ?>
			</div>
		</div>

	</div>
</body>
</html>
