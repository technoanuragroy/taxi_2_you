

<?php $this->load->view("_include/header_inner_driver"); ?>


    <body class="main_body">
    	<div class="cpmpleat_wrapper">
    		<header>
    			<section class="header_inner">
    				<span class="side_bar_btn">
    					<i class="fa fa-bars"></i>
    				</span>

            <?php
            if($details['driver_status'] == 1){
                $checked = 'checked = "checked"';
                $status_text = 'Go Offline';
            } else {
                $checked = '';
                $status_text = 'Go Online';
            }
            ?>

    				<!-- <div class="toggleSuitch" id="click_to_chenge">
    					<label class="switch">
                <?php $id = $this->session->userdata('driver_id'); ?>
                <input class="status_<?php echo $id; ?>" type="hidden" name="driver_status" value="<?php echo $details['driver_status']; ?>" />

                <input type="checkbox" <?php echo $checked ?> onclick="return change_status(<?php echo $id ?>);">
    						<span class="slider round"></span>
    					</label>
    					<b class="chenge_text"><?php echo $status_text; ?></b>
    				</div> -->
            <div class="toggleSuitch" id="click_to_chenge">
              <input class="status_<?php echo $id; ?> driver_status_hidden" type="hidden" name="driver_status" value="<?php echo $details['driver_status']; ?>" />
            <div class="onoffswitch">
              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" <?php echo $checked;?> onclick="return change_status(<?php echo $id ?>);">
              <label class="onoffswitch-label" for="myonoffswitch">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
              </label>
          </div>
          </div>


    				<h2>current location</h2>
    			</section>
    		</header>
        <?php $this->load->view('_include/sidebar_driver'); ?>
    		<div class="full_page_map_wrap">
    			<div id="map"></div>
    		</div>
    	</div>

      <script>
        $(document).ready(function(){
          $('.nav_side_link li:has(".subMenu")').append("<i class='fa fa-chevron-right'></i>");
          $(".side_bar_btn i").click(function(){
          $(this).toggleClass('fa-bars fa-times');
          $("aside").toggleClass("active_aside");
          //$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
          });
          $(".nav_side_link li").click(function(){
            $(this).find(".subMenu").slideToggle(300);
            $(this).find("i").toggleClass("rotateIcon");
          });
          /***********swipe function**************/
         $('body').append("<div class='swipe_to_open'></div>");
         $('body').append("<div class='swipe_to_close'></div>");
             $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
                  swipeStatus:function(event, phase, direction, distance, duration, fingers)
                      {
                          if (phase=="move" && direction =="right") {
                               $("aside").addClass("active_aside");
                               $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                               return false;
                          }
                          if (phase=="move" && direction =="left") {
                               $("aside").removeClass("active_aside");
                               $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                               return false;
                          }
                      }
              });

        });
      </script>

      <script>
        // Note: This example requires that you consent to location sharing when
        // prompted by your browser. If you see the error "The Geolocation service
        // failed.", it means you probably did not give permission for the browser to
        var map, infoWindow, marker;
        var markerArray = [];
        var stepDisplay;
        var styles = [
            {
              stylers: [
                { hue: "#00ffe6" },
                { saturation: -20 }
              ]
            },{
              featureType: "road",
              elementType: "geometry",
              stylers: [
                { lightness: 100 },
                { visibility: "simplified" }
              ]
            },{
              featureType: "road",
              elementType: "labels",
              stylers: [
                { visibility: "on" }
              ]
            }
          ];

        function initMap() {
          var driverStatus = '<?php echo $details['driver_status']; ?>';
          ////Set From Address On Page Load////

          map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 31.0461, lng: 34.8516},
            zoom: 12,
            //mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: styles,
            draggable:true
          });

          infoWindow = new google.maps.InfoWindow();

          var iconBase = '<?php echo base_url();?>images/marker.png';
          marker = new google.maps.Marker({
          map: map,
          draggable: false,
          //icon: iconBase,
          animation: google.maps.Animation.DROP,
          position: {lat: 31.0461, lng: 34.8516}
          });

          //marker.addListener('click', toggleBounce);

          //////Geeolocation Tracking Code Starts here///////////

         if (navigator.geolocation) {
           navigator.geolocation.getCurrentPosition(function(position) {

             ////If Geolocation Found Set Current Address From geo location////
             //GetAddress(position.coords.latitude,position.coords.longitude);

             var pos = {
               lat: position.coords.latitude,
               lng: position.coords.longitude
             };
             infoWindow.setPosition(pos);
             //infoWindow.setContent('Location found.');
             //infoWindow.open(map);
             map.setCenter(pos);
             toggleBounce();
           }, function() {
             handleLocationError(true, infoWindow, map.getCenter());
           });
           } else {
             // Browser doesn't support Geolocation
             handleLocationError(false, infoWindow, map.getCenter());
           }

           //////Geeolocation Tracking Code Ends here//////////


        ////////////////End Init Function(Map)/////////////////////
        }

        function toggleBounce() {
         if (marker.getAnimation() !== null) {
           marker.setAnimation(null);
         } else {
           marker.setAnimation(google.maps.Animation.BOUNCE);
         }
       }

        ///////////Error Handler for Geolocation///////////////////
        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
         infoWindow.setPosition(pos);
         infoWindow.setContent(browserHasGeolocation ?
                               'Error: The Geolocation service failed.' :
                               'Error: Your browser doesn\'t support geolocation.');
         infoWindow.open(map);
       }
       ///////////Error Handler for Geolocation///////////////////

       /////////Gps Update if Current Location Changed//////////////
         function autoUpdate() {
           //console.log('kk');
           navigator.geolocation.getCurrentPosition(function(position) {
           var newPoint = new google.maps.LatLng(position.coords.latitude,
                                              position.coords.longitude);

           autoUpdate_driver_location(position.coords.latitude,position.coords.longitude);
           if (marker) {
              // Marker already created - Move it
              marker.setPosition(newPoint);
            }
            else {
              // Marker does not exist - Create it
              marker = new google.maps.Marker({
                position: newPoint,
                map: map
              });
            }
            // Center the map on the new position
            map.setCenter(newPoint);
          });

          setTimeout(autoUpdate, 2000);
        }
        //Call the autoUpdate() function every 5 seconds

        autoUpdate();
      </script>
      <script type="text/javascript">
      function autoUpdate_driver_location(lat,lon)
      {
        var data = {
          'lat' : lat,
          'lon' : lon
        };
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>driver/user/update_driver_location/"+key,
            data: data,
            success: function(msg){
                console.log(msg);

            }
        });
      }
            function change_status(id){

              var status = $('.status_'+id).val();
              var key = '<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>';
              //alert(status)
              var data = {
                'id' : id,
                'driver_status' : status,
              };
              $.ajax({
                  type: "POST",
                  url: "<?php echo base_url();?>driver/user/update_status/"+key,
                  data: data,
                  success: function(msg){
                      //alert(msg);
                      $('.status_'+id).val(msg);
                  }
              });
            }


      </script>
      <?php
      if($details['driver_status'] == 1){?>
      <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY;?>&libraries=places&callback=initMap"
          async defer></script>
      <?php }else{?>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY;?>&libraries=places"
            async defer></script>
      <?php }?>

          <input id="origin" name="origin" type="hidden"/></div>
          <input id="destination" name="destination" type="hidden"/>

  </body>
</html>


<?php //$this->load->view("_include/footer"); ?>
