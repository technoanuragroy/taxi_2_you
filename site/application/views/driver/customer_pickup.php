<?php $this->load->view("_include/header_inner_driver"); ?>
	<script>
		$(document).ready(function(){
		  $('.nav_side_link li').append("<i class='fa fa-chevron-right'></i>");
		  $(".side_bar_btn i").click(function(){
		    $(this).toggleClass('fa-bars fa-times');
			$("aside").toggleClass("active_aside");
			//$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
		  });
		  /***********swipe function**************/
		 $('body').append("<div class='swipe_to_open'></div>");
		 $('body').append("<div class='swipe_to_close'></div>");
         $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="right") {
                           $("aside").addClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                           return false;
                      }
                      if (phase=="move" && direction =="left") {
                           $("aside").removeClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                           return false;
                      }
                  }
          });
					// $("#start_trip").click(function(){
					//  var rides_id = '<?php //echo $this->uri->segment(5);?>';
					//  var driver_id = '<?php //echo $this->uri->segment(4);?>';
					//  var security_key = '<?php //echo TAXI_APP_DRIVER_SECURITY_KEY; ?>';
					// 	 //alert(rider_id)
					// 	 var data = {
					// 		 'rides_id' : rides_id,
					// 	 };
					// 	 $.ajax({
					// 			 type: "POST",
					// 			 url: "<?php //echo base_url();?>driver/user/change_trip_status",
					// 			 data: data,
					// 			 success: function(msg){
					// 					 //alert(msg);
					// 					 var urls = '<?php //echo base_url();?>driver/user/driver_complete_trip/'+driver_id+'/'+rides_id+'/'+security_key
					// 					 if(msg == 'success'){
					// 						 window.location.href = urls;
					// 					 }
         //
					// 			 }
					// 	 });
				 // });



		});
	</script>
</head>

<body>
<?php //print_r($rides_details);?>
	<div class="cpmpleat_wrapper">
		<div class="sticky_header">

		</div>
		<header>
      <section class="header_inner">
				<p>
					<span><?php echo $rides_details['pickup_adress'];?></span>
					<span><?php echo $rides_details['drop_address'];?></span>
				</p>
        <span class="side_bar_btn">
          <i class="fa fa-bars"></i>
        </span>
        <!-- <h2>current location</h2> -->
      </section>
    </header>
    <?php $this->load->view('_include/sidebar_driver'); ?>
		<div class="top_map_wrap">
		<div id="map"></div>
		</div>
		<div class="customer_pickup">
			<p class="pick">
				<b>Pick</b>
				<name>
        <?php $rider_id = $rides_details['rider_id'];

        $first_name = $this->my_custom_functions->get_particular_field_value("tbl_rider", "first_name", " and id='" . $rider_id . "' ");
        $last_name = $this->my_custom_functions->get_particular_field_value("tbl_rider", "last_name", " and id='" . $rider_id . "' ");
        echo $first_name." ".$last_name;
        ?>

        </name>
			</p>
			<p class="confermation_tick">
				<img src="<?php echo base_url(); ?>driver_assets/images/tick.png" />
				Confirm Arrived
			</p>
			<?php
			$rides_id =  $this->uri->segment(5);
			$driver_id = $this->uri->segment(4);

			  ?>
			<div class="confirm_btn_grup">
				<a href="#" class="c_btn_short"><i class="fa fa-phone"></i> contact</a>
				<a href="#" class="c_btn_short"><i class="fa fa-times"></i> cancel</a>
				<a href="<?php echo base_url();?>driver/user/driver_complete_trip/<?php echo $driver_id ?>/<?php echo $rides_id ?>/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>" class="c_btn_long">start trip <i class="fa fa-long-arrow-right"></i></a>
			</div>
		</div>
	</div>
	<script>
  // Note: This example requires that you consent to location sharing when
  // prompted by your browser. If you see the error "The Geolocation service
  // failed.", it means you probably did not give permission for the browser to
  var map, infoWindow, marker;
  var markerArray = [];
  var stepDisplay;
  var styles = [
      {
        stylers: [
          { hue: "#00ffe6" },
          { saturation: -20 }
        ]
      },{
        featureType: "road",
        elementType: "geometry",
        stylers: [
          { lightness: 100 },
          { visibility: "simplified" }
        ]
      },{
        featureType: "road",
        elementType: "labels",
        stylers: [
          { visibility: "on" }
        ]
      }
    ];

  /////////////////LOAD MAP///////////////

  function initMap() {
		//alert()
  var directionsService = new google.maps.DirectionsService();
  var directionsRenderer = new google.maps.DirectionsRenderer({ polylineOptions: { strokeColor: "#062847" } });

  var origin = '<?php echo $rides_details['pickup_location_latitude'];?>';
  var destination = '<?php echo $rides_details['pickup_location_longitute'];?>';
  //alert(destination);

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: {lat: parseFloat(origin), lng: parseFloat(destination)},
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: styles,
    draggable:true,
    zoomControl: false,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: true
  });

  directionsRenderer.setMap(map);
  directionsRenderer.setOptions({ suppressMarkers: true });

  calculateAndDisplayRoute(directionsService, directionsRenderer,map);
}

function calculateAndDisplayRoute(directionsService, directionsRenderer, map) {

var icons = {
    start: new google.maps.MarkerImage(
      'http://server1/webdev/taxi2u/site/images/location_starts.png',
      new google.maps.Size(44, 32),
      new google.maps.Point(0, 0),
      new google.maps.Point(22, 32)),
    end: new google.maps.MarkerImage(
      'http://server1/webdev/taxi2u/site/images/location_ends.png',
      new google.maps.Size(44, 32),
      new google.maps.Point(0, 0),
      new google.maps.Point(22, 32))
  };

  var origin = '<?php echo $rides_details['pickup_adress'];?>';
  var destination = '<?php echo $rides_details['drop_address'];?>';
  //alert(destination)
  directionsService.route(
      {
        origin: {query: origin},
        destination: {query: destination},
        travelMode: google.maps.DirectionsTravelMode.DRIVING
      },
      function(response, status) {
        if (status === 'OK') {
          var leg = response.routes[0].legs[0];
          //alert(icons.start)
          //marker.setMap(null);
          makeMarker( leg.start_location, icons.start, "Start", map);
          makeMarker( leg.end_location, icons.end, 'End', map );
          directionsRenderer.setDirections(response);
          //calculateDistance(origin,destination);////////////Calculate fare Price
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
}

////////TO CHANGE START & END MARKER ICON FUNCTION////////////
function makeMarker(position, icon, title, map) {
  //alert(position)
         marker = new google.maps.Marker({
             position: position,
             map: map,
             icon: icon,
             title: title
         });
     }
    ////////TO CHANGE START & END MARKER ICON FUNCTION END////////////


  /////////Gps Update if Current Location Changed//////////////
  function autoUpdate() {
           //var marker =null;
           navigator.geolocation.getCurrentPosition(function(position) {
           var newPoint = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
           console.log(position.coords.latitude);
           console.log(position.coords.longitude);
           if (marker) {
              // Marker already created - Move it
              marker.setPosition(newPoint);
            }
            else {
              // Marker does not exist - Create it
              marker = new google.maps.Marker({
                position: newPoint,
                map: map
              });
            }
            // Center the map on the new position
            map.setCenter(newPoint);
          });

          setTimeout(autoUpdate, 2000);
        }
        //Call the autoUpdate() function every 5 seconds

        autoUpdate();

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY;?>&libraries=places&callback=initMap"
    async defer></script>
	<input id="origin" name="origin" type="hidden"/></div>
    <input id="destination" name="destination" type="hidden"/>
</body>
</html>
