<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/touchSwipe.min.js"></script>
	<script>
		$(document).ready(function(){
		  $('.nav_side_link li').append("<i class='fa fa-chevron-right'></i>");
		  $(".side_bar_btn i").click(function(){
		    $(this).toggleClass('fa-bars fa-times');
			$("aside").toggleClass("active_aside");
			//$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
		  });
		  /***********swipe function**************/
		 $('body').append("<div class='swipe_to_open'></div>");
		 $('body').append("<div class='swipe_to_close'></div>");
         $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="right") {
                           $("aside").addClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                           return false;
                      }
                      if (phase=="move" && direction =="left") {
                           $("aside").removeClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                           return false;
                      }
                  }
          });

		});
	</script>
</head>

<body>
	<div class="cpmpleat_wrapper">
		<div class="customer_notification_message">
			<div class="customer_notification_container">
				<span>
					<img src="images/speech_bouble.png" />
					<p>A nearby rider is requesting for taxi</p>
				</span>
			</div>
		</div>
		<div class="time_bar">
			<span>
				<p>
					<b>time - </b>
					<img src="images/clock.png" />
					30 seconds
				</p>
			</span>
		</div>
		<div class="address_bar">
			<address>
				<img src="images/map_pointer.png" />
				<p>
					<b>address</b>
					123 Herzl Street, 4th floor Shaviv Quarter 46000 Herzliya
				</p>
			</address>
		</div>
	</div>
</body>
</html>
