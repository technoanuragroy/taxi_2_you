<?php $this->load->view("_include/header_inner_driver"); ?>
	<script>
		$(document).ready(function(){
		  $('.nav_side_link li').append("<i class='fa fa-chevron-right'></i>");
		  $(".side_bar_btn i").click(function(){
		    $(this).toggleClass('fa-bars fa-times');
			$("aside").toggleClass("active_aside");
			//$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
		  });
		  /***********swipe function**************/
		 $('body').append("<div class='swipe_to_open'></div>");
		 $('body').append("<div class='swipe_to_close'></div>");
         $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="right") {
                           $("aside").addClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
                           return false;
                      }
                      if (phase=="move" && direction =="left") {
                           $("aside").removeClass("active_aside");
                           $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
                           return false;
                      }
                  }
          });

		});
	</script>
</head>

<body>
	<div class="cpmpleat_wrapper">
		<div class="sticky_header">
			<p>
				<span><?php echo $rides_details['pickup_adress'];?></span>
				<span><?php echo $rides_details['drop_address'];?></span>
			</p>
		</div>
		<div class="top_map_wrap">
		<div id="map"></div>
		</div>
		<div class="customer_pickup">
			<p class="pick">
				<b>Pick</b>
				<name>Ploni Almoni</name>
			</p>
			<p class="confermation_tick">
				<img src="<?php echo base_url(); ?>driver_assets/images/tick.png" />
				Confirm Arrived
			</p>
			<div class="confirm_btn_grup">
				<a href="#" class="c_btn_short"><i class="fa fa-phone"></i> contact</a>
				<a href="#" class="c_btn_short"><i class="fa fa-times"></i> cancel</a>
				<a href="#" class="c_btn_long">start trip <i class="fa fa-long-arrow-right"></i></a>
			</div>
		</div>
	</div>
	<script>
  // Note: This example requires that you consent to location sharing when
  // prompted by your browser. If you see the error "The Geolocation service
  // failed.", it means you probably did not give permission for the browser to
  var map, infoWindow, marker;
  var markerArray = [];
  var stepDisplay;
  var styles = [
      {
        stylers: [
          { hue: "#00ffe6" },
          { saturation: -20 }
        ]
      },{
        featureType: "road",
        elementType: "geometry",
        stylers: [
          { lightness: 100 },
          { visibility: "simplified" }
        ]
      },{
        featureType: "road",
        elementType: "labels",
        stylers: [
          { visibility: "on" }
        ]
      }
    ];

  /////////////////LOAD MAP///////////////

  function initMap() {
  var directionsService = new google.maps.DirectionsService();
  var directionsRenderer = new google.maps.DirectionsRenderer({ polylineOptions: { strokeColor: "#062847" } });

  var origin = '<?php echo $rides_details['pickup_location_latitude'];?>';
  var destination = '<?php echo $rides_details['pickup_location_longitute'];?>';
  //alert(destination);

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 7,
    center: {lat: parseFloat(origin), lng: parseFloat(destination)},
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: styles,
    draggable:false,
    zoomControl: false,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: true
  });

  directionsRenderer.setMap(map);
  directionsRenderer.setOptions({ suppressMarkers: true });

  calculateAndDisplayRoute(directionsService, directionsRenderer,map);
}

function calculateAndDisplayRoute(directionsService, directionsRenderer, map) {

var icons = {
    start: new google.maps.MarkerImage(
      'http://server1/webdev/taxi2u/site/images/location_starts.png',
      new google.maps.Size(44, 32),
      new google.maps.Point(0, 0),
      new google.maps.Point(22, 32)),
    end: new google.maps.MarkerImage(
      'http://server1/webdev/taxi2u/site/images/location_ends.png',
      new google.maps.Size(44, 32),
      new google.maps.Point(0, 0),
      new google.maps.Point(22, 32))
  };

  var origin = '<?php echo $rides_details['pickup_adress'];?>';
  var destination = '<?php echo $rides_details['drop_address'];?>';
  //alert(destination)
  directionsService.route(
      {
        origin: {query: origin},
        destination: {query: destination},
        travelMode: google.maps.DirectionsTravelMode.DRIVING
      },
      function(response, status) {
        if (status === 'OK') {
          var leg = response.routes[0].legs[0];
          //alert(icons.start)
          //marker.setMap(null);
         // makeMarker( leg.start_location, icons.start, "Start", map);
          //makeMarker( leg.end_location, icons.end, 'End', map );
          directionsRenderer.setDirections(response);
          calculateDistance(origin,destination);////////////Calculate fare Price
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
}

////////TO CHANGE START & END MARKER ICON FUNCTION////////////
function makeMarker(position, icon, title, map) {
  //alert(position)
         marker = new google.maps.Marker({
             position: position,
             map: map,
             icon: icon,
             title: title
         });
     }
    ////////TO CHANGE START & END MARKER ICON FUNCTION END////////////
  function toggleBounce() {
   if (marker.getAnimation() !== null) {
     marker.setAnimation(null);
   } else {
     marker.setAnimation(google.maps.Animation.BOUNCE);
   }
 }

///////////////Distance Calculation/////////////////
 function calculateDistance(origin,destination) {
    //alert(origin)
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
        {
            origins: [origin],
            destinations: [destination],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.IMPERIAL, // miles and feet.
            // unitSystem: google.maps.UnitSystem.metric, // kilometers and meters.
            avoidHighways: false,
            avoidTolls: false
        }, callback);
  }

  // get distance results by callback
  function callback(response, status) {

      if (status != google.maps.DistanceMatrixStatus.OK) {
          $('#result').html(status);
      } else {
          var origin = response.originAddresses[0];
          //alert(origin);
          var destination = response.destinationAddresses[0];
          if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
              $('#result').html("There are no roads between "  + origin + " and " + destination);
              $('#fare_price').text('0');
          } else {
              var distance = response.rows[0].elements[0].distance;
              var duration = response.rows[0].elements[0].duration;
              console.log(response.rows[0].elements[0].distance);
              var distance_in_kilo = distance.value / 1000; // the kilom
              var distance_in_mile = distance.value / 1609.34; // the mile
              var duration_text = duration.text;
              var duration_value = duration.value;
              //alert(distance);
              $('#in_mile').text(distance_in_mile.toFixed(2));
              $('#in_kilo').text(distance_in_kilo.toFixed(2)+"km");
              $('#duration_text').text(duration_text);
              $('#duration_value').text(duration_value);
              $('#from').text(origin);
              $('#to').text(destination);
              //$('#result').html("");
              ///////CALCULATE PRICE///////////////////
              $('#fare_price').text(Number(distance_in_kilo * 7).toFixed(2));
          }
      }
  }

  function initMap() {
   var lat = '<?php echo $rides_details['pickup_location_latitude'];?>';
   var lng = '<?php echo $rides_details['pickup_location_longitute'];?>'; 

  var myLatLng = {lat: parseFloat(lat), lng: parseFloat(lng)};
  var center = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 7,
    center: myLatLng,
    draggable:false,
    zoomControl: false,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
  });

  var marker = new google.maps.Marker({
    position: center,
    map: map,
    title: 'Hello World!'
  });

   var circle = new google.maps.Circle({
                center: center,
                map:map,
                radius:100000,
                strokeColor: "red",
                strokeOpacity:0.8,
                strokeWeight: 2,
                fillColor: "red"
            });
    circle.bindTo('center',marker,'position');
}

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY;?>&libraries=places&callback=initMap"
    async defer></script>
	<input id="origin" name="origin" type="hidden"/></div>
    <input id="destination" name="destination" type="hidden"/>
</body>
</html>
