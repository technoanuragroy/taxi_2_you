<?php $this->load->view("_include/header_inner_driver"); ?>

    <script type="text/javascript" >
        $(document).ready(function(){
            $("#rider_login").validationEngine({promptPosition : "bottomRight", scroll: true});
        });

        setTimeout(function() {
            $('.s_message').hide('slow');
        }, 5000);

        setTimeout(function() {
            $('.e_message').hide('slow');
        }, 5000);

        function isNumber(evt) {

            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>

    <div class="cpmpleat_wrapper">
  		<div class="login_section_wrapper">
  			<div class="logo_wrapper">
  				<a href="#"><img src="<?php echo base_url(); ?>driver_assets/images/main-logo.png" /></a>
  			</div>
			<h4 class="headMain">driver login</h4>
  			<div class="login_form-section">
  				<?php echo form_open('', array('id' => 'rider_login', 'class' => 'formFields')); ?>

          <div class="invalid">
              <?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
              <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
          </div>
          <div class="sucess">
              <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
          </div>

  					<!-- <input type="text" class="username usr" placeholder="User name"> -->
            <?php echo form_input(array('name'=> 'username','id' => 'username','class'=>'username usr validate[required, custom[email]]')); ?>
  					<!-- <input type="password" class="pass usr" placeholder="Password"> -->
            <?php echo form_input(array('type' => 'password', 'name'=> 'password','id' => 'password','value'=> '','class'=>'pass usr validate[required]')); ?>
  					<!-- <input type="submit" class="submitButton" value="sign in"> -->
            <input type="submit" name="submit" value="sign in" class="submitButton">
  					<div class="register_wrap">
  						<a href="<?php echo base_url(); ?>driver/main/index/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>" class="register">Register</a>
  						<a href="<?php echo base_url(); ?>driver/main/forgot_password/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>" class="F_pass">Forget password</a>
  					</div>
  				<?php echo form_close(); ?>
  			</div>
  		</div>




  	<div class="rider_version_enter">
  		<a href="<?php echo base_url(); ?>rider/main/rider_login/<?php echo TAXI_APP_DRIVER_SECURITY_KEY ?>">Enter to Rider Version <img src="<?php echo base_url(); ?>driver_assets/images/right_arrow.png"></a>
  	</div>

  	</div>

<?php //$this->load->view("_include/footer"); ?>
