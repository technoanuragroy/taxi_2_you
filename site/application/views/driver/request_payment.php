<?php $this->load->view("_include/header_inner_driver"); ?>
  <script>
    $(document).ready(function(){
      $('.nav_side_link li').append("<i class='fa fa-chevron-right'></i>");
      $(".side_bar_btn i").click(function(){
      $(this).toggleClass('fa-bars fa-times');
      $("aside").toggleClass("active_aside");
      //$(".full_page_map_wrap").toggleClass("active_full_page_map_wrap");
      });
      /***********swipe function**************/
     $('body').append("<div class='swipe_to_open'></div>");
     $('body').append("<div class='swipe_to_close'></div>");
     $(".swipe_to_open, .side_slide, .swipe_to_close").swipe({
        swipeStatus:function(event, phase, direction, distance, duration, fingers)
          {
            if (phase=="move" && direction =="right") {
               $("aside").addClass("active_aside");
               $(".side_bar_btn i").addClass('fa-times').removeClass('fa-bars');
               return false;
            }
            if (phase=="move" && direction =="left") {
               $("aside").removeClass("active_aside");
               $(".side_bar_btn i").addClass('fa-bars').removeClass('fa-times');
               return false;
            }
          }
      });

      $("#request_amount").click(function(){
       var rides_id = '<?php echo $this->uri->segment(5);?>';
       var driver_id = '<?php echo $this->uri->segment(4);?>';
       var security_key = '<?php echo TAXI_APP_DRIVER_SECURITY_KEY; ?>';
      	 //alert(rider_id)
      	 var data = {
      		 'rides_id' : rides_id,
      	 };
      	 $.ajax({
      			 type: "POST",
      			 url: "<?php echo base_url();?>driver/user/requestAmount/"+security_key,
      			 data: data,
      			 success: function(msg){
      					 //alert(msg);
      					 //var urls = '<?php echo base_url();?>driver/user/payment_request/'+driver_id+'/'+rides_id+'/'+security_key
                 var urls = '<?php echo base_url();?>driver/user/dashboard/'+security_key ;
      					 if(msg == 'success'){
      						 window.location.href = urls;
      					 }
                 else {
                   console.log('something went wrong..');
                 }

      			 }
      	 });
     });

    });
  </script>
</head>

<body>
  <div class="cpmpleat_wrapper">
    <div class="payment_wrap">
      <a href="javascript:" id="request_amount" class="request">Request Amount</a>

      <a href="#" class="amount">Amount- ₪<?php echo $request_payment['amount']; ?></a>
    </div>
  </div>
</body>
</html>
