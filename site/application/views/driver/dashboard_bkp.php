<?php $this->load->view("_include/header_inner"); ?>
<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
      height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
      height: 100%;
      margin: 0;
      padding: 0;
    }
    .controls {
      margin-top: 10px;
      border: 1px solid transparent;
      border-radius: 2px 0 0 2px;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      height: 32px;
      outline: none;
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #origin-input,
    #destination-input {
      background-color: #fff;
      font-family: Roboto;
      font-size: 15px;
      font-weight: 300;
      margin-left: 12px;
      padding: 0 11px 0 13px;
      text-overflow: ellipsis;
      width: 200px;
    }

    #origin-input:focus,
    #destination-input:focus {
      border-color: #4d90fe;
    }

    #mode-selector {
      color: #fff;
      background-color: #4d90fe;
      margin-left: 12px;
      padding: 5px 11px 0px 11px;
    }

    #mode-selector label {
      font-family: Roboto;
      font-size: 13px;
      font-weight: 300;
    }

  </style>
    <script type="text/javascript" >
        $(document).ready(function(){
        });
    </script>

    <div class="container">
        <div class="row">
            <h2><span class="triangle"><img src="images/symbol_triangle.png" alt=""></span>Private Area</h2>
            <?php if ($this->session->userdata('rider_id')) {?>
                  <a class="nav-link" href="<?php echo base_url(); ?>rider/user/edit_profile">Edit Profile</a>
                  <a class="nav-link" href="<?php echo base_url(); ?>rider/user/change_password">Change Password</a>
                  <a class="nav-link" href="<?php echo base_url(); ?>rider/main/rider_logout">Logout</a>
            <?php }?>
         <div class="mainContent">
                Welcome, <?php echo $details['name']; ?>
         </div>

        </div>
    </div>
    <input id="origin-input" class="controls" type="text" placeholder="Enter an origin location">

    <input id="destination-input" class="controls" type="text" placeholder="Enter a destination location">

    <div id="map"></div>
    <script>
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.
      var map, infoWindow, marker;
      var markerArray = [];
      var stepDisplay;
      function initMap() {
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var directionsService = new google.maps.DirectionsService;

        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 31.0461, lng: 34.8516},
          zoom: 7
        });
        infoWindow = new google.maps.InfoWindow();
        var iconBase = 'http://server1/webdev/taxi2u/site/images/marker.png';
        marker = new google.maps.Marker({
        map: map,
        draggable: true,
        icon: iconBase,
        //animation: google.maps.Animation.DROP,
        position: {lat: 31.0461, lng: 34.8516}
        });
        marker.addListener('click', toggleBounce);

        directionsDisplay.setMap(map);

        /////////////////////AUTOCOMPLETE///////////////////////
        var originInput = document.getElementById('origin-input');
        var destinationInput = document.getElementById('destination-input');
        var from_places = new google.maps.places.Autocomplete(originInput);
        var to_places = new google.maps.places.Autocomplete(destinationInput);

        /////////////AUTOCOMPLETE PLACE CHANGE FROM ADDRESS//////////////////////
        google.maps.event.addListener(from_places, 'place_changed', function () {
            var from_place = from_places.getPlace();
            var from_address = from_place.formatted_address;
            $('#origin').val(from_address);
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        });

        /////////////AUTOCOMPLETE PLACE CHANGE TO ADDRESS//////////////////////
        google.maps.event.addListener(to_places, 'place_changed', function () {
            var to_place = to_places.getPlace();
            var to_address = to_place.formatted_address;
            $('#destination').val(to_address);
            var origin = $('#origin').val();
            var destination = $('#destination').val();

            /////////CALCULATE DISTANCE///////////////////////////
            calculateDistance(origin,destination);
            /////////CALCULATE ROUTE/////////////////////////////
            calculateAndDisplayRoute(directionsService, directionsDisplay);
          });
      }
      function toggleBounce() {
       if (marker.getAnimation() !== null) {
         marker.setAnimation(null);
       } else {
         marker.setAnimation(google.maps.Animation.BOUNCE);
       }
     }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
          'Error: The Geolocation service failed.' :
          'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        for (i = 0; i < markerArray.length; i++) {
            markerArray[i].setMap(null);
          }
       var start = $('#origin').val();
       var end = $('#destination').val();
       directionsService.route({
         origin: start,
         destination: end,
         travelMode: 'DRIVING'
       }, function(response, status) {
         if (status === 'OK') {
           directionsDisplay.setDirections(response);
           //showSteps(response);
         } else {
           window.alert('Directions request failed due to ' + status);
         }
       });
     }

     function showSteps(directionResult) {
      // For each step, place a marker, and add the text to the marker's
      // info window. Also attach the marker to an array so we
      // can keep track of it and remove it when calculating new
      // routes.
      var myRoute = directionResult.routes[0].legs[0];

      for (var i = 0; i < myRoute.steps.length; i++) {
          var marker = new google.maps.Marker({
            position: myRoute.steps[i].start_point,
            map: map
          });
          attachInstructionText(marker, myRoute.steps[i].instructions);
          markerArray[i] = marker;
      }
    }

    function attachInstructionText(marker, text) {
      //alert(text)
      google.maps.event.addListener(marker, 'click', function() {
      infoWindow.setContent(text);
      infoWindow.open(map, marker);
    });
  }

     function calculateDistance(origin,destination) {
        //alert(origin)
        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.IMPERIAL, // miles and feet.
                // unitSystem: google.maps.UnitSystem.metric, // kilometers and meters.
                avoidHighways: false,
                avoidTolls: false
            }, callback);
      }

      // get distance results by callback
      function callback(response, status) {

          if (status != google.maps.DistanceMatrixStatus.OK) {
              $('#result').html(status);
          } else {
              var origin = response.originAddresses[0];
              var destination = response.destinationAddresses[0];
              if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
                  $('#result').html("There are no roads between "  + origin + " and " + destination);
              } else {
                  var distance = response.rows[0].elements[0].distance;
                  var duration = response.rows[0].elements[0].duration;
                  console.log(response.rows[0].elements[0].distance);
                  var distance_in_kilo = distance.value / 1000; // the kilom
                  var distance_in_mile = distance.value / 1609.34; // the mile
                  var duration_text = duration.text;
                  var duration_value = duration.value;
                  //alert(distance);
                  $('#in_mile').text(distance_in_mile.toFixed(2));
                  $('#in_kilo').text(distance_in_kilo.toFixed(2));
                  $('#duration_text').text(duration_text);
                  $('#duration_value').text(duration_value);
                  $('#from').text(origin);
                  $('#to').text(destination);

                  ///////CALCULATE PRICE///////////////////
                  //$('#fare_price').text(Number(distance_in_kilo * 15).toFixed(2));
              }
          }
      }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY;?>&libraries=places&callback=initMap"
        async defer></script>
        <!-- <div id="result">
        <ul class="list-group">
        	<li class="list-group-item d-flex justify-content-between align-items-center">Distance In Mile :<span id="in_mile"></span></li>
        	<li class="list-group-item d-flex justify-content-between align-items-center">Distance is Kilo:<span id="in_kilo"></span></li>
        	<li class="list-group-item d-flex justify-content-between align-items-center">IN MINUTES:<span id="duration_value"></span></li>
          <li class="list-group-item d-flex justify-content-between align-items-center">IN Text:<span id="duration_text"></span></li>
        	<li class="list-group-item d-flex justify-content-between align-items-center">FROM:<span id="from"></span></li>
        	<li class="list-group-item d-flex justify-content-between align-items-center">TO:<span id="to"></span></li>
        </ul>
        </div> -->
        <!-- /////////////STORE INFORAMTION IN HIDDEN FIELD TO CHECK CALCULATE ROUTE/////////// -->
        <input id="origin" name="origin" type="hidden"/></div>
        <input id="destination" name="destination" type="hidden"/>
    </div>

    <div>Fare Price</div><span id="fare_price"></span>

    <div>Add Payment Method</div></span>

  </body>
</html>


<?php //$this->load->view("_include/footer"); ?>
