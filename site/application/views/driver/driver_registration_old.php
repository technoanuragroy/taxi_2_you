<?php $this->load->view("_include/header_inner_old"); ?>

    <script type="text/javascript" >

        $(document).ready(function() {
            $("#driver_registration").validationEngine({promptPosition : "bottomLeft", scroll: true});

            setTimeout(function() {
                $('.s_message').hide('slow');
            }, 5000);

            setTimeout(function() {
                $('.e_message').hide('slow');
            }, 5000);

            $('#datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange:"-100:+0",
                dateFormat: 'yy-mm-dd'
            });

            //////DISABLE DATEPICKER KEYPRESS ENTRY  /////
            $('#datepicker').keypress(function(event) {
                event.preventDefault();
            });

            ////////  ------  #END  #END  #END  ------   /////////
        });
    </script>

    <div class="container">
        <div class="row">
            <h2><span class="triangle"><img src="<?php echo base_url(); ?>images/symbol_triangle.png" alt=""></span><?php echo $title; ?></h2>

            <div class="inner_form">

                <?php echo form_open_multipart('', array('id' => 'driver_registration', 'class' => 'formFields')); ?>

                        <div class="invalid">
                            <?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
                            <?php if($this->session->flashdata("e_message")) { echo '<p class="">'.$this->session->flashdata("e_message").'</p>'; } ?>
                        </div>
                        <div class="sucess">
                            <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                        </div>
                        <h2>Step 1</h2>
                        <div class="form-group">
                            <label for="Name"><span class="symbolcolor">*</span>Name</label>
                            <?php echo form_input(array('name'=> 'name','id' => 'name','value'=>  set_value('name'),'class'=>'form-control validate[required]')); ?>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="mobile"><span class="symbolcolor">*</span>Mobile</label>
                                <?php echo form_input(array('name'=> 'mobile','id' => 'mobile','value'=> set_value('mobile'),'class'=>'form-control validate[required]')); ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4"><span class="symbolcolor">*</span>Email</label>
                                <?php echo form_input(array('type' => 'email', 'name'=> 'email','id' => 'email','value'=> set_value('email'),'class'=>'form-control validate[required,custom[email]]')); ?>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Country"><span class="symbolcolor">*</span>Country</label>
                                <?php
                                    $countries = $this->my_custom_functions->get_country_dropdown_data();
                                    echo form_dropdown('country', $countries, "", 'class="custom-select mr-sm-2 validate[required]" id="country"');
                                ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="login id"><span class="symbolcolor">*</span>Upload Photo </label>
                                <?php echo form_upload(array('name'=> 'formal_id','id' => 'formal_id','class'=>'form-control-file')); ?>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="password"><span class="symbolcolor">*</span>Password</label>
                                <?php echo form_input(array('type' => 'password', 'name'=> 'password','id' => 'password','value'=> set_value('password'),'class'=>'form-control validate[required]'));?>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="confirm password"><span class="symbolcolor">*</span>Confirm Password</label>
                                <input type="password" class="form-control validate[required,equals[password]]" name="confirm_password" id="confirm_password" value="">
                            </div>
                        </div>
                        <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="promocode"><span class="symbolcolor">*</span>Promocode</label>
                            <?php echo form_input(array('name'=> 'promocode','id' => 'promocode','value'=> set_value('promocode'),'class'=>'form-control validate[required,maxSize[20]]')); ?>
                        </div>
                        </div>
                        <br><br>

                        <h2>Step 2</h2>
                        <div id="spep_two">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="id_proof"><span class="symbolcolor">*</span>Identification Proof </label>
                              <?php echo form_upload(array('name'=> 'id_proof','id' => 'id_proof','class'=>'form-control-file')); ?>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="driving_licence"><span class="symbolcolor">*</span>Driving Licence </label>
                              <?php echo form_upload(array('name'=> 'driving_licence','id' => 'driving_licence','class'=>'form-control-file')); ?>
                            </div>
                        </div>
                      </div>

                      <br><br>

                      <h2>Step 3</h2>
                      <div id="spep_three">
                      <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="vehicle_name"><span class="symbolcolor">*</span>Vehicle Name</label>
                            <?php echo form_input(array('name'=> 'vehicle_name','id' => 'vehicle_name','value'=> set_value('promocode'),'class'=>'form-control validate[required]')); ?>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="vehicle_make"><span class="symbolcolor">*</span>Vehicle Make</label>
                            <?php echo form_input(array('name'=> 'vehicle_make','id' => 'vehicle_make','value'=> set_value('vehicle_make'),'class'=>'form-control validate[required]')); ?>
                          </div>
                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="vehicle_model"><span class="symbolcolor">*</span>Vehicle Model</label>
                            <?php echo form_input(array('name'=> 'vehicle_model','id' => 'vehicle_name','value'=> set_value('vehicle_model'),'class'=>'form-control validate[required]')); ?>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="vehicle_make"><span class="symbolcolor">*</span>Vehicle Color</label>
                            <?php echo form_input(array('name'=> 'vehicle_color','id' => 'vehicle_color','value'=> set_value('vehicle_color'),'class'=>'form-control validate[required] jscolor')); ?>
                          </div>
                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="vehicle_plate_no"><span class="symbolcolor">*</span>Vehicle Plate No.</label>
                            <?php echo form_input(array('name'=> 'vehicle_plate_no','id' => 'vehicle_plate_no','value'=> set_value('vehicle_plate_no'),'class'=>'form-control validate[required]')); ?>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="vehicle_photo"><span class="symbolcolor">*</span>Vehicle PhotoLicence </label>
                            <?php echo form_upload(array('name'=> 'vehicle_photo','id' => 'vehicle_photo','class'=>'form-control-file')); ?>
                          </div>
                      </div>
                    </div>

                        <input type="submit" name="submit" value="Register" class="btn_1 btn">

                <?php echo form_close(); ?>

            </div>
        </div>
    </div>

<?php //$this->load->view("_include/footer"); ?>
