<?php $this->load->view("_include/header_inner"); ?>
<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
      height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
      height: 100%;
      margin: 0;
      padding: 0;
    }
    .controls {
      margin-top: 10px;
      border: 1px solid transparent;
      border-radius: 2px 0 0 2px;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      height: 32px;
      outline: none;
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #origin-input,
    #destination-input {
      background-color: #fff;
      font-family: Roboto;
      font-size: 15px;
      font-weight: 300;
      margin-left: 12px;
      padding: 0 11px 0 13px;
      text-overflow: ellipsis;
      width: 200px;
    }

    #origin-input:focus,
    #destination-input:focus {
      border-color: #4d90fe;
    }

    #mode-selector {
      color: #fff;
      background-color: #4d90fe;
      margin-left: 12px;
      padding: 5px 11px 0px 11px;
    }

    #mode-selector label {
      font-family: Roboto;
      font-size: 13px;
      font-weight: 300;
    }

  </style>
    <script type="text/javascript" >
        $(document).ready(function(){
        });
    </script>

    <div class="container">
        <div class="row">
            <h2><span class="triangle"><img src="images/symbol_triangle.png" alt=""></span>DRIVER DASHBOARD</h2>
            <?php if ($this->session->userdata('driver_id')) {?>
                  <a class="nav-link" href="<?php echo base_url(); ?>driver/user/edit_profile">Edit Profile</a>
                  <a class="nav-link" href="<?php echo base_url(); ?>driver/user/change_password">Change Password</a>
                  <a class="nav-link" href="<?php echo base_url(); ?>driver/main/driver_logout">Logout</a>
            <?php }?>
         <div class="mainContent">
                Welcome, <?php echo $details['name']; ?>
         </div>

        </div>
    </div>
    <!-- <input id="origin-input" class="controls" type="text" placeholder="Enter an origin location">

    <input id="destination-input" class="controls" type="text" placeholder="Enter a destination location"> -->

    <div id="map"></div>

    <script>
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      var map, infoWindow, marker;
      var markerArray = [];
      var stepDisplay;
      var styles = [
          {
            stylers: [
              { hue: "#00ffe6" },
              { saturation: -20 }
            ]
          },{
            featureType: "road",
            elementType: "geometry",
            stylers: [
              { lightness: 100 },
              { visibility: "simplified" }
            ]
          },{
            featureType: "road",
            elementType: "labels",
            stylers: [
              { visibility: "on" }
            ]
          }
        ];

      function initMap() {
        ////Set From Address On Page Load////
        GetAddress(31.0461,34.8516);

        //var directionsDisplay = new google.maps.DirectionsRenderer;
        var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
        var directionsService = new google.maps.DirectionsService;

        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 31.0461, lng: 34.8516},
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: styles
        });

        infoWindow = new google.maps.InfoWindow();
        var iconBase = 'http://server1/webdev/taxi2u/site/images/marker.png';
        marker = new google.maps.Marker({
        map: map,
        draggable: false,
        icon: iconBase,
        //animation: google.maps.Animation.DROP,
        position: {lat: 31.0461, lng: 34.8516}
        });
        //marker.addListener('click', toggleBounce);

        //////Geeolocation Tracking Code Starts here///////////
       if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(function(position) {

           ////If Geolocation Found Set Current Address From geo location////
           GetAddress(position.coords.latitude,position.coords.longitude);

           var pos = {
             lat: position.coords.latitude,
             lng: position.coords.longitude
           };
           infoWindow.setPosition(pos);
           infoWindow.setContent('Location found.');
           infoWindow.open(map);
           map.setCenter(pos);
         }, function() {
           handleLocationError(true, infoWindow, map.getCenter());
         });
         } else {
           // Browser doesn't support Geolocation
           handleLocationError(false, infoWindow, map.getCenter());
         }
         //////Geeolocation Tracking Code Ends here///////////

         ///////////Set Map Using Direction/////////////////
         directionsDisplay.setMap(map);
         directionsDisplay.setOptions({
          polylineOptions: {
                      strokeWeight: 5,
                      strokeOpacity: 1,
                      strokeColor:  'black'
                  }
          });

        /////////////////////AUTOCOMPLETE///////////////////////
        var originInput = document.getElementById('origin-input');
        var destinationInput = document.getElementById('destination-input');
        var from_places = new google.maps.places.Autocomplete(originInput);
        var to_places = new google.maps.places.Autocomplete(destinationInput);


        google.maps.event.addListener(marker, 'dragend', function () {
          //map.setCenter(this.getPosition()); // Set map center to marker position
          //GetAddress(this.getPosition().lat(),this.getPosition().lng());
          //updatePosition(this.getPosition().lat(), this.getPosition().lng()); // update position display
      });

        google.maps.event.addListener(map, 'dragend', function () {
            //marker.setPosition(this.getCenter()); // set marker position to map center
            //GetAddress(this.getCenter().lat(),this.getCenter().lng());
        });
        /////////////AUTOCOMPLETE PLACE CHANGE FROM ADDRESS//////////////////////
        google.maps.event.addListener(from_places, 'place_changed', function () {
            var from_place = from_places.getPlace();
            var from_address = from_place.formatted_address;
            $('#origin').val(from_address);
            var origin = $('#origin').val();
            var destination = $('#destination').val();
            if(destination != ''){
            /////////CALCULATE DISTANCE///////////////////////////
            calculateDistance(origin,destination);
            calculateAndDisplayRoute(directionsService, directionsDisplay);
            }
        });

        /////////////AUTOCOMPLETE PLACE CHANGE TO ADDRESS//////////////////////
        google.maps.event.addListener(to_places, 'place_changed', function () {
            var to_place = to_places.getPlace();
            var to_address = to_place.formatted_address;
            $('#destination').val(to_address);
            var origin = $('#origin').val();
            var destination = $('#destination').val();
            if(origin !='' && destination != ''){
            /////////CALCULATE DISTANCE///////////////////////////
            calculateDistance(origin,destination);
            /////////CALCULATE ROUTE/////////////////////////////
            calculateAndDisplayRoute(directionsService, directionsDisplay);
           }
          });

      ////////////////End Init Function(Map)/////////////////////
      }

      ///////////////GET ADDRESS FROM LAT LNG///////////////////
      function GetAddress(lat,lng) {
       //marker.setMap(null);
       var latlng = new google.maps.LatLng(lat, lng);
       var geocoder = new google.maps.Geocoder();
       geocoder.geocode({ 'latLng': latlng }, function (results, status) {
           if (status == google.maps.GeocoderStatus.OK) {
               if (results[1]) {
                 $('#origin-input').val(results[1].formatted_address);
                 $('#origin').val(results[1].formatted_address);
               }
           }
       });
      }

      ///////////Error Handler for Geolocation///////////////////
      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
       infoWindow.setPosition(pos);
       infoWindow.setContent(browserHasGeolocation ?
                             'Error: The Geolocation service failed.' :
                             'Error: Your browser doesn\'t support geolocation.');
       infoWindow.open(map);
     }
     ///////////Error Handler for Geolocation///////////////////

     /////////Gps Update if Current Location Changed//////////////
       function autoUpdate() {
         navigator.geolocation.getCurrentPosition(function(position) {
         var newPoint = new google.maps.LatLng(position.coords.latitude,
                                            position.coords.longitude);
         if (marker) {
            // Marker already created - Move it
            marker.setPosition(newPoint);
          }
          else {
            // Marker does not exist - Create it
            marker = new google.maps.Marker({
              position: newPoint,
              map: map
            });
          }
          // Center the map on the new position
          map.setCenter(newPoint);
        });

      }
      // Call the autoUpdate() function every 5 seconds
      //setTimeout(autoUpdate, 5000);
      //setTimeout(function(){autoUpdate();}, 2000);


      function toggleBounce() {
       if (marker.getAnimation() !== null) {
         marker.setAnimation(null);
       } else {
         marker.setAnimation(google.maps.Animation.BOUNCE);
       }
     }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
          'Error: The Geolocation service failed.' :
          'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
      // for (i = 0; i < markerArray.length; i++) {
      //     markerArray[i].setMap(null);
      //   }

      //////////////FROM ICON & TWO ICON////////////////////////
       var icons = {
       start: new google.maps.MarkerImage(
         'http://server1/webdev/taxi2u/site/images/location_starts.png',
         new google.maps.Size(44, 32),
         new google.maps.Point(0, 0),
         new google.maps.Point(22, 32)),
       end: new google.maps.MarkerImage(
         'http://server1/webdev/taxi2u/site/images/location_ends.png',
         new google.maps.Size(44, 32),
         new google.maps.Point(0, 0),
         new google.maps.Point(22, 32))
     };
     //////////////FROM ICON & TWO ICON////////////////////////

       var start = $('#origin').val();
       var end = $('#destination').val();
       directionsService.route({
         origin: start,
         destination: end,
         travelMode: google.maps.DirectionsTravelMode.DRIVING
       }, function(response, status) {
         if (status === 'OK') {

           ////////TO CHANGE START & END MARKER ICON////////////
           var leg = response.routes[0].legs[0];
           //alert(leg.end_location)
           marker.setMap(null);
           makeMarker( leg.start_location, icons.start, "Start", map);
           makeMarker( leg.end_location, icons.end, 'End', map );
           directionsDisplay.setDirections(response);
           //showSteps(response);
         } else {
           window.alert('Directions request failed due to ' + status);
         }
       });
     }

     ////////TO CHANGE START & END MARKER ICON FUNCTION////////////
     function makeMarker(position, icon, title, map) {
         marker = new google.maps.Marker({
             position: position,
             map: map,
             icon: icon,
             title: title
         });
     }
    ////////TO CHANGE START & END MARKER ICON FUNCTION END////////////

     function showSteps(directionResult) {
      // For each step, place a marker, and add the text to the marker's
      // info window. Also attach the marker to an array so we
      // can keep track of it and remove it when calculating new
      // routes.
      var myRoute = directionResult.routes[0].legs[0];
      for (var i = 0; i < myRoute.steps.length; i++) {
          var marker = new google.maps.Marker({
            position: myRoute.steps[i].start_point,
            map: map
          });
          attachInstructionText(marker, myRoute.steps[i].instructions);
          markerArray[i] = marker;
        }
      }

      function attachInstructionText(marker, text) {
        //alert(text)
        google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(text);
        infoWindow.open(map, marker);
      });
    }

    ///////////////Distance Calculation/////////////////
     function calculateDistance(origin,destination) {
        //alert(origin)
        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.IMPERIAL, // miles and feet.
                // unitSystem: google.maps.UnitSystem.metric, // kilometers and meters.
                avoidHighways: false,
                avoidTolls: false
            }, callback);
      }

      // get distance results by callback
      function callback(response, status) {

          if (status != google.maps.DistanceMatrixStatus.OK) {
              $('#result').html(status);
          } else {
              var origin = response.originAddresses[0];
              var destination = response.destinationAddresses[0];
              if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
                  $('#result').html("There are no roads between "  + origin + " and " + destination);
                  $('#fare_price').text('0');
              } else {
                  var distance = response.rows[0].elements[0].distance;
                  var duration = response.rows[0].elements[0].duration;
                  console.log(response.rows[0].elements[0].distance);
                  var distance_in_kilo = distance.value / 1000; // the kilom
                  var distance_in_mile = distance.value / 1609.34; // the mile
                  var duration_text = duration.text;
                  var duration_value = duration.value;
                  //alert(distance);
                  $('#in_mile').text(distance_in_mile.toFixed(2));
                  $('#in_kilo').text(distance_in_kilo.toFixed(2));
                  $('#duration_text').text(duration_text);
                  $('#duration_value').text(duration_value);
                  $('#from').text(origin);
                  $('#to').text(destination);
                  //$('#result').html("");
                  ///////CALCULATE PRICE///////////////////
                  $('#fare_price').text(Number(distance_in_kilo * 7).toFixed(2));
              }
          }
      }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAP_API_KEY;?>&libraries=places&callback=initMap"
        async defer></script>

        <input id="origin" name="origin" type="hidden"/></div>
        <input id="destination" name="destination" type="hidden"/>
    </div>

  </body>
</html>


<?php //$this->load->view("_include/footer"); ?>
