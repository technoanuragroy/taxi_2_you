<?php $this->load->view("admin/include/header"); ?>
<script type="text/javascript" >

</script>
<style>
    .fisrtRow{
       height:auto;
        padding: 20px 20px 10px 20px;
        margin-top: 20px;

    }
    .fisrtRowB1{
        background: #52B7A5;
        height:200px;
        padding: 5px;
        text-align: center;
        color: #fff;
    }
    .fisrtRowB1 h1{
      font-size: 24px;
    }
    .fisrtRowB2{
        background: #3DAAFF;
        height:200px;
        padding: 5px;
        color: #fff;
        text-align: center;
    }
    .fisrtRowB2 h1{
      font-size: 24px;
    }
    .fisrtRowB3{
        background: #52B7A5;
        height:200px;
        padding: 5px;
        text-align: center;
        color: #fff;

    }
      .fisrtRowB3 h1{
        font-size: 24px;
      }
    .Boxpackheading{
        font-weight: 700;
        background: #B8CAD8;
        height: auto;
        padding:15px 0;
        font-size: 22px !important;
        color: #333;
    }
</style>

<div class="ContainerList">

    <div class="contentHeader">
        <div style="font-size: 17px; color: #72838b; float: left; margin-top: 10px; padding-right: 5px;">
            <span class="Iconheading">
                <img src="<?php echo base_url();?>images/advertisersIcon.png" alt="">
            </span>
        </div>
        <div class="name_container">
            <div class="pageheadingContainer">
                <h1 class="Heading03"><?php echo $admin_details['name']; ?></h1>
            </div>
        </div>
    </div>

    <div class="ListDataContainer">
        <div class="invalid">
            <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
        </div>
        <div class="sucess">
            <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
        </div>

    </div>
</div>

<?php $this->load->view("admin/include/footer"); ?>
