<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function(){
        $("#addrider_form").validationEngine({promptPosition : "bottomLeft", scroll: true});
    });
</script>

<?php echo form_open('', array('name' => 'addrider_form', 'id' => 'addrider_form', 'onsubmit' => 'isSubmit(); return false;')); ?>

           <div class="ContainerList">
                <div class="contentHeader">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
                        <h3 class="Heading03 MBheadng03">Manage Rider</h3>
                    </div>
                </div>
             <div class="ListDataContainer">
                 <div class="form-group full-col">
            <table class="data-table tablesorter" id="myTable">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Country</th>
                        <th>Status</th>
                        <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($riders)) {
                       foreach($riders as $rider) {
                ?>
                            <tr>

                                <td><?php echo  $rider['first_name']; ?></td>
                                <td><?php echo  $rider['last_name']; ?></td>
                                <td><?php echo $rider['phone']; ?></td>
                                <td><?php echo $rider['email']; ?></td>
                                <td><?php
                                $contry = $this->my_custom_functions->get_particular_field_value('tbl_country', 'name', 'and country_id="' . $rider['country_id'] . '"');
                                echo $contry;
                                ?>
                                </td>
                                <td><?php
                                if($rider['status']==0){
                                    $ride='Inactive';
                                }else{
                                    $ride='Active';
                                }

                                echo $ride; ?>
                                </td>
                                <td align="center">
                                    <a href="<?php echo base_url().'admin/user/edit_rider/'.$rider['id']; ?>" title="Edit this rider"><img src="<?php echo base_url(); ?>images/edit.png" alt="Edit"></a>
                                </td>

                          <?php  } } else { ?>

                           <tr>
                                <td colspan="7">No other user found</td>
                            </tr>
                <?php  }  ?>

               </tbody>
            </table>
        </div>


                </div>
            </div>

   <?php echo form_close(); ?>



<?php $this->load->view("admin/include/footer"); ?>
