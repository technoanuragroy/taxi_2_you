<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function(){
        $("#addrider_form").validationEngine({promptPosition : "bottomLeft", scroll: true});
    });
</script>

<?php echo form_open('', array('name' => 'addrider_form', 'id' => 'addrider_form', 'onsubmit' => 'isSubmit(); return false;')); ?>

           <div class="ContainerList">
                <div class="contentHeader">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
                        <h3 class="Heading03 MBheadng03">Manage Currency</h3>
                    </div>
                    <a href="<?php echo base_url(); ?>admin/user/add_currency">Add Currency</a>
                </div>
             <div class="ListDataContainer">
                 <div class="form-group full-col">
            <table class="data-table tablesorter" id="myTable">
                <thead>
                    <tr>
                        <th>Currency Name</th>
                        <th>Currency Symbol</th>
                        <th>Currency Rate</th>
                        <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($currency_data)) {
                       foreach($currency_data as $data) {
                ?>
                            <tr>
                               <td><?php echo $data['currency_name']; ?></td>
                               <td><?php echo $data['currency_symbol']; ?></td>
                               <td><?php echo $data['currency_rate']; ?></td>

                                <td align="center">
                                    <?php if($data['id'] == 1){?><?php }else{?>
                                    <a href="<?php echo base_url().'admin/user/edit_currency/'.$data['id']; ?>" title="Edit"><img src="<?php echo base_url(); ?>images/edit.png" alt="Edit"></a>
                                    <a href="<?php echo base_url().'admin/user/delete_currency/'.$data['id']; ?>" title="Delete" onclick="return checkdelete()"><img src="<?php echo base_url(); ?>images/delete.png" alt="Delete"></a>
                                    <?php }?>
                                </td>


                          <?php  } } else { ?>

                           <tr>
                                <td colspan="7">No currrency found</td>
                            </tr>
                <?php  }  ?>

               </tbody>
            </table>
        </div>


                </div>
            </div>

   <?php echo form_close(); ?>



<?php $this->load->view("admin/include/footer"); ?>
