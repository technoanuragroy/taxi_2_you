<?php $this->load->view("admin/include/header"); ?>
<link href="<?php echo base_url(); ?>css/admin_dashboard.css" rel="stylesheet">
<script type="text/javascript" >
    $(document).ready(function(){
        $("#frmEdit").validationEngine({promptPosition : "bottomLeft", scroll: true});
    });
            
    function deleteImg() { 
                
        if (confirm("Delete profile picture?") == true) {
            window.location.href = '<?php echo base_url();?>admin/user/delete_profilePic/';
        } else {
            return false;
        }
        
        return false;                
    } 
</script>
<style type="text/css">
    .icon_link > input {
        visibility:hidden;
        width:0;
        height:0
    }
</style>

<?php echo form_open_multipart('', array('id' => 'frmEdit')); ?>
            <div class="contentHeader">
                <div style="font-size: 17px; color: #72838b; float: left; margin-top: 10px; padding-right: 5px;">
                    <span class="Iconheading"><?php echo $admin_details['image']; ?></span>
                </div>
                <div class="name_container">
                    <div class="pageheadingContainer">
                        <h1 class="Heading03"><?php echo $admin_details['name']; ?></h1>
                    </div>
                </div>
            </div>
    
            <div class="rowContent rowFirstContent">
                <div class="col-md-12 col-lg-3">
                    
                    <h2 class="Heading02" style="font-weight: 700;">Profile picture</h2>
                    
                    <span class="subheading"></span>
                    
                    <?php 
                        $filename = 'uploads/admin/'.$admin_details['admin_id'].'.jpg';
                        if(file_exists($filename)) { 
                    ?>
                            <div class="edit_photo_container">
                                <div class="icon_delete_container">
                                    <a href="#"  class="icon_link" onclick="deleteImg();" >
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                    <div class="icon_link">
                                        <label for="file-input" >
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </label>
                                        <input id="file-input"  type="file" name="adminphoto" id="adminphoto">
                                    </div>
                                </div>
                                <img src="<?php echo base_url().'uploads/admin/'.$admin_details['admin_id'].'.jpg'.'?'.time(); ?>">                                
                            </div>
                    
                    <?php } else { ?>
                            
                            <div class="edit_photo_container">
                                <div class="icon_delete_container">
                                    <div class="icon_link">
                                        <label for="file-input" >
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </label>
                                        <input id="file-input"  type="file" name="adminphoto" id="adminphoto">
                                    </div>
                                </div>
                                <img src="https://cdn1.iconfinder.com/data/icons/metro-ui-dock-icon-set--icons-by-dakirby/256/User_No-Frame.png">                                
                            </div>
                    
                    <?php } ?>                    
                </div>
                
                <div class="col-lg-9">
                    <div class="rightContainer">
                        <div class="invalid">
                            <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
                        </div>
                        <div class="sucess">
                            <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                        </div>
                                               
                        <div class="left-col mb-col-left">
                            <label class="label-form"><span class="symbolcolor">*</span>Name</label>
                            <input required type="text" class="form-control validate[required]" name="name" id="name" placeholder="Name" value="<?php echo $admin_details['name']; ?>">
                        </div>
                        
                        <div class="right-col mb-col-left">
                            <label class="label-form"><span class="symbolcolor">*</span>Phone</label>
                            <input type="number" class="form-control validate[required,phone]" name="phone" id="phone" placeholder="Phone number" value="<?php echo $admin_details['phone']; ?>">
                        </div>
                        
                        <div class="left-col mb-col-left">
                            <label class="label-form"><span class="symbolcolor">*</span>Account Email</label>
                            <input type="email" class="form-control validate[required,custom[email]]" name="email" id="email" placeholder="Account email" value="<?php echo $admin_details['email']; ?>">
                        </div>

                        <div class="right-col mb-col-left">
                            <label class="label-form"><span class="symbolcolor">*</span>Username</label>
                            <input type="text" class="form-control validate[required,minSize[5]]" name="username" id="username" placeholder="Enter username" value="<?php echo $admin_details['username']; ?>" readonly="true">
                        </div>                                                        
                                                                                
                        <span class="buttonSbmit">
                            <input type="submit" name="save" value="Update" class="submitButton">
                        </span>
                    </div>
                </div>
            </div>      
 <?php echo form_close(); ?>
                
<?php $this->load->view("admin/include/footer"); ?>
