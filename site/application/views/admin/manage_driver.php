<?php $this->load->view("admin/include/header"); ?>

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 35px;
  height: 20px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 3px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(13px);
  -ms-transform: translateX(13px);
  transform: translateX(13px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<script type="text/javascript">
    $(document).ready(function(){
        $("#driver_form").validationEngine({promptPosition : "bottomLeft", scroll: true});
    });


    function change_status(id){

      var status = $('.status_'+id).val();
      var data = {
        'id' : id,
        'status' : status,
      };
      $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>admin/user/update_status",
          data: data,
          success: function(msg){
              //alert(msg);
              $('.status_'+id).val(msg);
          }
      });
    }
</script>

<?php echo form_open('', array('name' => 'driver_form', 'id' => 'driver_form', 'onsubmit' => 'isSubmit(); return false;')); ?>

           <div class="ContainerList">
                <div class="contentHeader">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
                        <h3 class="Heading03 MBheadng03">Manage Driver</h3>
                    </div>
                </div>
             <div class="ListDataContainer">
                 <div class="form-group full-col">
            <table class="data-table tablesorter" id="myTable">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Vehical Name</th>
                        <th>Vehical Make</th>
                        <th>Vehical Model</th>
                        <th>Status</th>
                        <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($drivers)) {
                       foreach($drivers as $driver) {
                ?>
                            <tr>

                               <td><?php echo  $driver['first_name']; ?></td>
                               <td><?php echo  $driver['last_name']; ?></td>
                                <td><?php echo $driver['phone']; ?></td>
                                <td><?php echo $driver['email']; ?></td>
                                <td><?php echo $driver['vehicle_name'];?></td>
                                <td><?php echo $driver['vehicle_make'];?></td>
                                <td><?php echo $driver['vehicle_model'];?></td>

<!--                                <td><?php
//                                if($driver['status']==0){
//                                    $drive='Inactive';
//                                }else{
//                                    $drive='Active';
//                                }
//
//                                echo $drive; ?>
                                </td>-->
                                <?php
                                if($driver['status'] == 1){
                                    $checked = 'checked = "checked"';
                                } else {
                                    $checked = '';
                                }
                                ?>
                                <td>
                                  <input class="status_<?php echo $driver['id']; ?>" type="hidden" name="status" value="<?php echo $driver['status']; ?>" />
                                  <label class="switch">
                                    <input type="checkbox" <?php echo $checked ?> onclick="return change_status(<?php echo $driver['id']; ?>);">
                                    <span class="slider round"></span>
                                  </label>
                                </td>

                                <td align="center">
                                    <a href="<?php echo base_url().'admin/user/edit_driver/'.$driver['id']; ?>" title="Edit this driver"><img src="<?php echo base_url(); ?>images/edit.png" alt="Edit"></a>
                                </td>

                          <?php  } } else { ?>

                           <tr>
                                <td colspan="7">No other user found</td>
                            </tr>
                <?php  }  ?>

               </tbody>
            </table>
        </div>


                </div>
            </div>

   <?php echo form_close(); ?>




<?php $this->load->view("admin/include/footer"); ?>
