<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript" >
    $(document).ready(function(){
        $("#edit_rider").validationEngine({promptPosition : "bottomLeft", scroll: true});


        ///////////  END OF READY FUNCTION   ///////////
    });

</script>
<?php// foreach($riders_data as $rides_data){?>
<?php echo form_open('admin/user/addAdmin', array('id' => 'edit_rider', 'name' => 'edit_rider')); ?>

            <div class="ContainerList">
                <div class="contentHeader">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
                        <h3 class="Heading03 MBheadng03">Add Admin</h3>
                    </div>
                </div>

                <div class="ListDataContainer">


                        <div class="invalid">
                            <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
                        </div>
                        <div class="sucess">
                            <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                        </div>

                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Admin Name : </label>
                            <?php echo form_input(array('name'=> 'admin_name','id' => 'admin_name','value'=> set_value('admin_name'),'class'=>'form-control validate[required]'));?>
                        </div>
                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Admin Phone : </label>
                            <?php echo form_input(array('name'=> 'admin_phone','id' => 'admin_phone','value'=> set_value('currency_name'),'class'=>'form-control validate[required]'));?>
                        </div>
                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Admin E-mail : </label>
                            <?php echo form_input(array('name'=> 'email','id' => 'email','value'=> set_value('currency_name'),'class'=>'form-control validate[required]'));?>
                        </div>
                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Admin User Name : </label>
                            <?php echo form_input(array('name'=> 'user_name','id' => 'user_name','value'=> set_value('currency_rate'),'class'=>'form-control validate[required]'));?>
                        </div>
                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Password : </label>
                            <?php echo form_password(array('name'=> 'password','id' => 'password','value'=> set_value('currency_rate'),'class'=>'form-control validate[required]'));?>
                        </div>
                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Country : </label>
                            <?php
                  							$countries = $this->my_custom_functions->get_country_dropdown_data();
                  							echo form_dropdown('country', $countries, "", 'class="form-control validate[required]" id="country"');
                  					?>
                        </div>



                        <span class="buttonSbmit">
                            <input type="submit" name="submit" value="Save" class="submitButton">
                        </span>
                 </div>
             </div>
 <?php echo form_close(); ?>
<?php //} ?>

<?php $this->load->view("admin/include/footer"); ?>
