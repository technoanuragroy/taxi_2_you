<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function(){
        $("#addrider_form").validationEngine({promptPosition : "bottomLeft", scroll: true});
    });
</script>

<?php //echo form_open('', array('name' => 'addrider_form', 'id' => 'addrider_form', 'onsubmit' => 'isSubmit(); return false;')); ?>

           <div class="ContainerList">
                <div class="contentHeader">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
                        <h3 class="Heading03 MBheadng03">Manage Admin</h3>
                    </div>
                    <a href="<?php echo base_url(); ?>admin/user/addAdmin">Add Admin</a>
                </div>
             <div class="ListDataContainer">
                 <div class="form-group full-col">
            <table class="data-table tablesorter" id="myTable">
                <thead>
                    <tr>
                        <th>Admin Name</th>
                        <th>Admin Phone</th>
                        <th>Admin E-Mail</th>
                        <th>Country</th>
                        <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($admin_data)) {
                       foreach($admin_data as $data) {
                ?>
                            <tr>
                               <td><?php echo $data['name']; ?></td>
                                <td><?php echo $data['phone']; ?></td>
                                <td><?php echo $data['email']; ?></td>
                                <?php $country_name = $this->my_custom_functions->get_particular_field_value('tbl_country', 'name', 'and country_id="'.$data['country_id'].'"');?>
                                <td><?php echo $country_name; ?></td>
                                <td align="center">
                                    <a href="<?php echo base_url().'admin/user/edit_admin/'.$data['admin_id']; ?>" title="Edit"><img src="<?php echo base_url(); ?>images/edit.png" alt="Edit"></a>
                                </td>

                          <?php  } } else { ?>

                           <tr>
                                <td colspan="7">No admin found</td>
                            </tr>
                <?php  }  ?>

               </tbody>
            </table>
        </div>


                </div>
            </div>

   <?php //echo form_close(); ?>



<?php $this->load->view("admin/include/footer"); ?>
