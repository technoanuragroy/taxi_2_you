<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">    
    $(document).ready(function(){
        $("#form_reset").validationEngine({promptPosition : "bottomLeft", scroll: true});
    });        
</script>
                
<?php echo form_open('',array('name' => 'form_reset', 'id' => 'form_reset', 'method' => 'POST', 'class' => 'formFields')); ?>

            <div class="ContainerList">
                <div class="contentHeader">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
                        <h3 class="Heading03 MBheadng03">Reset new password</h3>
                    </div>                             
                </div>
            
                <div class="ListDataContainer"> 
                                
                    <div class="invalid">
                        <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>                            
                    </div>
                    <div class="sucess">
                        <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                    </div>

                    <div class="form-group full-col">
                        <label class="label-form"><span class="symbolcolor">*</span>New Password</label>                                  
                        <?php echo form_password(array('name'=>'new_password','id'=>'new_password' ,'class'=>'form-control validate[required,minSize[6]]', 'placeholder'=>'Enter new password')); ?> 
                    </div>

                    <div class="form-group full-col">
                        <label class="label-form"><span class="symbolcolor">*</span>Retype Password</label>                                  
                        <?php echo form_password(array('name'=>'re_new_password','id'=>'re_new_password' ,'class'=>'form-control validate[required,minSize[6],equals[new_password]]', 'placeholder'=>'Retype new password')); ?> 
                    </div>

                    <span class="buttonSbmit02">                                                   
                         <input type="submit" name="submit" id="submit" value="Change Password" class="submitButton">                        
                    </span>
                </div>   
            </div>          
<?php echo form_close(); ?>		
         
<?php $this->load->view("admin/include/footer"); ?>
