<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript" >
    $(document).ready(function(){
        $("#edit_rider").validationEngine({promptPosition : "bottomLeft", scroll: true});


        ///////////  END OF READY FUNCTION   ///////////
    });

</script>
<?php// foreach($riders_data as $rides_data){?>
<?php echo form_open('admin/user/edit_currency/'.$currency_data['id'], array('id' => 'edit_rider', 'name' => 'edit_rider')); ?>

            <div class="ContainerList">
                <div class="contentHeader">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
                        <h3 class="Heading03 MBheadng03">Edit Currency</h3>
                    </div>
                </div>

                <div class="ListDataContainer">


                        <div class="invalid">
                            <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
                        </div>
                        <div class="sucess">
                            <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                        </div>

                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Currency Name : </label>
                            <?php echo form_input(array('name'=> 'currency_name','id' => 'currency_name','value'=> set_value('currency_name', $currency_data['currency_name']),'class'=>'form-control validate[required]'));?>
                        </div>
                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Currency Symbol : </label>
                            <?php echo form_input(array('name'=> 'currency_symbol','id' => 'currency_symbol','value'=> set_value('currency_symbol', $currency_data['currency_symbol']),'class'=>'form-control validate[required]'));?>
                        </div>
                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Currency Rate : </label>
                            <?php echo form_input(array('name'=> 'currency_rate','id' => 'currency_rate','value'=> set_value('currency_rate', $currency_data['currency_rate']),'class'=>'form-control validate[required]'));?>
                        </div>



                        <span class="buttonSbmit">
                            <input type="submit" name="submit" value="Save" class="submitButton">
                        </span>
                 </div>
             </div>
 <?php echo form_close(); ?>
<?php //} ?>

<?php $this->load->view("admin/include/footer"); ?>
