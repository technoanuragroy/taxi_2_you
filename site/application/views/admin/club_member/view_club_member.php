<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript" >
    function confirm_delete(){

        var check = confirm("Are you sure that you want to remove this coupon?");
        if (check == true) {
            return true;
        } else {
            return false;
        }
    }

    $(document).ready(function() {



//        $(".data-table").tablesorter({
//            headers: {0: {sorter: false}, 1: {sorter: false}, 2: {sorter: false}, 6: {sorter: false}}
//        });
    });
</script>

<div class="ContainerList">
    <!-- <div class="contentHeader">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
            <h3 class="Heading03 MBheadng03">Manage Club Members</h3>
        </div>
        <span class="buttoncontainer">
            <span class="buttonMargin"><a href="javascript:;" class="add_button Button09 search_btn">Search</a></span>
        </span>
    </div> -->



    <div class="ListDataContainer">

        <div class="invalid">
            <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
        </div>
        <div class="sucess">
            <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
            <h4 class="Heading03 MBheadng03"><?php echo ucwords($club_member_det['name']); ?></h4>
        </div>
        <div class="form-group full-col table-responsive">
          <b>Mobile:</b> <?php echo $club_member_det['mobile']; ?><br>
          <b>Email:</b> <?php echo $club_member_det['email']; ?><br>
          <b>Address:</b> <?php echo $club_member_det['address']; ?><br>
          <b>Country:</b> <?php $country_name = $this->my_custom_functions->get_particular_field_value('tbl_country', 'name', 'AND country_id="'.$club_member_det['country'].'"');
          echo $country_name; ?>
          <b>City:</b> <?php echo $club_member_det['city']; ?><br>
          <b>Zip Code:</b> <?php echo $club_member_det['zip_code']; ?><br>

          <!-- ///////  ==========  QUESTIONNAIRE  ===========   ////////// -->
          <h2><u>Consumer Preferences Questionnaire</u></h2>
          <h4 class="sub_categ">Area of Consumable Consumption</h4>

          <?php
              $questions = $this->Admin_model->get_questions(2);

              //echo "<pre>"; print_r($questions); echo "</pre>";
              $i = 1;
              foreach($questions as $ques) { ?>
                <h5> <b>Ques: <?php echo $ques['question_text']; ?> </b></h5>
                  <!-- search for sub option existence -->
                  <?php $answered_options = $this->Admin_model->get_answered_options($ques['id'], $grp_admin['id']);
                  if(!empty($answered_options)){
                    foreach($answered_options as $answered_option){

                      $option_text = $this->Admin_model->get_options_text($answered_option['answered_option_id']);

                      if($option_text['option_id'] == 0){
                        echo 'Ans: ' . $option_text['option_text'];
                      }else{
                        $sub_option = $this->my_custom_functions->get_particular_field_value('tbl_questionnaire_options', 'option_text', 'and id ="'.$option_text['option_id'].'"');
                        echo 'Ans: ' . $sub_option . ': ' . $option_text['option_text'] . '<br>';
                      }
                    }
                  } else{
                    echo 'Ans: ';
                  }
                  ?>
          <?php } ?>
          <br>

          <h4 class="sub_categ">Area of Appliances, household equipment, clothing and other categories</h4>

          <?php
              $questions = $this->Admin_model->get_questions(3);

              $i = 1;
              foreach($questions as $ques) { ?>
                <h5> <b>Ques: <?php echo $ques['question_text']; ?> </b></h5>
                  <!-- search for sub option existence -->
                  <?php $answered_options = $this->Admin_model->get_answered_options($ques['id'], $grp_admin['id']);
                  if(!empty($answered_options)){
                    foreach($answered_options as $answered_option){

                      $option_text = $this->Admin_model->get_options_text($answered_option['answered_option_id']);

                      if($option_text['option_id'] == 0){
                        echo 'Ans: ' . $option_text['option_text'];
                      }else{
                        $sub_option = $this->my_custom_functions->get_particular_field_value('tbl_questionnaire_options', 'option_text', 'and id ="'.$option_text['option_id'].'"');
                        echo 'Ans: ' . $sub_option . ': ' . $option_text['option_text'] . '<br>';
                      }
                    }
                  } else{
                    echo 'Ans: ';
                  }
                  ?>
          <?php } ?>


          <br>
          <h2><u>Family status questionnaire</u></h2>

          <?php
              $questions = $this->Admin_model->get_questions(4);
              $i = 1;
              foreach($questions as $ques) { ?>
                <h5> <b>Ques: <?php echo $ques['question_text']; ?> </b></h5>
                  <!-- search for sub option existence -->
                  <?php $answered_options = $this->Admin_model->get_answered_options($ques['id'], $grp_admin['id']);
                  if(!empty($answered_options)){
                    foreach($answered_options as $answered_option){

                      $option_text = $this->Admin_model->get_options_text($answered_option['answered_option_id']);

                      if($option_text['option_id'] == 0){
                        echo 'Ans: ' . $option_text['option_text'];
                      }else{
                        $sub_option = $this->my_custom_functions->get_particular_field_value('tbl_questionnaire_options', 'option_text', 'and id ="'.$option_text['option_id'].'"');
                        echo 'Ans: ' . $sub_option . ': ' . $option_text['option_text'] . '<br>';
                      }
                    }
                  } else{
                    echo 'Ans: ';
                  }
                  ?>
          <?php } ?>

          <table class="data-table table table-striped">
              <thead>
                  <tr>
                      <th>Name</th>
                      <th>Birthday</th>
                  </tr>
              </thead>
              <tbody>
          <?php if(!empty($club_member_det['bdays'])){
            foreach($club_member_det['bdays'] as $bdays) { ?>
              <tr>
                  <td>
                      <?php echo $bdays['name']; ?>
                  </td>
                  <td>
                      <?php echo $bdays['dob']; ?>
                  </td>
              </tr>
              <?php
                      }
                  } else {
              ?>
              <tr>
                  <td colspan="2">No Data found</td>
              </tr>
              <?php } ?>
              </tbody>
          </table>

          <h2><u>Shopping preferences questionnaire</u></h2>
          <?php
              $questions = $this->Admin_model->get_questions(5);
              $i = 1;

              foreach($questions as $ques) { ?>
                <h5> <b>Ques: <?php echo $ques['question_text']; ?> </b></h5>
                  <!-- search for sub option existence -->
                  <?php $answered_options = $this->Admin_model->get_answered_options($ques['id'], $grp_admin['id']);
                  if(!empty($answered_options)){
                    foreach($answered_options as $answered_option){

                      $option_text = $this->Admin_model->get_options_text($answered_option['answered_option_id']);

                      if($option_text['option_id'] == 0){
                        echo 'Ans: ' . $option_text['option_text'];
                      }else{
                        $sub_option = $this->my_custom_functions->get_particular_field_value('tbl_questionnaire_options', 'option_text', 'and id ="'.$option_text['option_id'].'"');
                        echo 'Ans: ' . $sub_option . ': ' . $option_text['option_text'] . '<br>';
                      }
                    }
                  } else{
                    echo 'Ans: ';
                  }
                  ?>
          <?php } ?>
        </div>



    </div>
</div>
<?php $this->load->view("admin/include/footer"); ?>
