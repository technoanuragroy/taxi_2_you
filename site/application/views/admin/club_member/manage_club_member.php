<?php $this->load->view("admin/include/header"); ?>

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 35px;
  height: 20px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 3px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(13px);
  -ms-transform: translateX(13px);
  transform: translateX(13px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<script type="text/javascript" >



    $(document).ready(function() {
      $(".search_btn").click(function() {
          $(".search_box").slideToggle(700);
      });


//        $(".data-table").tablesorter({
//            headers: {0: {sorter: false}, 1: {sorter: false}, 2: {sorter: false}, 6: {sorter: false}}
//        });
   ////////   ---------- END  END  END  END   _--------   ///////
    });

    function change_status(id){

      var status = $('.status_'+id).val();
      var data = {
        'id' : id,
        'status' : status,
      };
      $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>admin/club_member/update_status",
          data: data,
          success: function(msg){
              //alert(msg);
              $('.status_'+id).val(msg);
          }
      });
    }
    function confirm_delete(){

        var check = confirm("Are you sure that you want to remove this coupon?");
        if (check == true) {
            return true;
        } else {
            return false;
        }
    }
</script>

<div class="ContainerList">
    <div class="contentHeader">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
            <h3 class="Heading03 MBheadng03">Manage Club Members</h3>
        </div>
        <span class="buttoncontainer">
            <span class="buttonMargin"><a href="javascript:;" class="add_button Button09 search_btn">Search</a></span>
        </span>
    </div>

    <!-- Search Form -->
    <div class="search_box"  style="display: none;width:100%;height:200px;margin-top: 28px;margin-bottom: 38px;">

      <?php
      $attributes = array('name' => 'club_mem_search', 'id' => 'club_mem_search');
      echo form_open('admin/club_member/manage_club_member', $attributes);

      ?>
      <div class="col-lg-6 col-md-6 col-sm-6" style=" margin-top:10px;left:-15px;">
        <div class="search_child_section">
            <?php echo form_input(array('name' => 'email', 'class' => 'form-control', 'placeholder' => 'Search by email'), set_value('mob_no')); ?>
        </div>
      </div>

      <div class="col-lg-6 col-md-6 col-sm-6" style="margin-top:10px;left:-15px;">
        <div class="search_child_section">
          <?php
              $countries = $this->my_custom_functions->get_country_dropdown_data();
              echo form_dropdown('country', $countries, "", 'class="form-control custom-select mr-sm-2 validate[required]" id="country"');
          ?>
        </div>
      </div>

          <div style="width:20%;text-align: center;float:right;margin:30px 0;"><?php echo form_submit('submit', 'Search', 'class = "add_button Button09"'); ?> </div>
          <?php echo form_close(); ?>
      </div>

    <div class="ListDataContainer">

        <div class="invalid">
            <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
        </div>
        <div class="sucess">
            <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
            <h4 class="Heading03 MBheadng03">List of Club Members</h4>
        </div>
        <div class="form-group full-col table-responsive">
            <table class="data-table table table-striped">
                <thead>
                    <tr>
                        <th>Club Member Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Country</th>
                        <!-- <th>City</th> -->
                        <th>Status</th>
                        <!-- <th>Change status</th> -->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($club_members)) {
                        foreach($club_members as $club_member) {

                ?>
                            <tr>
                                <td>
                                    <?php echo $club_member['name']; ?>
                                </td>
                                <td>
                                    <?php echo $club_member['mobile']; ?>
                                </td>
                                <td>
                                    <?php echo $club_member['email']; ?>
                                </td>
                                <td>
                                    <?php echo $club_member['address']; ?>
                                </td>
                                <td>
                                    <?php
                                    $country_name = $this->my_custom_functions->get_particular_field_value('tbl_country', 'name', 'AND country_id="'.$club_member['country'].'"');
                                    echo $country_name; ?>
                                </td>
                                <!-- <td>
                                    <?php echo $club_member['city']; ?>
                                </td>
                                <td>
                                    <?php echo $club_member['zip_code']; ?>
                                </td> -->
                                <!-- <td>
                                    <?php
                                    // if($club_member['status'] == 1){
                                    //     echo "<span class='s_msg'>"."Active". "</span>";
                                    // }else{
                                    //     echo "<span class='e_msg'>"."Inactive". "</span>";
                                    // }
                                    ?>
                                </td> -->
                                <?php
                                if($club_member['status'] == 1){
                                    $checked = 'checked = "checked"';
                                }else{
                                    $checked = '';
                                }
                                ?>

                                <td>
                                  <input class="status_<?php echo $club_member['id']; ?>" type="hidden" name="status" value="<?php echo $club_member['status']; ?>" />
                                  <label class="switch">
                                    <input type="checkbox" <?php echo $checked ?> onclick="return change_status(<?php echo $club_member['id']; ?>);">
                                    <span class="slider round"></span>
                                  </label>
                                </td>

                                <td align="center">
                                    <a href="<?php echo base_url().'admin/club_member/view_club_mem_det/'.$club_member['id']; ?>" title="View Details"><img src="<?php echo base_url(); ?>images/eye_view.png" title="View Details"></a>
                                    <a href="<?php echo base_url().'club_member/main/login_as_club_member/'.$club_member['id']; ?>" target="_blank"><img src="<?php echo base_url(); ?>images/icon-person.png" title="Login As Club Member"></a>
                                </td>
                            </tr>
                <?php
                        }
                    } else {
                ?>
                            <tr>
                                <td colspan="7">No Club Member found</td>
                            </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>



    </div>
    
    <div class="pagination"><?php echo $this->pagination->create_links(); ?></div>
</div>

<?php $this->load->view("admin/include/footer"); ?>
