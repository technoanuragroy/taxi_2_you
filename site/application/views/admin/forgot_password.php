<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">    
    $(document).ready(function(){
        $("#forgot_password").validationEngine({promptPosition : "bottomLeft", scroll: true});
    });        
</script>
                
<?php echo form_open('',array('name' => 'forgot_password', 'id' => 'forgot_password', 'method' => 'POST', 'class' => 'formFields')); ?>

            <div class="ContainerList">
                <div class="contentHeader">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
                        <h3 class="Heading03 MBheadng03">Request new password</h3>
                    </div>                             
                </div>
            
                <div class="ListDataContainer">    
            
                    <div class="invalid">
                        <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>                            
                    </div>
                    <div class="sucess">
                        <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                    </div>

                    <div class="form-group full-col">
                        <label class="label-form"><span class="symbolcolor">*</span>Username</label>
                        <input type="text" name="username" class="form-control validate[required]" id="username" placeholder="Enter registered username">
                    </div>

                    <span class="buttonSbmit">                                            
                         <input type="submit" name="submit" id="submit" value="Submit" class="submitButton">                        
                    </span>
                </div>   
            </div>         
<?php echo form_close(); ?>                     
 
<?php $this->load->view("admin/include/footer"); ?> 
