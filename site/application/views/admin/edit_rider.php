<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript" >
    $(document).ready(function(){
        $("#edit_rider").validationEngine({promptPosition : "bottomLeft", scroll: true});


        ///////////  END OF READY FUNCTION   ///////////
    });

</script>
<?php// foreach($riders_data as $rides_data){?>
<?php echo form_open('admin/user/edit_rider/'.$riders_data['id'], array('id' => 'edit_rider', 'name' => 'edit_rider')); ?>

            <div class="ContainerList">
                <div class="contentHeader">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
                        <h3 class="Heading03 MBheadng03">Edit Rider</h3>
                    </div>
                </div>

                <div class="ListDataContainer">


                        <div class="invalid">
                            <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
                        </div>
                        <div class="sucess">
                            <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                        </div>

                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Rider First Name : </label>
                            <?php echo form_input(array('name'=> 'first_name','id' => 'first_name','value'=> set_value('first_name', $riders_data['first_name']),'class'=>'form-control validate[required]'));?>
                        </div>
                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Rider Last Name : </label>
                            <?php echo form_input(array('name'=> 'last_name','id' => 'last_name','value'=> set_value('last_name', $riders_data['last_name']),'class'=>'form-control validate[required]'));?>
                        </div>

                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Phone : </label>
                            <?php echo form_input(array('name'=> 'phone','id' => 'phone','value'=> set_value('phone', $riders_data['phone']),'class'=>'form-control validate[required]'));?>
                        </div>

                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Email : </label>
                            <?php echo form_input(array('name'=> 'email','id' => 'email','value'=> set_value('email', $riders_data['email']),'class'=>'form-control validate[required,custom[email]]'));?>
                        </div>


                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Username: </label>
                            <?php echo form_input(array('name'=> 'username','id' => 'username','value'=> set_value('username', $riders_data['username']),'class'=>'form-control validate[required]'));?>
                        </div>

                        <div class="form-group full-col">
                            <label class="label-form"><span class="symbolcolor">*</span>Country : </label>
                            <?php
                                $countries = $this->my_custom_functions->get_country_dropdown_data();
                                echo form_dropdown('country', $countries, $riders_data['country_id'], 'class="form-control validate[required]" id="country"');
                                echo form_error('country');
                            ?>
                        </div>

                        <div class="form-group full-col">
                            <label class="label-form">Status : </label>
                            <select name="status" id="status" class="form-control">
                                <option value="1" <?php if($riders_data['status'] == 1) { echo "selected='selected'"; }?>>Active</option>
                                <option value="0" <?php if($riders_data['status'] == 0) { echo "selected='selected'"; }?>>Inactive</option>
                            </select>
                        </div>

                        <span class="buttonSbmit">
                            <input type="submit" name="submit" value="Save" class="submitButton">
                        </span>
                 </div>
             </div>
 <?php echo form_close(); ?>
<?php //} ?>

<?php $this->load->view("admin/include/footer"); ?>
