<?php $this->load->view("_include/header_inner"); ?>

    <script type="text/javascript" >
        $(document).ready(function(){
            $("#rider_login").validationEngine({promptPosition : "bottomLeft", scroll: true});
        });

        setTimeout(function() {
            $('.s_message').hide('slow');
        }, 5000);

        setTimeout(function() {
            $('.e_message').hide('slow');
        }, 5000);

        function isNumber(evt) {

            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>

    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 login_form">
                <h2><span class="triangle"><img src="<?php echo base_url(); ?>images/symbol_triangle.png" alt=""></span><?php echo $title; ?></h2>
				
                <div class="inner_form">

                    <?php echo form_open('', array('id' => 'rider_login', 'class' => 'formFields')); ?>

                            <div class="invalid">
                                <?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
                                <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
                            </div>
                            <div class="sucess">
                                <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                            </div>

                            <div class="form-group">
                                <label class="label-form"><span class="symbolcolor">*</span>User Login Id : </label>
                                <?php echo form_input(array('name'=> 'username','id' => 'username','class'=>'form-control validate[required, custom[email]]')); ?>
                            </div>

                            <div class="form-group">
                                <label class="label-form"><span class="symbolcolor">*</span>Password : </label>
                                <?php echo form_input(array('type' => 'password', 'name'=> 'password','id' => 'password','value'=> '','class'=>'form-control validate[required]')); ?>
                            </div>

                            <input type="submit" name="submit" value="Login" class="btn_1 btn">

                            <a class="club_member_forgot_password" href="<?php echo base_url(); ?>rider/main/forgot_password"> Forgot Password? </a>

                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("_include/footer"); ?>
