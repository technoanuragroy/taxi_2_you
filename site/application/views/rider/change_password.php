<?php $this->load->view("_include/header_inner"); ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#reset_password").validationEngine({promptPosition : "bottomLeft", scroll: true});
        });
    </script>

    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 login_form">
                <h2><span class="triangle"><img src="<?php echo base_url(); ?>images/symbol_triangle.png" alt=""></span><?php echo $title; ?></h2>

                <div class="inner_form">

                    <?php echo form_open('', array('name' => 'passwordchange_form', 'id' => 'passwordchange_form', 'onsubmit' => 'isSubmit(); return false;')); ?>

                            <div class="invalid">
                                <?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
                                <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
                            </div>
                            <div class="sucess">
                                <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                            </div>

                            <div class="form-group">
                                <label class="label-form"><span class="symbolcolor">*</span>Old Password</label>
                                <?php echo form_password(array("name" => "o_password", "id" => "o_password", "class" => "form-control validate[required]")); ?>
                            </div>

                            <div class="form-group">
                                <label class="label-form"><span class="symbolcolor">*</span>New Password</label>
                                <?php echo form_password(array("name" => "n_password", "id" => "n_password", "class" => "form-control validate[required,minSize[6]]")); ?>
                            </div>

                            <div class="form-group">
                                <label class="label-form"><span class="symbolcolor">*</span>Confirm New Password</label>
                                <?php echo form_password(array("name" => "c_password", "id" => "c_password", "class" => "form-control validate[required,minSize[6],equals[n_password]]")); ?>
                            </div>

                            <input type="submit" name="submit" value="Change Password" class="btn_1 btn">

                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </div>

<?php //$this->load->view("_include/footer"); ?>
