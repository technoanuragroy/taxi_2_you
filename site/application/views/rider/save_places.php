<?php $this->load->view("_include/header_inner"); ?>

<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
<style>
      html, body, #map-canvas {
	height: 100%;
	width: 100%;
	margin: 0;
	padding: 0;
}

#map-canvas {
	height: 100%;
	width: 100%;
}

label {
	padding: 20px 10px;
	display: inline-block;
	font-size: 1.5em;
}

input {
	font-size: 0.75em;
	padding: 10px;
}
    </style>
    <div class="container">
        <div class="row">
        <a  href="<?php echo base_url(); ?>rider/user/rider_dashboard"><h2><span class="triangle"><img src="images/symbol_triangle.png" alt=""></span>Private Area</h2>
        </a>
            <?php if ($this->session->userdata('rider_id')) {?>
                  <a class="nav-link" href="<?php echo base_url(); ?>rider/user/edit_profile">Edit Profile</a>
                  <a class="nav-link" href="<?php echo base_url(); ?>rider/user/save_places">Save places</a>
                  <a class="nav-link" href="<?php echo base_url(); ?>rider/user/change_password">Change Password</a>
                  <a class="nav-link" href="<?php echo base_url(); ?>rider/main/rider_logout">Logout</a>
            <?php }?>
         <div class="mainContent">
                Welcome, <?php echo $details['name']; ?>
         </div>
				 <span id="success"></span>

        </div>
    </div>
    <form id="save_location" method="post" enctype="multipart/form-data">
    <input type="hidden" id="rider_id" name="rider_id" value="<?php echo $details['id']; ?>">
    Location Title:<input  class="controls" id="place_title" name="place_title" type="text" placeholder="Enter Title">

    Address: <input id="map-search" class="controls" id="place_address" name="place_address" type="text" placeholder="Enter Location" >
             <input type="hidden" id="latitude" name="latitude" class="latitude" >
              <input type="hidden" id="longitude" name="longitude" class="longitude">
<!-- <input type="text" class="reg-input-city" placeholder="City"> -->
  <input type="submit" name="submit" value="Save Location">

    </form >

<div id="map-canvas"></div>

<script src="<?php echo base_url(); ?>js/map_javascript.js" ></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzPnF5nX1gp6gjzep6kWP2ktSe9l_ikVo&libraries=places&callback=initialize"></script>

<script type="text/javascript">


	$('form#save_location').submit(function(e) {

		 e.preventDefault();

		 var rider_id = $("#rider_id").val();
		 var place_title = $("#place_title").val();
		 var place_address = $("#place_address").val();
		 var latitude = $("#latitude").val();
		 var longitude = $("#longitude").val();


			$.ajax({

				 url: "<?php echo base_url(); ?>rider/user/save_places_register",
				 type: 'POST',
				 data: {rider_id:rider_id,place_title:place_title,place_address:place_address,latitude:latitude,longitude:longitude},
				 data: $('form').serialize(),
				 error: function() {
						alert('Something is wrong');
				 },
				 success: function(data) {
							if(data=='success'){
								$("#success").html('Location registered Successfully');
								setTimeout(function(){
									$("#success").hide(500);
									//$('#myModal2').modal('toggle');
								}, 3000);

								//window.parent.location.reload();
							}else if (data=='fail3') {
								$("#success").html('4 Location Already Added');
								setTimeout(function(){
									$("#success").hide(500);
								}, 3000);
							}
				 }
			});


	});
</script>
  </body>
</html>




<?php //$this->load->view("_include/footer"); ?>
