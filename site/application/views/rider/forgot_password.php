<?php $this->load->view("_include/header_inner"); ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#forgot_password").validationEngine({promptPosition : "bottomLeft", scroll: true});
        });
    </script>

    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 login_form">
                <h2><span class="triangle"><img src="<?php echo base_url(); ?>images/symbol_triangle.png" alt=""></span><?php echo $title; ?></h2>

                <div class="inner_form">

                    <?php echo form_open('',array('name' => 'forgot_password', 'id' => 'forgot_password', 'method' => 'POST', 'class' => 'formFields')); ?>

                            <div class="invalid">
                                <?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
                                <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
                            </div>
                            <div class="sucess">
                                <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                            </div>

                            <div class="form-group">
                                <label class="label-form"><span class="symbolcolor">*</span>Enter User Email Id : </label>
                                <?php echo form_input(array('name'=> 'user_name','id' => 'user_name','class'=>'form-control validate[required]')); ?>
                            </div>

                            <input type="submit" name="submit" value="Submit" class="btn_1 btn">

                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("_include/footer"); ?>
