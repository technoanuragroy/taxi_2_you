<?php $this->load->view("_include/header_inner"); ?>

    <script type="text/javascript" >

        $(document).ready(function() {
            $("#edit_profile").validationEngine({promptPosition : "bottomLeft", scroll: true});

            setTimeout(function() {
                $('.s_message').hide('slow');
            }, 5000);

            setTimeout(function() {
                $('.e_message').hide('slow');
            }, 5000);

            ////////  ------  #END  #END  #END  ------   /////////
        });
    </script>

    <div class="container">
        <div class="row">
            <h2><span class="triangle"><img src="<?php echo base_url(); ?>images/symbol_triangle.png" alt=""></span><?php echo $title; ?></h2>

            <div class="inner_form">

                <?php echo form_open_multipart('', array('id' => 'edit_profile', 'class' => 'formFields')); ?>

                        <div class="invalid">
                            <?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
                            <?php if($this->session->flashdata("e_message")) { echo '<p class="">'.$this->session->flashdata("e_message").'</p>'; } ?>
                        </div>
                        <div class="sucess">
                            <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                        </div>

                        <div class="form-group">
                            <label for="Name"><span class="symbolcolor">*</span>Name</label>
                            <?php echo form_input(array('name'=> 'name','id' => 'name','value'=> set_value('name', $details['name']),'class'=>'form-control validate[required]')); ?>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="mobile"><span class="symbolcolor">*</span>Mobile</label>
                                <?php echo form_input(array('name'=> 'mobile','id' => 'mobile','value'=> set_value('mobile', $details['phone']),'class'=>'form-control validate[required]')); ?>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="Country"><span class="symbolcolor">*</span>Country</label>
                                <select name="country" id="country" class="custom-select mr-sm-2 validate[required]">
                                <?php
                                    $countries = $this->my_custom_functions->get_country_dropdown();
                                    foreach($countries as $country)
                                    {?>
                                      <option value="<?php echo $country['country_id'];?>" <?php if($details['country_id'] == $country['country_id']){echo 'selected';}?>> <?php echo $country['name'];?></option>
                                <?php  }
                                ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for="login id"><span class="symbolcolor">*</span>Upload Photo </label>
                                <?php
                                    $filename = 'uploads/rider/'.$this->session->userdata('rider_id').'.jpg';
                                    if(file_exists($filename)) {
                                ?>
                                        <p>
                                            <a href="<?php echo base_url(); ?>uploads/rider/<?php echo $this->session->userdata('rider_id').'.jpg?t='.time(); ?>" class="fancybox" rel="group">
                                                Photo
                                            </a>
                                        </p>
                                <?php
                                    }

                                    echo form_upload(array('name'=> 'formal_id','id' => 'formal_id','class'=>'form-control-file'));
                                ?>
                            </div>

                        </div>

                        <input type="submit" name="submit" value="Update" class="btn_1 btn">

                <?php echo form_close(); ?>

            </div>
        </div>
    </div>

<?php $this->load->view("_include/footer"); ?>
