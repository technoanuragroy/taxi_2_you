<?php $this->load->view("_include/header_inner"); ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#reset_password").validationEngine({promptPosition : "bottomLeft", scroll: true});
        });
    </script>

    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 login_form">
                <h2><span class="triangle"><img src="<?php echo base_url(); ?>images/symbol_triangle.png" alt=""></span><?php echo $title; ?></h2>

                <div class="inner_form">

                    <?php echo form_open('',array('name' => 'reset_password', 'id' => 'reset_password', 'method' => 'POST', 'class' => 'formFields')); ?>

                            <div class="invalid">
                                <?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
                                <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>
                            </div>
                            <div class="sucess">
                                <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                            </div>

                            <div class="form-group">
                                <label class="label-form"><span class="symbolcolor">*</span>New Password : </label>
                                <?php echo form_input(array('type' => 'password', 'name'=> 'nw_password','id' => 'nw_password','class'=>'form-control validate[required]')); ?>
                            </div>

                            <div class="form-group">
                                <label class="label-form"><span class="symbolcolor">*</span>Confirm Password : </label>
                                <?php echo form_input(array('type' => 'password', 'name'=> 'confirm_password','id' => 'confirm_password','class'=>'form-control validate[required,equals[nw_password]]')); ?>
                            </div>

                            <input type="submit" name="submit" value="Reset Password" class="btn_1 btn">
                            
                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </div>
      
<?php $this->load->view("_include/footer"); ?>
