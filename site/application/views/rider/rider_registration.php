<?php $this->load->view("_include/header_inner"); ?>

    <script type="text/javascript" >

        $(document).ready(function() {
            $("#rider_registration").validationEngine({promptPosition : "bottomLeft", scroll: true});

            setTimeout(function() {
                $('.s_message').hide('slow');
            }, 5000);

            setTimeout(function() {
                $('.e_message').hide('slow');
            }, 5000);

            $('#datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange:"-100:+0",
                dateFormat: 'yy-mm-dd'
            });

            //////DISABLE DATEPICKER KEYPRESS ENTRY  /////
            $('#datepicker').keypress(function(event) {
                event.preventDefault();
            });

            ////////  ------  #END  #END  #END  ------   /////////
        });
    </script>

    <div class="container">
        <div class="row">
            <h2><span class="triangle"><img src="<?php echo base_url(); ?>images/symbol_triangle.png" alt=""></span><?php echo $title; ?></h2>

            <div class="inner_form">

                <?php echo form_open_multipart('', array('id' => 'rider_registration', 'class' => 'formFields')); ?>

                        <div class="invalid">
                            <?php if($this->session->flashdata("validation_message")) { echo $this->session->flashdata("validation_message"); } ?>
                            <?php if($this->session->flashdata("e_message")) { echo '<p class="">'.$this->session->flashdata("e_message").'</p>'; } ?>
                        </div>
                        <div class="sucess">
                            <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                        </div>

                        <div class="form-group">
                            <label for="Name"><span class="symbolcolor">*</span>Name</label>
                            <?php echo form_input(array('name'=> 'name','id' => 'name','value'=>  set_value('name'),'class'=>'form-control validate[required]')); ?>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="mobile"><span class="symbolcolor">*</span>Mobile</label>
                                <?php echo form_input(array('name'=> 'mobile','id' => 'mobile','value'=> set_value('mobile'),'class'=>'form-control validate[required]')); ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4"><span class="symbolcolor">*</span>Email</label>
                                <?php echo form_input(array('type' => 'email', 'name'=> 'email','id' => 'email','value'=> set_value('email'),'class'=>'form-control validate[required,custom[email]]')); ?>
                            </div>
                        </div>
<!--
                        <div class="form-group">
                            <label for="inputAddress"><span class="symbolcolor">*</span>Address</label>
                            <?php //echo form_textarea(array('name'=>'address','rows'=>'4','cols'=>'30','class'=>'form-control validate[required]'), set_value('address')); ?>
                        </div> -->

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Country"><span class="symbolcolor">*</span>Country</label>
                                <?php
                                    $countries = $this->my_custom_functions->get_country_dropdown_data();
                                    echo form_dropdown('country', $countries, "", 'class="custom-select mr-sm-2 validate[required]" id="country"');
                                ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="login id"><span class="symbolcolor">*</span>Upload Photo </label>
                                <?php echo form_upload(array('name'=> 'formal_id','id' => 'formal_id','class'=>'form-control-file')); ?>
                            </div>

                            <!-- <div class="form-group col-md-4">
                                <label for="inputCity"><span class="symbolcolor">*</span>City</label>
                                <?php //echo form_input(array('name'=> 'city','id' => 'city','value'=> set_value('city'),'class'=>'form-control validate[required]')); ?>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="inputZip"><span class="symbolcolor">*</span>Zipcode</label>
                                <?php //echo form_input(array('name'=> 'zipcode','id' => 'zipcode','value'=> set_value('zipcode'),'class'=>'form-control validate[required,maxSize[10]]')); ?>
                            </div> -->

                        </div>



                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="password"><span class="symbolcolor">*</span>Password</label>
                                <?php echo form_input(array('type' => 'password', 'name'=> 'password','id' => 'password','value'=> set_value('password'),'class'=>'form-control validate[required]'));?>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="confirm password"><span class="symbolcolor">*</span>Confirm Password</label>
                                <input type="password" class="form-control validate[required,equals[password]]" name="confirm_password" id="confirm_password" value="">
                            </div>
                        </div>

                        <br><br>

                        <input type="submit" name="submit" value="Register" class="btn_1 btn">

                <?php echo form_close(); ?>

            </div>
        </div>
    </div>

<?php //$this->load->view("_include/footer"); ?>
