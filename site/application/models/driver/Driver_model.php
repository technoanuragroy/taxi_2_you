<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //////////   ---------   DRIVER LOGIN  ----------   /////////////
    function login_check() {
        $password = $this->input->post("password");
        $data = array(
            "username" => $this->input->post("username"),
            "status" => 1,
            "profile_completed_steps" => 3
        );
        //echo '<pre>';print_r($data);die;
        $this->db->where($data);
        $query = $this->db->get("tbl_driver");

        $record = array();
        if ($query->num_rows() > 0) {
            $record = $query->row_array();
            if(password_verify($password, $record['password'])) {

                $driver_details = $this->my_custom_functions->get_details_from_id("", "tbl_driver", array("id" => $record["id"]));
                //echo '<pre>';print_r($rider_details);die;
                $session_data = array(
                    "driver_id" => $driver_details["id"],
                    "driver_name" => $driver_details["name"],
                    "driver_email" => $driver_details["email"],
                );
                $this->session->set_userdata($session_data);

                return 1;

            } else {

                return 0;
            }
        } else {
            return 0;
        }
    }

    ///////////TO INSERT DRIVER//////////
    function insert_driver_data($data) {
        $this->db->insert('tbl_driver', $data);
        return $this->db->insert_id();
    }
    function update_vehicle_data($data) {
      $this->db->where('id', $this->uri->segment(3));
        $return = $this->db->update('tbl_driver', $data);
        return $return;
    }

    ////////  ------- UPDATE RIDER MAIN DATA  -------   ////////
    function update_driver_data() {
      $driver_data = array(
          'name'          =>  $this->input->post('name'),
          'phone'        =>  $this->input->post('mobile'),
          'country_id'    =>  $this->input->post('country')
      );

      $this->db->where('id', $this->session->userdata('driver_id'));
      $return = $this->db->update('tbl_driver', $driver_data);
      return $return;
    }


    function get_driver_request($driver_id)
    {
      //$sql = "SELECT * FROM tbl_rides WHERE id = $driver_id AND status = 0 AND time<=CURTIME() ? ORDER BY rides_id DESC ";
      //$rides_reject_id = $this->session->userdata('rides_reject_id');
      //if($rides_reject_id == ""){
          //echo 'here';die;
      $sql = $this->db->query("SELECT * FROM tbl_rides WHERE driver_id = ".$driver_id." AND status = 0 AND request_status = 0 ORDER BY rides_id DESC LIMIT 0,1");
      //}else{
       //echo 'There';die;
      //$sql = $this->db->query("SELECT * FROM tbl_rides WHERE driver_id = ".$driver_id." AND rides_id != ".$rides_reject_id." AND status = 0 AND request_status = 0 ORDER BY rides_id DESC LIMIT 0,1");
      //echo $this->db->last_query();die;
    //}

      if ($sql->num_rows() > 0) {
      $record = $sql->row_array();
      $rides_id = $record['rides_id'];
      $html = '<div class="cpmpleat_wrappers">
    		<div class="customer_notification_message">
    			<div class="customer_notification_container">
    				<span>
    					<img src="'.base_url().'driver_assets/images/speech_bouble.png" />
    					<p>A nearby rider is requesting for taxi</p>
    				</span>
    			</div>
    		</div>
    		<div class="time_bar">
    			<span>
    				<p>
    					<b>time - </b>
    					<img src="'.base_url().'driver_assets/images/clock.png" />
    					<b id="timeleft_'.$rides_id.'"></b> seconds
    				</p>
    			</span>
    		</div>
    		<div class="address_bar" data-ridesid="'.$record['rides_id'].'">
    			<address>
    				<img src="'.base_url().'driver_assets/images/map_pointer.png" />
    				<p>
    					<b>address</b>
    					'.$record['pickup_adress'].'
    				</p>
    			</address>
    		</div>
        </div>
        <input type="hidden" id="rides_id" value="'.$record['rides_id'].'">
        ';
      return $html;
    }else{
      return 'NA';
    }
  }


  function driver_reject_request_status_update_html($driver_id,$rides_id){
      //$sql = "SELECT * FROM tbl_rides WHERE id = $driver_id AND status = 0 AND time<=CURTIME() ? ORDER BY rides_id DESC ";
      $sql = $this->db->query("SELECT * FROM tbl_rides WHERE driver_id = ".$driver_id." AND rides_id != ".$rides_id." AND status = 0 ORDER BY rides_id DESC LIMIT 0,1");

      if ($sql->num_rows() > 0) {
      $record = $sql->row_array();

      $html = '<div class="cpmpleat_wrappers">
    		<div class="customer_notification_message">
    			<div class="customer_notification_container">
    				<span>
    					<img src="'.base_url().'driver_assets/images/speech_bouble.png" />
    					<p>A nearby rider is requesting for taxi</p>
    				</span>
    			</div>
    		</div>
    		<div class="time_bar">
    			<span>
    				<p>
    					<b>time - </b>
    					<img src="'.base_url().'driver_assets/images/clock.png" />
    					<b id="timeleft">30</b> seconds
    				</p>
    			</span>
    		</div>
    		<div class="address_bar">
    			<address>
    				<img src="'.base_url().'driver_assets/images/map_pointer.png" />
    				<p>
    					<b>address</b>
    					'.$record['pickup_adress'].'
    				</p>
    			</address>
    		</div>
        </div>
        <input type="hidden" id="rides_id" value="'.$record['rides_id'].'">
        ';
      return $html;
    }
  }

  function driver_reject_request_status_update($rides_id,$data) {
    $this->db->where('rides_id', $rides_id);
    $return = $this->db->update('tbl_rides', $data);
    return $return;
  }

  function driver_chk_status_list($driver_id,$rides_id){
      //echo $driver_id;die;
    $sql = $this->db->query("SELECT * FROM tbl_rides WHERE rides_id = ".$rides_id." AND driver_id = ".$driver_id." AND status = 0 AND request_status = 0");
    //echo $this->db->last_query();die;
    if ($sql->num_rows() > 0) {
       return 1;
    }else{
       return 0;
    }
  }

  function get_rider_details_from_segment($rides_id)
  {
    $sql = $this->db->query("SELECT * FROM tbl_rides WHERE rides_id = ".$rides_id." AND request_status = 1");
    //echo $this->db->last_query();die;
    if ($sql->num_rows() > 0) {
       return $sql->row_array();
    }else{
       return false;
    }
  }

  public function get_pending_trips($driver_id)
  {
    $sql = $this->db->query("SELECT * FROM tbl_rides WHERE driver_id = ".$driver_id." AND status = 0 AND request_status = 0 ORDER BY rides_id ASC");
    // $this->db->where('driver_id',$driver_id);
    // $this->db->where('status', 0 );
  //  echo $this->db->last_query();die;
    if ($sql->num_rows() > 0) {
       return $sql->result_array();
    }else{
       return false;
    }

    //echo $driver_id;die;
  }

  public function get_completed_trips($driver_id)
  {
    $sql = $this->db->query("SELECT * FROM tbl_rides WHERE driver_id = ".$driver_id." AND status = 2 ORDER BY rides_id ASC");
    // $this->db->where('driver_id',$driver_id);
    // $this->db->where('status', 0 );
  //  echo $this->db->last_query();die;
    if ($sql->num_rows() > 0) {
       return $sql->result_array();
    }else{
       return false;
    }

    //echo $driver_id;die;
  }

  public function get_rejected_trips($driver_id)
  {
    $sql = $this->db->query("SELECT * FROM tbl_rides WHERE driver_id = ".$driver_id." AND status = 3 ORDER BY rides_id ASC");
    // $this->db->where('driver_id',$driver_id);
    // $this->db->where('status', 0 );
  //  echo $this->db->last_query();die;
    if ($sql->num_rows() > 0) {
       return $sql->result_array();
    }else{
       return false;
    }

    //echo $driver_id;die;
  }
  public function get_payment($driver_id, $rides_id)
  {
    $sql = $this->db->query("SELECT * FROM tbl_rides WHERE rides_id = ".$rides_id." AND request_status = 1 AND trip_start_status = 1 AND status = 2 AND payment_status = 0");
    //echo $this->db->last_query();die;
    return $sql->row_array();
  }








    ///////////////   -------------  #END  #END  #END  #END  --------------    /////////////////
}
