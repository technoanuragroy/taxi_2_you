<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rider_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //////////   ---------   RIDER LOGIN  ----------   /////////////
    function login_check() {
        $password = $this->input->post("password");
        $data = array(
            "username" => $this->input->post("username"),
            "status" => 1
        );

        $this->db->where($data);
        $query = $this->db->get("tbl_rider");

        $record = array();
        if ($query->num_rows() > 0) {
            $record = $query->row_array();
            if(password_verify($password, $record['password'])) {
              //echo "<pre>";print_r($data);die;
                $rider_details = $this->my_custom_functions->get_details_from_id("", "tbl_rider", array("id" => $record["id"]));
                $session_data = array(
                    "rider_id" => $rider_details["id"],
                    "rider_name" => $rider_details["name"],
                    "rider_email" => $rider_details["email"],
                );
                $this->session->set_userdata($session_data);

                return 1;

            } else {

                return 0;
            }
        } else {
            return 0;
        }
    }

    ////////   -------  TO INSERT/UPDATE RIDER  ------   ////////
    function insert_rider_data($data) {

        $this->db->insert('tbl_rider', $data);
        return $this->db->insert_id();
    }

    ////////  ------- UPDATE RIDER MAIN DATA  -------////////
    function update_rider_data() {

      $rider_data = array(
          'name'          =>  $this->input->post('name'),
          'phone'         =>  $this->input->post('mobile'),
          'country_id'    =>  $this->input->post('country')
      );

      $this->db->where('id', $this->session->userdata('rider_id'));
      $return = $this->db->update('tbl_rider', $rider_data);

      return $return;

    }

///////////////////////////////////////////////////////////////////////////////
 /// Save Place
 ///////////////////////////////////////////////////////////////////////////////
	function save_place()
    {

                    $rider_id = $this->input->post('rider_id');
                    $place_title = $this->input->post('place_title');
                    $place_address = $this->input->post('place_address');
                    $latitude = $this->input->post('latitude');
                    $longitude = $this->input->post('longitude');


                            $insertdetails = array(
                            'rider_id'=>$rider_id,
                            'place_title'=>$place_title,
                            'place_address'=>$place_address,
                            'latitude'=>$latitude,
                            'longitude'=>$longitude,
                            'status'=>1,

                            );
                    $insert=$this->db->insert('tbl_rider_save_place',$insertdetails);
                    return $insert;

        }
        ////////  ------- SEARCH DRIVER  -------////////
        function search_driver($travel_mode) {

          $lat_rider =  $this->session->userdata('frm_latitude');
          $lon_rider =  $this->session->userdata('frm_longitude');
          $this->db->where('status', 1);
          //$this->db->where('car_type',$travel_mode);
          $return = $this->db->query("SELECT *, ( 6371 * acos( cos( radians('$lat_rider') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$lon_rider') ) + sin( radians('$lat_rider') ) * sin( radians( latitude ) ) ) ) AS distance FROM tbl_driver HAVING distance < 15 AND car_type='$travel_mode' ORDER BY distance LIMIT 1");

          return $return->result_array();

        }
        ////////  ------- send_driver_request_save  -------////////
        function send_driver_request_save($data,$trip_data) {
            //echo "<pre>";print_r($trip_data);die;
            $distance = $trip_data['distance'];
            $amount = $trip_data['amount'];
            //echo $data[0]['vehicle_name'];die;
            $insertdetails = array(
            'rider_id'=>$this->session->userdata('rider_id'),
            'driver_id'=>$data[0]['id'],
            'pickup_adress'=>$this->session->userdata('ses_origin'),
            'drop_address'=>$this->session->userdata('ses_destination'),
            'pickup_location_latitude'=>$this->session->userdata('frm_latitude'),
            'pickup_location_longitute'=>$this->session->userdata('frm_longitude'),
            'drop_location_latitude'=>$this->session->userdata('to_latitude'),
            'drop_location_longitude'=>$this->session->userdata('to_longitude'),
            'distance'=>$distance,
            'amount'=>$amount,
            'car_type'=>$trip_data['travel_mode'],
            'status'=>0,

            );
    $insert=$this->db->insert('tbl_rides',$insertdetails);
    return $this->db->insert_id();
    //return $insert;



        }

        /////////////  --------- ---------- ----------- --------    /////////////
        function past_trip_records(){
          $rider_id = $this->session->userdata('rider_id');
          $order_summary_sql = ' SELECT * FROM tbl_rides WHERE rider_id = "'.$rider_id.'"';
          $order_summary__query = $this->db->query($order_summary_sql);
          return $order_summary__query->result_array();
        }

        function get_pending_request(){
            $rides_id = $this->session->userdata('act_rides_id');
            //echo $rides_id;die;
            $order_summary_sql = 'SELECT * FROM tbl_rides WHERE rides_id = "'.$rides_id.' status=0"';
            $order_summary__query = $this->db->query($order_summary_sql);
            return $order_summary__query->result_array();
          }


    function getAcceptDriverdetails($rides_id)
    {
        //echo $rides_id;die;
      $sql = $this->db->query("SELECT * FROM tbl_rides WHERE rides_id = ".$rides_id." AND status = 1 ");
      //echo $this->db->last_query();die;
      if ($sql->num_rows() > 0) {
      $record = $sql->row_array();
      //echo '<pre>';print_r($record);die;
      $driver_id = $record['driver_id'];
      $trip_start_status = $record['trip_start_status'];

      if($trip_start_status == 0)  {
        $btns = '<a href="javascript:" class="trip_btn_short" data-rides-id="'.$rides_id.'" id="cancel_rides"><i class="fa fa-times"></i>Cancel Ride</a>';
      }else{
        $btns = '<a href="'.base_url().'rider/user/payment_status/'.$rides_id.'/'.TAXI_APP_DRIVER_SECURITY_KEY.'" class="trip_btn_short" data-rides-id="'.$rides_id.'" id="pay_ride"><i class="fa fa-money"></i>Pay</a>';
      }
      $driver_details = $this->my_custom_functions->get_details_from_id("", "tbl_driver", array("id" => $driver_id));

      $make = $driver_details["vehicle_make"];
      $model = $driver_details["vehicle_model"];
      $color = $driver_details["vehicle_color"];
      $driver_id =$driver_details["id"];
      $vehicle_plate_no = $driver_details["vehicle_plate_no"];
      $driver_name = $driver_details["first_name"]. $driver_details["last_name"];

      $driver_phone = $driver_details["phone"];
      //$color = $driver_details["vehicle_make"];

      $rider_id = $this->session->userdata('rider_id');
      $currency_id = $this->my_custom_functions->get_particular_field_value("tbl_rider", "currency_id", " and id='".$rider_id."'");
      $currency_symbol = $this->my_custom_functions->get_particular_field_value("tbl_currency", "currency_symbol", " and id='".$currency_id."'");
      if($currency_id != 0 ){$currencysymbol = $currency_symbol;}else{$currencysymbol = '$';}


      $html = '<div class="client_information" id="driver_infrmtion">
      <div class="c_inform">
              <span><img src="'.base_url().'uploads/driver/idproof/'.$driver_id.'.jpg"></span>
          </div>
          <div class="c_address">
              <name>'.$driver_name.'</name>
              <b>Car details:</b>
              <ul>
                  <li>Make-  '.$make.' </li>
                  <li>Model- '.$model.'</li>
                  <li>Color-  '.$color.'</li>
              </ul>
         <div class="name_plate">
              <b>Licence plate-</b> <span class="plate">'.$vehicle_plate_no.'</span>

         </div>

    </div>
    <div class="trip_payment">
          <a href="#" class="trip_amount">'.$currencysymbol .$record['amount'].'</a>
          <a href="#" class="trip_request">Fare Estimate</a>

     </div>
</div>
<div class="confirm_btn_grup">
      <a href="tel:+'.$driver_phone.'" class="trip_btn_short"><i class="fa fa-phone"></i> Call Driver</a>'.$btns.
'</div>
        ';
      return $html;
    }
  }


  function CancelRidesRider($rides_id) {
    $data = array(
        'status' => 3
    );
    $this->db->where('id', $rides_id);
    $return = $this->db->update('tbl_driver', $data);
    return $return;
  }

  function get_eligible_customer(){
    $sql = $this->db->query("SELECT *, COUNT(*) AS COUNTS FROM tbl_referral_details GROUP BY rider_id ORDER BY id ASC");
    if ($sql->num_rows() > 0) {
       return $sql->result_array();
    }else{
       return false;
    }
  }


    ///////////////-------------  #END  #END  #END  #END  --------------/////////////////
}
