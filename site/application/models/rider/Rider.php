<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rider_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //////////   ---------   Rider LOGIN  ----------   /////////////
    function login_check() {

        $password = $this->input->post("password");
        $data = array(
            "login_id" => mysqli_real_escape_string($this->my_custom_functions->get_mysqli(), strip_tags(trim($this->input->post("login_id")))),
            "type" => 1,
            "status" => 1
        );

        $this->db->where($data);
        $query = $this->db->get("tbl_login_details");

        $record = array();
        if ($query->num_rows() > 0) {
            $record = $query->row_array();

            if(password_verify($password, $record['password'])) {

                $club_members_details = $this->my_custom_functions->get_details_from_id("", "tbl_club_members", array("id" => $record["id"]));
                $session_data = array(
                    "club_member_id" => $club_members_details["id"],
                    "club_member_username" => $club_members_details["name"],
                    "club_member_email" => $club_members_details["email"],
                    "customer_id" => $club_members_details["id"]
                );
                $this->session->set_userdata($session_data);

                return 1;

            } else {

                return 0;
            }
        } else {

            return 0;
        }
    }

    //////////   ---------   QUESTIONNAIRE  ----------   /////////////
    function get_questionnaire($ques_category_id) {

        $question_array = array();
        $question_sql = 'SELECT * FROM tbl_questionnaire_questions WHERE category_id = "'.$ques_category_id.'"';
        $question_sql_query = $this->db->query($question_sql);
        $question_array = $question_sql_query->result_array();

        foreach($question_array as $key => $value) {

            $option_sql = 'SELECT * FROM tbl_questionnaire_options WHERE question_id = "'.$value['id'].'" ';
            $option_sql_query = $this->db->query($option_sql);
            $option_array = $option_sql_query->result_array();

            $question_array[$key]['options'] = $option_array;

            foreach($question_array[$key]['options'] as $option_key => $option_value) {

                $option_sql = 'SELECT * FROM tbl_questionnaire_options WHERE option_id = "'.$option_value['id'].'" ';
                $option_sql_query = $this->db->query($option_sql);
                $sub_option_array = $option_sql_query->result_array();

                $question_array[$key]['options'][$option_key]['sub_options'] = $sub_option_array;
            }
        }

        return $question_array;
    }

    ////////   -------  TO INSERT/UPDATE CLUB MEMBERS  ------   ////////
    function insert_club_member_data($data) {

        $this->db->insert('tbl_club_members', $data);
        return $this->db->insert_id();
    }

    ////////   --------  GET DETAILS OF POSTED OPTION ANSWERS   ------    ////////
    function get_details_of_posted_options($option_id) {

        $option_details_sql = 'SELECT * FROM tbl_questionnaire_options WHERE id = "'.$option_id.'" ';
        $option_details_query = $this->db->query($option_details_sql);
        return $option_details_query->row_array();
    }

    ////////  ------- INSERT INTO ANSWERS TABLE  -------   ////////
    function insert_questionnaire_answers($data) {

        $this->db->insert('tbl_questionnaire_answers', $data);
        return;
    }

    ////////  ------- UPDATE CLUB MEMBER MAIN DATA  -------   ////////
    function update_club_member_data() {

        $club_member_data = array(
            'name'          =>  $this->input->post('name'),
            'mobile'        =>  $this->input->post('mobile'),
            'email'         =>  $this->input->post('email'),
            'address'       =>  $this->input->post('address'),
            'country'       =>  $this->input->post('country'),
            'city'          =>  $this->input->post('city'),
            'zip_code'      =>  $this->input->post('zipcode')
        );

        $this->db->where('id', $this->session->userdata('club_member_id'));
        $return = $this->db->update('tbl_club_members', $club_member_data);

        return $return;
    }

    ////////   ----------- ----------- --------------    /////////
    function club_member_det($club_member_id){
      $this->db->where('id', $club_member_id);
      $query = $this->db->get('tbl_club_members');
      if($query->num_rows() > 0){
        $club_members_details = $query->row_array();
        $session_data = array(
            "club_member_id" => $club_members_details["id"],
            "club_member_username" => $club_members_details["name"],
            "club_member_email" => $club_members_details["email"],
            "customer_id" => $club_members_details["id"]
        );
        $this->session->set_userdata($session_data);

        return 1;
      }
    }


    /////////////  --------- ---------- ----------- --------    /////////////
    function order_summary($customer_id){
      $order_summary_sql = ' SELECT * FROM tbl_order_main WHERE customer_id = "'.$customer_id.'" ORDER BY order_date DESC LIMIT 0, 10';
      $order_summary__query = $this->db->query($order_summary_sql);
      return $order_summary__query->result_array();
    }

    ///////////////   -------------  #END  #END  #END  #END  --------------    /////////////////
}
