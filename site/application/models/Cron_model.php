<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'controllers/aws/aws-autoloader.php';
use Aws\S3\S3Client;
class Cron_model extends CI_Model {
    
    public function __construct() {
            parent::__construct();
    }	
    function get_details_from_id($id = "", $table, $extra = array()) {
    if ($id != "") {
            $this->db->where("id", $id);
        }
        if (count($extra) > 0) {
            $this->db->where($extra);
        }
        $query = $this->db->get($table);

        $record = array();
        if ($query->num_rows() > 0) {
            $record = $query->row_array();
        }
        return $record;
    }
    //// delete demo company Temporary purpose///
    function del_demo_company(){

        $this->benchmark->mark('code_start');
                    //  Ablion IT Solutions Pvt Ltd 	1
                    // 	soumyadeep@ablion		80
                    //	Das&Mukherjee 			97
                    //	BigandHot Advertising 		101
                    // 	qwerty pvt.ltd 			106
        $sql = "SELECT id FROM tbl_login_details WHERE ( id NOT IN(1,80,97,101,106) AND type=1) AND status=1 ORDER BY id ASC LIMIT 1000";
        $query = $this->db->query($sql);
        $comIDs = $query->result_array();
        $counter=1;
        if(!empty($comIDs)){
            //echo "<pre>";print_r($comIDs);
                foreach($comIDs as $row) { //echo $row['id']; die;
                    //echo $counter.'.......COMPANY ID IS...........'.$row['id'].'<br>';
                    $existing_deletion_record = $this->my_custom_functions->get_details_from_id("", "tbl_del_user", array("user_id" => $row['id']));
                            if(empty($existing_deletion_record)){
                                   $this->db->where('id', $row['id']);
                                   $update_data = array('status'=>2); //PENDING DELETE
                                   $updated = $this->db->update('tbl_login_details', $update_data);
                                   $deletion_record = array(
                                                           "user_id"=>$row['id'],
                                                           "request_date"=>time(),
                                                           "delete_date"=>time()//strtotime("+".DELETE_PROCESS_START_DAY."day")
                                                       );
                                   $inserted = $this->db->insert("tbl_del_user", $deletion_record);
                               }
                    $counter ++;
                }
       }
       
       $this->benchmark->mark('code_end');
       echo "TIME TAKEN : ".$this->benchmark->elapsed_time('code_start', 'code_end');
    }
    
    
    
    ///////// DELETE COMPANY START HERE //////////
     function set_delete_company(){ /// NEED TO RUN  PER HOUR
         $this->benchmark->mark('code_start');
         
         $this->db->select("user_id,delete_date,request_date");
         $this->db->where("delete_date <=",time()); //FOR TESTING PURPOSE IT IS >=, IT SHOULD BE =,  <= in order to delete old record also
         $query = $this->db->get('tbl_del_user',1000);
         
          $sql = "SELECT tbl_login_details.type,tbl_del_user.user_id 
                    FROM tbl_del_user
                    INNER JOIN tbl_login_details ON tbl_del_user.user_id = tbl_login_details.id 
                    WHERE ( tbl_login_details.type =1 AND delete_date <= ".time()." ) LIMIT 1000";
          $query = $this->db->query($sql);
          $deleted_company = $query->result_array();
            
         if (!empty($deleted_company)) {
             foreach($query->result_array() as $row) {
                 $company_details = $this->get_details_from_id($row['user_id'], "tbl_login_details",array('type'=>1));
                 if(!empty($company_details)){
                        // set company to delete in PROGRESS status, which is 3
                        $this->db->where('id', $row['user_id']);
                        $update_data = array('status'=>3); //DELETE IN PROGRESS
                        $updated = $this->db->update('tbl_login_details', $update_data);
                        $employeeIDs = $this->my_custom_functions->get_company_employeesID($row['user_id']);
                        /// After settting up company in delete in process, schedule all its employee direct to the Delete in process immediately
                        if($updated){
                                if($employeeIDs != ""){
                                    $emp_ids = explode(",",$employeeIDs);
                                        foreach($emp_ids as $empID){
                                                $existing_deletion_record = $this->my_custom_functions->get_details_from_id("", "tbl_del_user", array("user_id" => $empID));
                                                if(empty($existing_deletion_record)){ // checking wether the employee is already in delete list or not
                                                        // if not in delete list set it into delete list
                                                        $this->db->where('id', $empID);
                                                        $update_data = array('status'=>3); // DELETE in PROCESS
                                                        $updated = $this->db->update('tbl_login_details', $update_data);
                                                        /// AFTER CHANGING THE STATUS INSERT IT TO TBL_DEL_USER AND SET THE DELETE DATE IMMEDIATELY
                                                        $deletion_record = array(
                                                                            "user_id"=>$empID,
                                                                            "request_date"=>time(),
                                                                            "delete_date"=>time()
                                                                        );
                                                        $inserted = $this->db->insert("tbl_del_user", $deletion_record);
                                                }
                                            }
                                        }
                                    }   
                                }
                            }
                        }
                        
            $this->benchmark->mark('code_end');
            echo "TIME TAKEN : ".$this->benchmark->elapsed_time('code_start', 'code_end');            
    }
    function delete_company_record(){ /// NEED TO RUN ONCE PER HOUR
        $this->benchmark->mark('code_start');
        $sql = "SELECT tbl_login_details.type,tbl_del_user.user_id 
                    FROM tbl_del_user
                    INNER JOIN tbl_login_details ON tbl_del_user.user_id = tbl_login_details.id 
                    WHERE ( tbl_login_details.type =1 AND delete_date <= ".time()." ) LIMIT 1000";
        
            $query = $this->db->query($sql);
            $deleted_users = $query->result_array();
            if (!empty($deleted_users)) {
                //echo '<pre>';print_r($deleted_users); die;
                    foreach($deleted_users as $row) { 
                              $employee_counter = $this->my_custom_functions->get_perticular_count("tbl_employees"," AND company_id='".$row['user_id']."'");
                                    if($employee_counter == 0){
                                                          //// deletion tbl_employees////////
                                                             $this->db->where('company_id',$row['user_id']);
                                                             $delete = $this->db->delete('tbl_companies');
                                                         //// deletion tbl_login_details////////
                                                             $this->db->where('id',$row['user_id']);
                                                             $delete = $this->db->delete('tbl_login_details');
                                                         //// deletion tbl_del_user////////
                                                             $this->db->where('user_id',$row['user_id']);
                                                             $delete = $this->db->delete('tbl_del_user');

                                                      }
                                            }
                                        }   
                                    
            $this->benchmark->mark('code_end');
            echo "TIME TAKEN : ".$this->benchmark->elapsed_time('code_start', 'code_end');  
        }
    ////////// DELETE USER USING CRON START HERE///////////////
    function set_delete_record(){
        $this->benchmark->mark('code_start');
        $counter = 0;
        $sql = "SELECT tbl_login_details.type,tbl_del_user.user_id 
                        FROM tbl_del_user
                        INNER JOIN tbl_login_details ON tbl_del_user.user_id = tbl_login_details.id 
                        WHERE ( tbl_login_details.type !=1 AND delete_date <= ".time()." ) LIMIT 15";
        $query = $this->db->query($sql);
        $deleted_users = $query->result_array(); 
         
         if ($query->num_rows() > 0) { 
             // foreach($query->result_array() as $row) { 
              foreach($deleted_users as $row) {     
                            $this->db->select("id");
                            $this->db->where("id",$row['user_id']);
                            $Checking_query = $this->db->get('tbl_login_details');
                            //echo "ssss"; echo $this->db->last_query(); die;
                             if ($Checking_query->num_rows() > 0) { 
                                          //echo "<pre>";print_r($row);
                                         
                                          $this->db->where('id', $row['user_id']);
                                          $update_data = array('status'=>3); //DELETE IN PROGRESS
                                          $updated = $this->db->update('tbl_login_details', $update_data);
                                          //echo $this->db->last_query(); die;

                                          /// DELETE FROM tbl_register_device_api
                                              $this->db->where("emp_id",$row['user_id']);
                                              $this->db->delete('tbl_register_device_api');

                                          /// DELETE FROM tbl_work
                                              $this->db->where("ref_emp_id",$row['user_id']);
                                              $this->db->delete('tbl_works');

                                          /// DELETE FROM tbl_emp_gps
                                              $this->db->where("ref_emp_id",$row['user_id']);
                                              $this->db->delete('tbl_emp_gps'); 

                                          /// DELETE FROM tbl_expenses
                                              $this->db->where("ref_emp_id",$row['user_id']);
                                              $this->db->delete('tbl_expenses'); 

                                          /// DELETION OF CHECKIN RECORD AND INSERTION OF CHECKIN PHOTO ON DELETION IMAGE TABLE

                                             $checkinIds = "";
                                             $this->db->select('checkin_id');
                                             $this->db->where("ref_emp_id",$row['user_id']);
                                             $checkin_query = $this->db->get("tbl_checkins",100);
                                             $usr_rec = $checkin_query->result_array();

                                              if(!empty($usr_rec)){
                                                  foreach($usr_rec as $key=>$val){
                                                      $checkinPhoto_record = $this->get_details_from_id("", "tbl_checkin_photos", array("check_in_id" => $val['checkin_id']));
                                                      if(!empty($checkinPhoto_record)){ // if related checkin record is present on tbl_checkin_photos
                                                                      //Insert record in image deletion table
                                                                          $img_record = array('img_url'=>$checkinPhoto_record['photo_url'],
                                                                                              'big_img_url'=>$checkinPhoto_record['big_photo_url']
                                                                                          );
                                                                          $this->db->insert("tbl_img_deletion", $img_record);
                                                                      // delete checkin record from tbl_checkin_photos
                                                                          $this->db->where('id',$checkinPhoto_record['id']);
                                                                          $this->db->delete('tbl_checkin_photos');
                                                                          // delete checkin record from tbl_checkins
                                                                          $this->db->where('checkin_id',$val['checkin_id']);
                                                                          $this->db->delete('tbl_checkins');
                                                      }else{      //Delete the checkin record from tbl_checkins
                                                                      $this->db->where('checkin_id',$val['checkin_id']);
                                                                      $this->db->delete('tbl_checkins');
                                                      }
                                                      $counter++;
                                                  }
                                              }
                                            }
                                        }
                                    }
        $this->benchmark->mark('code_end');
        echo "TIME TAKEN : ".$this->benchmark->elapsed_time('code_start', 'code_end');                     
     }
     //DELETION OF CHECKIN PHOTOS FROM  tbl_img_deletion
     function delete_checkin_photos() { 
         $this->benchmark->mark('code_start');
         
        $s3 = S3Client::factory(array(
            'version' => 'latest',
            'region'  => 'ap-south-1',
            'credentials' => array(
                'key'    => 'AKIAJHQVWZJHQFU23JKA',
                'secret' => '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',
            ),
        ));
        $bucket = AMAZON_BUCKET; 
        $multipleObjects = array();
        $this->db->order_by('id ASC'); 
        $imgRecord = $this->db->get('tbl_img_deletion',0,DEL_IMAGE_LIMITATION_FOR_CRON);
        $record = $imgRecord->result_array();
        $limit_counter = 0;
        $counter = 0;
        //echo "<pre>";print_r($record); die;
        if(!empty($record)) {
            foreach ($imgRecord->result_array() as $key=>$val){
                    $photoUrlArray = explode("/",$val['img_url']);
                    $awsKey = $photoUrlArray[count($photoUrlArray) - 1];
                    $multipleObjects[$counter] = array('Key' => $awsKey);                          
                      if($val['big_img_url'] != ""){
                                $photoUrlArray2 = explode("/",$val['big_img_url']);
                                $awsKey2 = $photoUrlArray2[count($photoUrlArray2) - 1];
                                $counter++;
                                $multipleObjects[$counter] = array('Key' => $awsKey2);  
                        }      
                    $counter++;
                    $limit_counter++;
            }
          // echo '<pre>'; print_r($multipleObjects); die;
          
    /// COMMENT-OUT FOR NOW IN ORDER TO DELETE DEMO DATA         
        /*    try {

                $result = $s3->deleteObjects(array(
                    'Bucket' => $bucket,                    
                    'Delete' => array( // REQUIRED
                        'Objects' => $multipleObjects,
                        'Quiet' => true,
                    ),
                ));
              // echo "<pre>";print_r($result); 
            } catch (S3Exception $e) {
                echo $e->getMessage() . "\n";
            }
            
        */    
            //$this->db->empty_table('tbl_img_deletion'); 
            $del_sql = "DELETE FROM tbl_img_deletion WHERE 1= 1 LIMIT ".$limit_counter;
            $query = $this->db->query($del_sql);
        }
        
     $this->benchmark->mark('code_end');
     echo "TIME TAKEN : ".$this->benchmark->elapsed_time('code_start', 'code_end');    
    }
    function delete_user_record(){
        $this->benchmark->mark('code_start');
        
            // Amazon Web server credentials
        /// COMMENT-OUT FOR NOW IN ORDER TO DELETE DEMO DATA 
            /*$s3 = new S3Client(array(
                                'version' => 'latest',
                                'region'  => 'ap-south-1',
                                'credentials' => array(
                                                        'key'    => 'AKIAJHQVWZJHQFU23JKA',
                                                        'secret' => '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',
                                                    ),
                                ));
            $bucket = AMAZON_BUCKET; 
            */
            
            $sql = "SELECT tbl_login_details.type,tbl_del_user.user_id 
                    FROM tbl_del_user
                    INNER JOIN tbl_login_details ON tbl_del_user.user_id = tbl_login_details.id 
                    WHERE ( tbl_login_details.type !=1 AND delete_date <= ".time()." ) LIMIT 500";
        
            $query = $this->db->query($sql);
            $deleted_users = $query->result_array();
            //echo "<pre>";print_r($deleted_users); die;
            if (!empty($deleted_users)) { 
                    foreach($deleted_users as $row) {
                            $work_counter = $this->my_custom_functions->get_perticular_count("tbl_works"," AND ref_emp_id='".$row['user_id']."'");
                            $emp_gps_counter = $this->my_custom_functions->get_perticular_count("tbl_emp_gps"," AND ref_emp_id='".$row['user_id']."'");
                            $expenses_counter = $this->my_custom_functions->get_perticular_count("tbl_expenses"," AND ref_emp_id='".$row['user_id']."'");
                            if($work_counter == 0 && $emp_gps_counter == 0 && $expenses_counter == 0 ){
                                    
                                $photoUrl = $this->my_custom_functions->get_particular_field_value("tbl_employees", "photo_url", " and emp_id='".$row['user_id']."'");
                                                
                /// COMMENT-OUT FOR NOW IN ORDER TO DELETE DEMO DATA                                
                                             /*   if($photoUrl != ""){ /// If profile image is available then delete the image first
                                                        try {
                                                               if($photoUrl != "") {
                                                                            // echo $photoUrl; die;
                                                                             $photoUrlArray = explode("/",$photoUrl);
                                                                             $awsKey = $photoUrlArray[count($photoUrlArray) - 1];

                                                                             $result = $s3->deleteObject(array(
                                                                                 'Bucket' => $bucket,
                                                                                 'Key' => $awsKey
                                                                             ));
                                                                    }

                                                            } catch (S3Exception $e) {
                                                                        //echo $e->getMessage() . "\n";
                                                         }
                                                    }  */
                                                    
                                                    // Insert to log
                                                    $company_id = $this->my_custom_functions->get_particular_field_value("tbl_employees", "company_id", " and emp_id=" . $row['user_id'] . " ");
                                                    $this->my_custom_functions->insert_data(array("company_id" => $company_id, "emp_id" => $row['user_id'], "activity_date" => time(), "activity" => "User Deleted Permanently"), "tbl_company_activity_log");    
                                
                                                    //// deletion tbl_employees////////
                                                    $this->db->where('emp_id',$row['user_id']);
                                                    $delete = $this->db->delete('tbl_employees');
                                                    //// deletion tbl_login_details////////
                                                    $this->db->where('id',$row['user_id']);
                                                    $delete = $this->db->delete('tbl_login_details');
                                                    //// deletion tbl_del_user////////
                                                    $this->db->where('user_id',$row['user_id']);
                                                    $delete = $this->db->delete('tbl_del_user');                                                                                                        
                                            }
                                        
                                }
                            }
     $this->benchmark->mark('code_end');
     echo "TIME TAKEN : ".$this->benchmark->elapsed_time('code_start', 'code_end');                       
    }
    
 ////// DELETE USER USING CRON ENDS HERE///////////////   
function delete_user_images(){
        $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region'  => 'ap-south-1',
                                'credentials' => array(
                                                        'key'    => 'AKIAJHQVWZJHQFU23JKA',
                                                        'secret' => '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',
                                                    ),
                                ));
            $bucket = AMAZON_BUCKET; 
        
        
         $this->db->select("user_id,delete_date,request_date");
         $this->db->where("delete_date <=",time()); //FOR TESTING PURPOSE IT IS >=, IT SHOULD BE =, <= in order to delete old record
         $query = $this->db->get('tbl_del_user');
         //echo "<pre>";print_r($query->result_array()); die;
         
         //echo $query->num_rows(); die;
         if ($query->num_rows() > 0) { 
              foreach($query->result_array() as $row) {
                        $sql = "SELECT tbl_checkins.checkin_id,tbl_checkins.ref_emp_id,tbl_checkin_photos.photo_url,tbl_checkin_photos.big_photo_url"
                            . " FROM tbl_checkins"
                            . " INNER JOIN tbl_checkin_photos ON tbl_checkins.checkin_id = tbl_checkin_photos.check_in_id "
                            . " WHERE tbl_checkins.ref_emp_id=".$row['user_id']." LIMIT 100"; 
                   
                   $query = $this->db->query($sql);
                   $checkin_record = $query->result_array();
                   $record = array();
                   
                        if ($query->num_rows() > 0) {
                                foreach($checkin_record as $row) {
                                    if(!empty($row)){
                                             // Instantiate an Amazon S3 client.
                                        if($row['photo_url'] != ""){
                                              try {
                                                $photoUrl = $row['photo_url'];
                                                if($photoUrl != "") {
                                                            // echo $photoUrl; die;
                                                             $photoUrlArray = explode("/",$photoUrl);
                                                             $awsKey = $photoUrlArray[count($photoUrlArray) - 1];

                                                             $result = $s3->deleteObject(array(
                                                                 'Bucket' => $bucket,
                                                                 'Key' => $awsKey
                                                             ));
                                                        }
                                                    
                                            } catch (S3Exception $e) {
                                                //echo $e->getMessage() . "\n";
                                            }
                                        }
                                        if($row['big_photo_url'] != ""){
                                              try {
                                                $photoUrl = $row['big_photo_url'];
                                                if($photoUrl != "") {
                                                            // echo $photoUrl; die;
                                                             $photoUrlArray = explode("/",$photoUrl);
                                                             $awsKey = $photoUrlArray[count($photoUrlArray) - 1];

                                                             $result = $s3->deleteObject(array(
                                                                 'Bucket' => $bucket,
                                                                 'Key' => $awsKey
                                                             ));
                                                        }
                                                    
                                            } catch (S3Exception $e) {
                                                //echo $e->getMessage() . "\n";
                                                }
                                            }
                                            $this->db->where('checkin_id',$row['checkin_id']);
                                            $delete = $this->db->delete('tbl_checkins');
                                            
                                            $this->db->where('check_in_id',$row['checkin_id']);
                                            $delete = $this->db->delete('tbl_checkin_photos');
                                            
                                        }
                                    }
                                }else{
                                        
                                       
                                   }
                                }
                            }
    }
    
    
        //// define get particular field by value  function in order to reduce loading time///
     function get_particular_field_value($tablename, $fieldname, $where = "") {
        $str = "select " . $fieldname . " from " . $tablename . " where 1=1 " . $where;

        $query = $this->db->query($str);
        $record = "";
        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $field_arr = explode(" as ", $fieldname);
                if (count($field_arr) > 1) {
                    $fieldname = $field_arr[1];
                } else {
                    $fieldname = $field_arr[0];
                }
                $record = $row[$fieldname];
            }
        }
        return $record;
    }
    
    function delete_checkinRecord_on_user_type(){
        $todaysTimestamp = strtotime(date("Y-m-d 00:00:00"));
         $com_sql = "SELECT tbl_companies.company_id,
                        tbl_employees.emp_id,tbl_company_package.package_id 
                        FROM tbl_employees
                        INNER JOIN tbl_companies ON tbl_companies.company_id = tbl_employees.company_id
                        INNER JOIN tbl_company_package ON tbl_company_package.company_id = tbl_companies.company_id
                        ORDER BY tbl_companies.company_id LIMIT 100";
        //echo "<br><br><br><br>";
        $com_query = $this->db->query($com_sql);
        $counter = 0;
        $free_package_data = array();
        $free_package_employee_ids = "";
        
        $basic_package_data = array();
        $basic_package_employee_ids = "";
        
        if ($com_query->num_rows() > 0) {
            $com_query_data = $com_query->result_array();
            
            foreach($com_query_data as $key=>$val) {
                $com_query_data[$key]['package_type'] = $this->get_particular_field_value("tbl_packages","type", " and id='".$val['package_id']."'  order by id DESC");
                //$employee_ids .= $row['emp_id'].',';
            }
            foreach($com_query_data as $key=>$val){
                if($val['package_type'] == 1){ // free package
                    $free_package_data[$counter] = $val;
                    $free_package_employee_ids .=$val['emp_id'].',';
                }
                 if($val['package_type'] == 2){ // basic package
                    $basic_package_data[$counter] = $val;
                    $basic_package_employee_ids .=$val['emp_id'].',';
                }
                $counter++;
            }
            if($free_package_employee_ids != ""){
                $free_package_employee_ids = rtrim($free_package_employee_ids,',');
            }
            if($basic_package_employee_ids != ""){
                $basic_package_employee_ids = rtrim($basic_package_employee_ids,',');
            }

            ////// FOR FREE PACKAGES //////
            $lastrangeDatetimestampForFree_Account = strtotime ( '-'.FREE_PACKAGE_HISTORY_LIMIT.' day' ,  $todaysTimestamp  ) ;
            //echo "<br>".date("d/m/Y h:i:s",$lastrangeDatetimestampForFree_Account).'<br>';
            if($free_package_employee_ids != ""){
                            /// delete  record from tbl_attendance
                                $attendance_sql = "DELETE FROM tbl_attendance WHERE  ref_emp_id IN(".$free_package_employee_ids.")  AND  attendance_date < ".$lastrangeDatetimestampForFree_Account."  ";
                                $attendance_query = $this->db->query($attendance_sql);   
                            //// DELETE record from tbl_expenses
                            $expense_sql = "DELETE FROM tbl_expenses WHERE  ref_emp_id IN(".$free_package_employee_ids.")  AND  expense_date < ".$lastrangeDatetimestampForFree_Account."";
                            $expense_query = $this->db->query($expense_sql);
                            
                            //// DELETE record from tbl_works
                            $work_sql = "DELETE FROM tbl_works WHERE  ref_emp_id IN(".$free_package_employee_ids.")  AND  add_datetime < ".$lastrangeDatetimestampForFree_Account."";
                            $work_query = $this->db->query($work_sql);
                            
                             //// DELETE record from tbl_expenses
                                       
                         //// SELECT RECORD FROM TBL CHECK-INS 
                        //// MOVE RELATED CHECK-INS PHOTOS TO TBL_IMG_DELETION
                       //// AND DELETE THE CHECKINS RECORD FROM TBL_CHECK-INS AND TBL_CHECK-INS PHOTOS
                       $Freecounter = 0;     
                                $checkin_sql = "Select * FROM tbl_checkins WHERE  ref_emp_id IN(".$free_package_employee_ids.")  AND  checkin_date < ".$lastrangeDatetimestampForFree_Account."  LIMIT 100";       
                                $checkin_query = $this->db->query($checkin_sql);
                                        if ($checkin_query->num_rows() > 0) {
                                            $checkin_result = $checkin_query->result_array();
                                           // echo "<pre>";print_r($checkin_result);
                                            if(!empty($checkin_result)){
                                            foreach($checkin_result as $key=>$val){
                                                $checkinPhoto_record = $this->get_details_from_id("", "tbl_checkin_photos", array("check_in_id" => $val['checkin_id']));
                                                if(!empty($checkinPhoto_record)){ 
                                                            // if related checkin record is present on tbl_checkin_photos
                                                            //Insert record in image deletion table
                                                                 $img_record = array('img_url'=>$checkinPhoto_record['photo_url'],
                                                                                     'big_img_url'=>$checkinPhoto_record['big_photo_url']
                                                                                 );
                                                                 $this->db->insert("tbl_img_deletion", $img_record);

                                                                 // delete checkin record from tbl_checkin_photos
                                                                         $this->db->where('id',$checkinPhoto_record['id']);
                                                                         $this->db->delete('tbl_checkin_photos');
                                                                 // delete checkin record from tbl_checkins
                                                                         $this->db->where('checkin_id',$val['checkin_id']);
                                                                         $this->db->delete('tbl_checkins');
                                                }else{
                                                            //Delete the checkin record from tbl_checkins
                                                            $this->db->where('checkin_id',$val['checkin_id']);
                                                            $this->db->delete('tbl_checkins');
                                                }
                                                 $Freecounter++;
                                            }
                                        }
                                    }
                                }
        ///////////// for basic package /////////////////////////////                         
       $lastrangeDatetimestamp = strtotime ( '-'.BASIC_PACKAGE_HISTORY_LIMIT.' day' ,  $todaysTimestamp  ) ;
       //echo "<br>".date("d/m/Y h:i:s",$lastrangeDatetimestamp); 
                          if($basic_package_employee_ids != ""){
                            /// record from tbl_attendance
                                $attendance_sql = "DELETE FROM tbl_attendance WHERE  ref_emp_id IN(".$basic_package_employee_ids.")  AND  attendance_date < ".$lastrangeDatetimestamp."  ";
                                $attendance_query = $this->db->query($attendance_sql);   
                                
                                //// select record from tbl_expenses
                                $expense_sql = "DELETE  FROM tbl_expenses WHERE  ref_emp_id IN(".$basic_package_employee_ids.")  AND  expense_date < ".$lastrangeDatetimestamp."";
                                $expense_query = $this->db->query($expense_sql);
                                
                                //// DELETE record from tbl_works
                                $work_sql = "DELETE FROM tbl_works WHERE  ref_emp_id IN(".$basic_package_employee_ids.")  AND  add_datetime < ".$lastrangeDatetimestamp."";
                                $work_query = $this->db->query($work_sql);
                 
                         //// SELECT RECORD FROM TBL CHECK-INS 
                        //// MOVE RELATED CHECK-INS PHOTOS TO TBL_IMG_DELETION
                       //// AND DELETE THE CHECKINS RECORD FROM TBL_CHECK-INS AND TBL_CHECK-INS PHOTOS
                        $basicCounter = 0;    
                                $checkin_sql = "Select * FROM tbl_checkins WHERE  ref_emp_id IN(".$basic_package_employee_ids.")  AND  checkin_date < ".$lastrangeDatetimestamp."  LIMIT 100";       
                                $checkin_query = $this->db->query($checkin_sql);
                                        if ($checkin_query->num_rows() > 0) {
                                            $checkin_result = $checkin_query->result_array();
                                           // echo "<pre>";print_r($checkin_result);
                                            if(!empty($checkin_result)){
                                            foreach($checkin_result as $key=>$val){
                                                $checkinPhoto_record = $this->get_details_from_id("", "tbl_checkin_photos", array("check_in_id" => $val['checkin_id']));
                                                if(!empty($checkinPhoto_record)){ 
                                                            // if related checkin record is present on tbl_checkin_photos
                                                            //Insert record in image deletion table
                                                                 $img_record = array('img_url'=>$checkinPhoto_record['photo_url'],
                                                                                     'big_img_url'=>$checkinPhoto_record['big_photo_url']
                                                                                 );
                                                                 $this->db->insert("tbl_img_deletion", $img_record);

                                                                 // delete checkin record from tbl_checkin_photos
                                                                         $this->db->where('id',$checkinPhoto_record['id']);
                                                                         $this->db->delete('tbl_checkin_photos');
                                                                 // delete checkin record from tbl_checkins
                                                                         $this->db->where('checkin_id',$val['checkin_id']);
                                                                         $this->db->delete('tbl_checkins');
                                                }else{
                                                            //Delete the checkin record from tbl_checkins
                                                            $this->db->where('checkin_id',$val['checkin_id']);
                                                            $this->db->delete('tbl_checkins');
                                                }
                                               $basicCounter++;  
                                            }
                                        }
                                    }
                                }    
                            }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////    
    
    function delete_api_logs() {
        
        $date_dl = date("Y-m-d 00:00:00", strtotime('-30 days'));
        $sql = "DELETE FROM tbl_api_log WHERE transaction_time < '".$date_dl."'";
        $query = $this->db->query($sql);                
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////  
    
    function delete_activity_log() {
     
        $date_dl = strtotime(date("Y-m-d 00:00:00", strtotime('-30 days')));
        $sql = "DELETE FROM tbl_company_activity_log WHERE activity_date < '".$date_dl."'";
        $query = $this->db->query($sql); 
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    
    function delete_email_logs() {
     
        $date_dl = strtotime(date("Y-m-d 00:00:00", strtotime('-30 days')));
        $sql = "DELETE FROM tbl_email_log WHERE request_time < '".$date_dl."'";
        $query = $this->db->query($sql); 
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////    
    
    function get_api_log_counter(){
        $sql = "SELECT COUNT(id) AS counter FROM tbl_api_log WHERE transaction_time > DATE_SUB( NOW( ) , INTERVAL ".DIGEST_EMAIL_FREQUENCY_HOUR." HOUR ) AND transaction_time <= NOW( )";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $counter = $result->counter;
    }
    function get_counter($counter_field,$table,$condition_field){
                $sql = "SELECT COUNT(".$counter_field.") AS counter FROM ".$table." WHERE ".$condition_field." > UNIX_TIMESTAMP( NOW( ) - INTERVAL ".DIGEST_EMAIL_FREQUENCY_HOUR." HOUR )";
                $query = $this->db->query($sql);
                $result = $query->row();
                return $counter = $result->counter;
    }
    function send_daily_digest_email(){
         ////// company counter /////
            $context_string = "<h2>Daily digest email for last 24 hours</h2><br>";
                
                $company_counter = $this->get_counter("company_id","tbl_companies","join_date");
                $context_string .=" No of New Registrations: ".$company_counter." <br>";
            ////// user counter /////////
                $user_counter = $this->get_counter("emp_id","tbl_employees","join_date"); 
                $context_string .="<br> No of Users Added: ".$user_counter." <br>";
            ////// checkins  counter ///////// 
                $checkin_counter = $this->get_counter("checkin_id","tbl_checkins","checkin_date");  
                $context_string .="<br> No of Check-ins: ".$checkin_counter." <br>";
            ////// Expense  counter///////// 
                 $expense_counter = $this->get_counter("expense_id","tbl_expenses","expense_date");  
                 $context_string .="<br> No of Expense added: ".$expense_counter." <br>";  
                 $context_string; 
            ////// Work  counter///////// 
                 $work_counter = $this->get_counter("work_id","tbl_works","add_datetime");  
                 $context_string .="<br> No of Work Assigned: ".$work_counter." <br></span><br>"; 
            ////// API log counter ///////     
                 $API_log_counter = $this->get_api_log_counter();  
                 $context_string .="<br> No of API Logs: ".$API_log_counter." <br>";
                return  $context_string;     
                
           
    }
    function get_total_employee_by_companyID($company_id){
        $sql = "SELECT COUNT(emp_id) AS emp_counter FROM tbl_employees WHERE company_id=".$company_id;
        $query = $this->db->query($sql);
        $result = $query->row();
        return $counter = $result->emp_counter;
    }
    function get_company_employeesID($company_id) {
        $employeeId = "";
        $this->db->select('emp_id');
        $this->db->where('company_id', $company_id);
        $query = $this->db->get('tbl_employees');
        $result = $query->result_array();
        if (!empty($result)) {
                foreach ($result as $row) {
                    $status = $this->get_particular_field_value("tbl_login_details", "status", " and id='" . $row['emp_id'] . "'");
                        if ($status == 1) { // if employee is active
                            $employeeId .= $row['emp_id'] . ',';
                        }
                    }
            return rtrim($employeeId, ',');
        }
    }
    function get_company_user_last_login_date_diff($company_id){
        $employeeIds = $this->get_company_employeesID($company_id);
        $login_date_diff = "";
        if($employeeIds != ""){
            $sql = "SELECT last_login_date FROM tbl_login_details WHERE ( id IN(".$employeeIds.") AND status = 1 ) ORDER BY id DESC LIMIT 0,1";
            $query = $this->db->query($sql);
             if ($query->num_rows() > 0) {
                        $result = $query->row();
                        $login_date = $result->last_login_date;
                        $day_diff = round((time() - $login_date)/(60 * 60 * 24));
                            if($day_diff >= 1){
                                   $login_date_diff = $day_diff;
                               }else{
                                   $login_date_diff = 0;
                               }
                }
            }
            return $login_date_diff;
    }
    function sendReminderEmail(){
            $record = array();
            $sql = "SELECT tbl_login_details.last_login_date,company_id,company_name,email,email_verification,join_date,contact_person 
                    FROM tbl_companies
                    LEFT JOIN tbl_login_details ON tbl_companies.company_id = tbl_login_details.id WHERE 
                    ( tbl_companies.email_verification= 0 OR tbl_login_details.last_login_date >= CURDATE() - INTERVAL 7 DAY ) 
                    AND  tbl_login_details.status = 1";
            $query = $this->db->query($sql);
            $result = $query->result_array();
                if(!empty($result)){
                    foreach($result as $key=>$val){
                                $result[$key]['modified_date_last_login'] = date('d/m/Y',$val['last_login_date']);
                                $day_diff = round((time() - $val['last_login_date'])/(60 * 60 * 24));
                                    if($day_diff >= 1){
                                           $result[$key]['login_date_diff'] = $day_diff;
                                       }else{
                                           $result[$key]['login_date_diff'] = 0;
                                       }
                                $join_date_diff =   round((time() - $val['join_date'])/(60 * 60 * 24)); 
                                    if($join_date_diff >= 1){
                                         $result[$key]['join_date_diff'] = $join_date_diff;
                                    }else{
                                        $result[$key]['join_date_diff'] = 0;
                                    }

                                $result[$key]['employee_counter'] = $this->get_total_employee_by_companyID($val['company_id']);
                                
                        }
                   //echo "<pre>";print_r($result);
                   //echo "################## <br><br>";
                    foreach($result as $data){
                        if( ($data['email_verification'] == 0 || $data['employee_counter'] == 0 || $data['login_date_diff'] > 7 )
                             OR
                             ($data['join_date_diff'] == 2 || $data['join_date_diff'] == 7 || $data['join_date_diff'] == 14  || $data['join_date_diff'] == 30 )   
                          ){
                            $record[] = $data;
                          }
                        }
                   //echo "<pre>";print_r($record); die;
                  return $record;  
                }else{
                    return $record;
                }
     }
     
     
    //////////////////////////////////////////////////////////////////////////////////////////////////////                
    
    function get_active_newsletters() {
        
        $sql = "SELECT * FROM tbl_newsletters_sent WHERE schedule_time<".time()." ORDER BY id ASC LIMIT 0,100";
        $query = $this->db->query($sql);

        $record = array();
        if ($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                $record[] = $row;
            }
        }

        return $record;
    }      
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////   
    
    function delete_sent_newsletter($id) {
        
        $this->db->where('id', $id);
        return $this->db->delete('tbl_newsletters_sent');
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////   
    
//    function get_authorized_users_for_digest_email() {
//                       
//        $users = array();
//        
//        $sql = "SELECT tld.id,te.company_id,te.email FROM tbl_login_details AS tld JOIN tbl_employees AS te ON tld.id=te.emp_id WHERE tld.type='3' AND tld.status='1'";
//        $query = $this->db->query($sql);        
//        
//        if ($query->num_rows() > 0) { 
//            foreach($query->result_array() as $row) {
//                
//                $notification_settings = $this->my_custom_functions->get_details_from_id("", "tbl_notification_settings", array("user_id" => $row['id']));
//                
//                if(!empty($notification_settings)) { // If notification settings found, then go with the settings
//                    if($notification_settings['daily_digest_email'] > 0) {
//                        $users[] = $row;
//                    }
//                } else { // If no notification settings found, take it as approved
//                    $users[] = $row;
//                }
//            }
//        }
//        
//        $sql = "SELECT tld.id,tc.company_id,tc.email FROM tbl_login_details AS tld JOIN tbl_companies AS tc ON tld.id=tc.company_id WHERE tld.type='1' AND tld.status='1'";
//        $query = $this->db->query($sql);        
//        
//        if ($query->num_rows() > 0) { 
//            foreach($query->result_array() as $row) {
//                
//                $notification_settings = $this->my_custom_functions->get_details_from_id("", "tbl_notification_settings", array("user_id" => $row['id']));
//                
//                if(!empty($notification_settings)) { // If notification settings found, then go with the settings
//                    if($notification_settings['daily_digest_email'] > 0) {
//                        $users[] = $row;
//                    }
//                } else { // If no notification settings found, take it as approved
//                    $users[] = $row;
//                }
//            }
//        }
//        
//        return $users;
//    }
    
    function get_authorized_users_for_digest_email($email_frequency) {
                       
        $users = array();
        
        // Getting all authorized users
        $sql = "SELECT tld.id FROM tbl_login_details AS tld WHERE tld.type='3' AND tld.status='1'";
        $query = $this->db->query($sql);        
                
        if ($query->num_rows() > 0) { 
            foreach($query->result_array() as $row) {
                
                if(count($users) < 1000) {
                    
                    $already_sent = $this->my_custom_functions->get_perticular_count("tbl_daily_digest_counter", " and login_id='".$row['id']."' and email_frequency='".$email_frequency."' and send_date='".date("Y-m-d")."'");

                    if($already_sent == 0) {

                        $emp_details = $this->my_custom_functions->get_details_from_id("", "tbl_employees", array("emp_id" => $row['id']));
    
                        if(!empty($emp_details)) {
                            $row['company_id'] = $emp_details['company_id'];
                            $row['company_name'] = $this->my_custom_functions->get_particular_field_value("tbl_companies", "company_name", " and company_id='".$row['company_id']."'");
                            $row['email'] = $emp_details['email'];
                            $row['name'] = $emp_details['emp_name'];

                            $notification_settings = $this->my_custom_functions->get_details_from_id("", "tbl_notification_settings", array("user_id" => $row['id']));

                            if(!empty($notification_settings)) { // If notification settings found, then go with the settings
                                if(($notification_settings['daily_digest_email'] <= $email_frequency) AND ($notification_settings['daily_digest_email'] != 0)) {
                                    $users[] = $row;
                                }
                            } 
                        }
                    }
                }
            }
        }
        
        // Getting all companies
        $sql = "SELECT tld.id FROM tbl_login_details AS tld WHERE tld.type='1' AND tld.status='1'";
        $query = $this->db->query($sql);        
        
        if ($query->num_rows() > 0) { 
            foreach($query->result_array() as $row) {
                
                if(count($users) < 1000) {
                
                    $already_sent = $this->my_custom_functions->get_perticular_count("tbl_daily_digest_counter", " and login_id='".$row['id']."' and email_frequency='".$email_frequency."' and send_date='".date("Y-m-d")."'");

                    if($already_sent == 0) {

                        $cmp_details = $this->my_custom_functions->get_details_from_id("", "tbl_companies", array("company_id" => $row['id']));
                        
                        if(!empty($cmp_details)) {
                            $row['company_id'] = $cmp_details['company_id'];
                            $row['company_name'] = $this->my_custom_functions->get_particular_field_value("tbl_companies", "company_name", " and company_id='".$row['company_id']."'");
                            $row['email'] = $cmp_details['email'];
                            $row['name'] = $cmp_details['contact_person'];

                            $notification_settings = $this->my_custom_functions->get_details_from_id("", "tbl_notification_settings", array("user_id" => $row['id']));

                            if(!empty($notification_settings)) { // If notification settings found, then go with the settings
                                if(($notification_settings['daily_digest_email'] <= $email_frequency) AND ($notification_settings['daily_digest_email'] != 0)) {
                                    $users[] = $row;
                                }
                            } 
                        }
                    }
                }
            }
        }
        
        return $users;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////   
                
    function get_regular_activities($company_id, $auth_user_id, $days) {                                
        
        $countActivities = 0;
        $activities = array(
            "checkins" => 0,
            "tasks" => 0,
            //"kms" => 0,
            "expenses" => 0,
        );
        
        $this->load->model("company/Branch_model");
        $emps = $this->Branch_model->get_company_employees("", "ids_only", $company_id, $auth_user_id);
                                   
        $fromTime = strtotime(date("Y-m-d 00:00:00", strtotime('-'.$days.' days')));

        if($emps != "") {

            // Check-ins            
            $sqlc = "SELECT COUNT(checkin_id) AS checkins FROM tbl_checkins WHERE checkin_date > ".$fromTime." AND ref_emp_id IN(".$emps.")";
            $queryc = $this->db->query($sqlc);

            $rowc = $queryc->row_array();
            $activities["checkins"] = ($rowc["checkins"] == "") ? 0 : $rowc["checkins"];
            $countActivities = $activities["checkins"] > 0 ? 1 : $countActivities;

            // Tasks completed
            $sqlt = "SELECT COUNT(work_id) AS tasks FROM tbl_works WHERE UNIX_TIMESTAMP(CONVERT_TZ(status_changed_datetime, '+05:30', @@session.time_zone)) > ".$fromTime." AND ref_emp_id IN(".$emps.") AND work_status=1";
            $queryt = $this->db->query($sqlt);

            $rowt = $queryt->row_array();
            $activities["tasks"] = ($rowt["tasks"] == "") ? 0 : $rowt["tasks"];
            $countActivities = $activities["tasks"] > 0 ? 1 : $countActivities;

            // KMs travelled
//                $sqlk = "SELECT tbl_checkins.* "
//                        . "FROM tbl_checkins JOIN tbl_employees "
//                        . "ON tbl_checkins.ref_emp_id = tbl_employees.emp_id "
//                        . "WHERE "
//                        . "tbl_checkins.checkin_date > ".$fromTime." "
//                        . "AND tbl_employees.company_id='".$company_id."' "
//                        . "ORDER BY tbl_checkins.checkin_date ASC";
//                $queryk = $this->db->query($sqlk);  
//                
//                $distance = 0;
//                $gpsRecord = array();
//
//                if($queryk->num_rows() > 0) {
//                    foreach($queryk->result_array() as $rowk) {
//                        $gpsRecord[$rowk['ref_emp_id']][] = $rowk;
//                    }
//                    
//                    if(count($gpsRecord) > 0) {
//                
//                        foreach($gpsRecord as $emp => $gpss) {
//                    
//                            if(count($gpss) > 0) {                                        
//                                for($gps_count = 0; $gps_count < count($gpss); $gps_count++) {
//
//                                    $from = $gps_count;
//                                    $to = $gps_count + 1;
//
//                                    if(array_key_exists($from, $gpss) AND array_key_exists($to, $gpss)) {
//
//                                       $dis = $this->distance2($gpss[$from]['latitude'], $gpss[$from]['longitude'], $gpss[$to]['latitude'], $gpss[$to]['longitude']);
//
//                                       if(!is_nan($dis)) { 
//                                           $distance += $dis;
//                                       }    
//                                    }
//                                }                                                                       
//                            }
//                        }
//                    }
//                }
//                
//                $distance = ceil($distance);                 
//                $activities["kms"] = $distance;
//                $countActivities = $activities["kms"] > 0 ? 1 : $countActivities;

            // Expenses
            $sqle = "SELECT SUM(amount) AS expenses FROM tbl_expenses WHERE expense_date > ".$fromTime." AND ref_emp_id IN(".$emps.")";
            $querye = $this->db->query($sqle);

            $rowe = $querye->row_array();
            $activities["expenses"] = ($rowe["expenses"] == "") ? 0 : $rowe["expenses"];
            $countActivities = $activities["expenses"] > 0 ? 1 : $countActivities;
        }    
               
        if($countActivities > 0) {
            return $activities;
        } else {
            return array();
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////// 
    
    function insert_daily_digest_counter($data) {
        
        $this->db->insert("tbl_daily_digest_counter", $data);
        $counter_id = $this->db->insert_id();
        
        return $counter_id;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////                
    
    function get_active_digest_emails() {
        
        $sql = "SELECT * FROM tbl_daily_digest_sent ORDER BY id ASC LIMIT 0,100";
        $query = $this->db->query($sql);

        $record = array();
        if ($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                $record[] = $row;
            }
        }

        return $record;
    } 
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////// 
    
    function delete_digest_email_sent($daily_digest_sent_id, $daily_digest_counter_id) {
        
        $this->db->where('id', $daily_digest_sent_id);
        $this->db->delete('tbl_daily_digest_sent');
        
        $this->db->where('id', $daily_digest_counter_id);
        $this->db->delete('tbl_daily_digest_counter');
        
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////// 
    
    function distance2($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371) {
        
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * $earthRadius;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////// 
    
    function get_subscribed_customers_for_renewal_alert() {        
       
        $fromDate = strtotime(date("Y-m-d 00:00:00", strtotime('+7 days')));
        $toDate = strtotime(date("Y-m-d 23:59:59", strtotime('+7 days')));
     
        $sql = "SELECT t_c.company_id,t_c.company_name,t_c.contact_person,t_c.email,t_c_p.expiry_date,t_c_p.no_of_users,t_c_p.package_id FROM "
             . "tbl_company_package AS t_c_p "
             . "JOIN "
             . "tbl_companies AS t_c "
             . "ON t_c_p.company_id = t_c.company_id "
             . "WHERE t_c_p.expiry_date>=".$fromDate." AND t_c_p.expiry_date<=".$toDate." AND t_c_p.expiry_date!=0 AND t_c_p.package_id>1 AND t_c_p.payment_gateway_id=".RAZORPAY_DB_ID." ORDER BY t_c_p.id ASC";
        $query = $this->db->query($sql);

        $record = array();
        if ($query->num_rows() > 0) {                                    
            foreach($query->result_array() as $key => $row) {                
                $package_details = $this->my_custom_functions->get_details_from_id("", "tbl_packages", array("id" => $row['package_id']));
                
                $record[$key] = $row;
                $record[$key]['currency'] = $package_details['currency'];
                $record[$key]['price'] = $package_details['price'];
            }
        }

        return $record;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////// 
    
    function get_subscribed_customers_with_no_payment_records() {
        
        $fromDate = strtotime(date("Y-m-d 00:00:00", strtotime('-1 days')));
        $toDate = strtotime(date("Y-m-d 23:59:59", strtotime('-1 days')));
     
        $sql = "SELECT t_c.company_id,t_c.company_name,t_c.contact_person,t_c.email,t_c_p.expiry_date,t_c_p.id,t_c_p.package_id FROM "
             . "tbl_company_package AS t_c_p "
             . "JOIN "
             . "tbl_companies AS t_c "
             . "ON t_c_p.company_id = t_c.company_id "
             . "WHERE t_c_p.expiry_date>=".$fromDate." AND t_c_p.expiry_date<=".$toDate." AND t_c_p.expiry_date!=0 AND t_c_p.package_id>1 AND t_c_p.payment_gateway_id!=".RAZORPAY_DB_ID." ORDER BY t_c_p.id ASC";
        $query = $this->db->query($sql);

        $record = array();
        if ($query->num_rows() > 0) {                                    
            foreach($query->result_array() as $key => $row) {                                               
                $record[$key] = $row;                
            }
        }

        return $record;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////// 
    
//    function change_expiry_dates() {
//        
//        $sql = "SELECT * FROM tbl_company_package AS t_c_p WHERE t_c_p.expiry_date<".time()." AND t_c_p.expiry_date!=0 AND t_c_p.package_id>1 AND payment_gateway_id!=".RAZORPAY_DB_ID." ORDER BY t_c_p.id ASC";
//        $query = $this->db->query($sql);
//
//        $record = array();
//        if ($query->num_rows() > 0) {                                    
//            foreach($query->result_array() as $key => $row) {                                               
//                $package_array = array(                    
//                    'expiry_date' => strtotime('+10 days')                                         
//                );
//
//                // Update package when set to free package
//                $this->my_custom_functions->update_data($package_array, "tbl_company_package", array("id" => $row['id'])); 
//                
//                echo $row['company_id'].'--'.date("Y-m-d", $row['expiry_date']).'<br>';
//            }
//        }
//
//        return $record;
//    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////// 
    
    function get_subscribed_customers_for_expiry_notification() {        
       
        $payment_frequencies = $this->config->item('payment_frequencies');
        
        // Get companies expiring after 7 days
        $fromDate = strtotime(date("Y-m-d 00:00:00", strtotime('+7 days')));
        $toDate = strtotime(date("Y-m-d 23:59:59", strtotime('+7 days')));
     
        $sql = "SELECT t_c.company_id,t_c.company_name,t_c.contact_person,t_c.designation,t_c.country,t_c.email,t_c.phone,t_c_p.package_id,t_c_p.payment_gateway_id FROM "
             . "tbl_company_package AS t_c_p "
             . "JOIN "
             . "tbl_companies AS t_c "
             . "ON t_c_p.company_id = t_c.company_id "
             . "WHERE t_c_p.expiry_date>=".$fromDate." AND t_c_p.expiry_date<=".$toDate." AND t_c_p.expiry_date!=0 AND t_c_p.package_id>1 ORDER BY t_c_p.id ASC";
        $query = $this->db->query($sql);

        $record1 = array();
        if ($query->num_rows() > 0) {                                    
            foreach($query->result_array() as $key => $row) {                
                $record1[$key]['company_name'] = $row['company_name'];
                $record1[$key]['payment_gate'] = $this->my_custom_functions->get_particular_field_value("tbl_payment_gateways", "payment_gateway_name", " and id='".$row['payment_gateway_id']."'");
                $record1[$key]['username'] = $this->my_custom_functions->get_particular_field_value("tbl_login_details", "username", " and id='".$row['company_id']."' and type=1");
                $record1[$key]['contact_person'] = $row['contact_person'];
                $record1[$key]['designation'] = $row['designation'];
                $record1[$key]['email'] = $row['email'];
                $record1[$key]['phone'] = $row['phone'];
                $record1[$key]['country'] = $this->my_custom_functions->get_particular_field_value("tbl_country", "name", " and country_id='".$row['country']."'");
                                
                $package_details = $this->my_custom_functions->get_details_from_id("", "tbl_packages", array("id" => $row['package_id']));
                $record1[$key]['package_name'] = $package_details['package_name'].'('.$payment_frequencies[$package_details['frequency']].')';
            }
        }
        
        // Get companies expiring tomorrow
        $fromDate = strtotime(date("Y-m-d 00:00:00", strtotime('+1 days')));
        $toDate = strtotime(date("Y-m-d 23:59:59", strtotime('+1 days')));
     
        $sql = "SELECT t_c.company_id,t_c.company_name,t_c.contact_person,t_c.designation,t_c.country,t_c.email,t_c.phone,t_c_p.package_id,t_c_p.payment_gateway_id FROM "
             . "tbl_company_package AS t_c_p "
             . "JOIN "
             . "tbl_companies AS t_c "
             . "ON t_c_p.company_id = t_c.company_id "
             . "WHERE t_c_p.expiry_date>=".$fromDate." AND t_c_p.expiry_date<=".$toDate." AND t_c_p.expiry_date!=0 AND t_c_p.package_id>1 ORDER BY t_c_p.id ASC";
        $query = $this->db->query($sql);

        $record2 = array();
        if ($query->num_rows() > 0) {                                    
            foreach($query->result_array() as $key => $row) {                
                $record2[$key]['company_name'] = $row['company_name'];
                $record2[$key]['payment_gate'] = $this->my_custom_functions->get_particular_field_value("tbl_payment_gateways", "payment_gateway_name", " and id='".$row['payment_gateway_id']."'");
                $record2[$key]['username'] = $this->my_custom_functions->get_particular_field_value("tbl_login_details", "username", " and id='".$row['company_id']."' and type=1");
                $record2[$key]['contact_person'] = $row['contact_person'];
                $record2[$key]['designation'] = $row['designation'];
                $record2[$key]['email'] = $row['email'];
                $record2[$key]['phone'] = $row['phone'];
                $record2[$key]['country'] = $this->my_custom_functions->get_particular_field_value("tbl_country", "name", " and country_id='".$row['country']."'");
                
                $package_details = $this->my_custom_functions->get_details_from_id("", "tbl_packages", array("id" => $row['package_id']));
                $record2[$key]['package_name'] = $package_details['package_name'].'('.$payment_frequencies[$package_details['frequency']].')';
            }
        }

        $record = array("record1" => $record1, "record2" => $record2);
        return $record;
    }
}

?>