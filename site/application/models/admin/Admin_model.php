<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct() {
            parent::__construct();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function login_check() {

        //$password = $this->my_custom_functions->encrypt_string($this->input->post('password'));
        $password = $this->input->post('password');


        $data = array(
          "username" => mysqli_real_escape_string($this->my_custom_functions->get_mysqli(), strip_tags(trim($this->input->post("username")))),
          //"password" => mysqli_real_escape_string($this->my_custom_functions->get_mysqli(), strip_tags(trim($password))),
        );

        $this->db->where($data);
        $this->db->where("status",1); // checking status
        $query = $this->db->get("tbl_admins");

        $record = array();
        if ($query->num_rows() > 0) {

            $record = $query->row_array();

            if(password_verify($password, $record['password'])) {
                $session_data = array(
                    "admin_id" => $record["admin_id"],
                    "admin_username" => $record["username"],
                    "admin_email" => $record["email"],
                    "admin_type" => $record["admin_type"]
                );
                $this->session->set_userdata($session_data);

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function update_admin($admin_id) {

        $update_data = array(
            "name" => $this->input->post("name"),
            "phone" => $this->input->post("phone"),
            "email" => $this->input->post("email")
        );

        $this->db->where('admin_id', $admin_id);
        $return = $this->db->update('tbl_admins', $update_data);

        return $return;
    }

    ////////  -------  TO GET USER DETAIL FOR SENDING MAIL   --------   ////////
    function user_detail_to_send_mail($user_id,$user_type, $table){
        $sql = "SELECT * FROM tbl_login_details login"
                . " INNER JOIN ".$table." rqd_table"
                . " ON login.id = rqd_table.id "
                . "WHERE login.type =". $user_type." AND rqd_table.id=".$user_id;
        //echo $sql;die;
        $query = $this->db->query($sql);
        return $query->row_array();
    }


    /////////---------TO GET ALL RIDERS------------////////////////
     function get_all_riders(){
         $query=$this->db->get('tbl_rider');
         return $query->result_array();

     }


     function update_rider_data($data, $rider_id){

       $this->db->where('id', $rider_id);
       return $this->db->update('tbl_rider', $data);

    }

     function get_riders_id($rider_id){
        $this->db->where('id', $rider_id);
        $query = $this->db->get('tbl_rider');
        $result = $query->row_array();
        return $result;
     }


    /////////---------TO GET ALL DRIVERS------------////////////////

        function total_drivers(){
          $data_set = array();
          $admin_id = $this->session->userdata('admin_id');
          $admin_country_id = $this->my_custom_functions->get_particular_field_value('tbl_admins', 'country_id', 'and admin_id ="'.$admin_id.'"');
         $sql="SELECT * FROM tbl_driver WHERE country_id = ".$admin_country_id."";
         $query = $this->db->query($sql);
         //echo $query;die;
         if ($query->num_rows() > 0) {
           $data_set = $query->result_array();
         }
         return $data_set;

     }

      function update_driver_data($data, $driver_id){

       $this->db->where('id', $driver_id);
       return $this->db->update('tbl_driver', $data);

    }

     function get_drivers_id($driver_id){
        $this->db->where('id', $driver_id);
        $query = $this->db->get('tbl_driver');
        $result = $query->row_array();
        return $result;
     }



    ////////  -------  TO GET THE PRODUCT CATEGORY FOR MANAGE PRODUCT CATEGORY PAGE   --------   //////
    function get_category_data(){
        $sql = "select * from tbl_product_categories order by category_name asc";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    ////////   --------  VALIDATE UNIQUE LOGIN ID   ----------     ////////
    function validate_login_id($login_id){
        $login_count = $this->my_custom_functions->get_perticular_count('tbl_login_details', 'and login_id="'.$login_id.'"');
        return $login_count;
    }


    function update_login_data($data,$manufacturer_id){

        $this->db->where('id', $manufacturer_id);
        $this->db->where('type', 3);
        $this->db->update('tbl_login_details', $data);
        // echo $CI->db->last_query(); die;
        return 1;
    }

    ////////   -------  TO GET MANUFACTURERS LIST FOR MANAGE MANUFACTURER PAGE   -------   ////////
    function get_all_manufacturers() {
      $total_rows = $this->my_custom_functions->get_perticular_count('tbl_manufacturers');
      $offset = 4;
      $this->load->library('pagination');
        $config['base_url'] = site_url() . 'admin/user/manage_manufacturer/';
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }

        $sql = "SELECT * FROM tbl_login_details login "
                . "INNER JOIN tbl_manufacturers manufacturer "
                . "ON login.id = manufacturer.id "
                . "WHERE login.type = '3' ORDER BY manufacturer.create_date DESC"
                . " LIMIT " . $offset . ", " . $config['per_page'] . " ";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    ////////   -------  TO GET MANUFACTURERS DETAILS FOR EDIT MANUFACTURER PAGE   -------   ////////
    function get_manufacturer_data() {

        $sql = "SELECT * FROM tbl_login_details login "
                . "INNER JOIN tbl_manufacturers manufacturer "
                . "ON login.id = manufacturer.id "
                . "WHERE login.type = '3' AND manufacturer.id = '".$this->uri->segment(4)."'";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    ////////   -------  INSERT MANUFACTURER MAIN RECORD   -------   ////////
    function insert_manufacturers_data($data){
        $this->db->insert('tbl_manufacturers', $data);
        return 1;
    }

    ////////   -------  UPDATE MANUFACTURER MAIN RECORD   -------   ////////
    function update_manufacturers_data($data, $manufacturer_id){

        $this->db->where('id', $manufacturer_id);
        $this->db->update('tbl_manufacturers', $data);

        return 1;
    }


    ////////  -------- CLUB MEMBER AREA   --------   ///////
    function get_all_club_member() {

      $total_rows = $this->my_custom_functions->get_perticular_count('tbl_club_members');
      $offset = 4;
      $this->load->library('pagination');
        $config['base_url'] = site_url() . 'admin/club_member/manage_club_member/';
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }

      $sql = "SELECT * FROM tbl_login_details login "
              . "INNER JOIN tbl_club_members club_mem "
              . "ON login.id = club_mem.id "
              . "WHERE login.type = '1' ORDER BY join_date ASC"
              . " LIMIT " . $offset . ", " . $config['per_page'] . " ";

      $query = $this->db->query($sql);
      return $query->result_array();
    }

    function club_mem_search($sql){
      $sql = base64_decode($sql);
      $offset = 4;
      $this->load->library('pagination');
        $config['base_url'] = site_url() . 'admin/club_member/manage_club_member/';
        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        $result_array = $query->result_array();
        return $result_array;
    }

    function view_club_mem($club_mem_id){
      $sql = "SELECT * FROM tbl_login_details login "
              . "INNER JOIN tbl_club_members club_mem "
              . "ON login.id = club_mem.id "
              . "WHERE login.type = '1' AND club_mem.id= '".$club_mem_id."'";
      $query = $this->db->query($sql);
      $club_mem = $query->row_array();

      $bday_sql = ' SELECT * FROM tbl_saved_birthdays WHERE user_id = "'.$club_mem_id.'" ';
      $bday_sql_query = $this->db->query($bday_sql);
      $club_mem['bdays'] = $bday_sql_query->result_array();


      return $club_mem;
    }

    ////////  -------- GROUP ADMIN AREA   --------   ///////
    function get_all_group_admin() {

      $total_rows = $this->my_custom_functions->get_perticular_count('tbl_group_admins');
      $offset = 4;
      $this->load->library('pagination');
        $config['base_url'] = site_url() . 'admin/group_admin/manage_group_admin/';
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
      $sql = "SELECT * FROM tbl_login_details login "
              . "INNER JOIN tbl_group_admins grp_admin "
              . "ON login.id = grp_admin.id "
              . "WHERE login.type = '2' ORDER BY join_date ASC"
              . " LIMIT " . $offset . ", " . $config['per_page'] . " ";

      $query = $this->db->query($sql);
      return $query->result_array();
    }

    function group_admin_search($sql){
      $sql = base64_decode($sql);
      $offset = 4;
      $this->load->library('pagination');
        $config['base_url'] = site_url() . 'admin/group_admin/manage_group_admin/';
        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        $result_array = $query->result_array();
        return $result_array;
    }

    function view_grp_admin($grp_admin_id){
      $sql = "SELECT * FROM tbl_login_details login "
              . "INNER JOIN tbl_group_admins grp_admin "
              . "ON login.id = grp_admin.id "
              . "WHERE login.type = '2' AND grp_admin.id = '".$grp_admin_id."'";

      $query = $this->db->query($sql);
      $grp_admin = $query->row_array();

      $bday_sql = ' SELECT * FROM tbl_saved_birthdays WHERE user_id = "'.$club_mem_id.'" ';
      $bday_sql_query = $this->db->query($bday_sql);
      $grp_admin['bdays'] = $bday_sql_query->result_array();

      return $grp_admin;
    }

    /////////////////////////////////////////////////////////////
    //////////  GET QUESTIONS FOR CATEGORIES  ////////
    ////////////////////////////////////////////////////////////
    function get_questions($categ_id){
      $ques_sql = ' SELECT * FROM tbl_questionnaire_questions WHERE category_id = "'.$categ_id.'" ';
      $query = $this->db->query($ques_sql);
      return $query->result_array();
    }

    /////////  GET OPTION TEXT   ///////
    function get_options_text($option_id){
      $option_sql = ' SELECT * FROM tbl_questionnaire_options WHERE id = "'.$option_id.'" ';
      $query = $this->db->query($option_sql);
      return $query->row_array();
    }

    ///////  GET ANSWERED OPTION   ///////
    function get_answered_options($ques_id, $user_id){
      $answered_option = ' SELECT * FROM tbl_questionnaire_answers WHERE user_id = "'.$user_id.'" AND question_id = "'.$ques_id.'" ';
      $query = $this->db->query($answered_option);
      return $query->result_array();
    }

    //////////////////////////////////////
    function sub_option($option_id){
      $sql = $this->my_custom_functions->get_particular_field_value('tbl_questionnaire_options', 'option_text', 'and id ="'.$option_id.'"');
      $query = $this->db->query($sql);
      return $query->result_array();
    }

    ////////////////////////////////////////////////////
    ////////////////----------------///////////////////
    //////////////////////////////////////////////////

    ///////////  ------   MANAGE ORDERS   ------   ///////////
    function get_all_customers(){
      $sql = 'SELECT *FROM tbl_club_members WHERE 1=1';
      $query = $this->db->query($sql);

      $record[""] = "-- Customer Name --";
      foreach ($query->result_array() as $row) {
          $record[$row['id']] = ucwords($row['name']);
      }
      return $record;
    }

    function get_all_orders() {

        $total_rows = $this->my_custom_functions->get_perticular_count('tbl_order_main');
        $offset = 4;
        $this->load->library('pagination');
        $config['base_url'] = site_url() . 'admin/orders/manage_orders/';
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }

        $order_sql = ' SELECT * FROM tbl_order_main ORDER BY order_date DESC  LIMIT '. $offset .' ,  '. $config['per_page'] .' ';
        $query = $this->db->query($order_sql);
        $order_detail = $query->result_array();
        return $this->customer_address($order_detail);

    }

    function customer_address($order_detail){
      foreach($order_detail as $key => $value){

        $address_sql = ' SELECT * FROM tbl_customer_shipping WHERE id = "'.$value['shipping_address'].'" ';
        $address_query = $this->db->query($address_sql)->row_array();

        $country_name = $this->my_custom_functions->get_particular_field_value('tbl_country', 'name', 'AND country_id="'.$address_query['country'].'"');

        $shipping_add = $address_query['contact_person'] . '<br>'
                        . $address_query['address'] . '<br>'
                        . $address_query['city'] . '<br>'
                        . $country_name . '<br>'
                        . $address_query['zip_code'] . '<br>'
                        . $country_name . '<br>'
                        . $address_query['email'] . '<br>'
                        . $address_query['mobile'];
        $order_detail[$key]['shipping_address'] = $shipping_add;
        // $order_detail[$key]['email'] = $address_query['email'];
        // $order_detail[$key]['mobile'] = $address_query['mobile'];

      }
      return $order_detail;
    }

    function view_order_detail($order_id){
      $order_sql = ' SELECT * FROM tbl_order_details WHERE main_order_id = "'.$order_id.'" ';
      // $order_sql = ' SELECT * FROM tbl_order_main order_main
      //                JOIN tbl_order_details order_det
      //                ON order_main.id = order_det.main_order_id
      //                JOIN tbl_customer_shipping shipping
      //                ON order_main.shipping_address = shipping.id
      //                WHERE order_det.main_order_id = "'.$order_id.'"';

      $order_query = $this->db->query($order_sql);
      $order_detail  = $order_query->result_array();
      //echo "<pre>";print_r($order_detail);
      foreach($order_detail as $key => $value){
        //echo "<pre>";print_r($value);
      $customer_name = $this->my_custom_functions->get_particular_field_value('tbl_club_members', 'name', 'AND id="'.$value['customer_id'].'"');
      $order_detail[$key]['customer_name'] = $customer_name;
      $manufacturer_name = $this->my_custom_functions->get_particular_field_value('tbl_manufacturers', 'name', 'AND id="'.$value['manufacturer_id'].'"');
      $order_detail[$key]['manufacturer_name'] = $manufacturer_name;
      $product_name = $this->my_custom_functions->get_particular_field_value('tbl_products', 'name', 'AND id="'.$value['product_id'].'"');
      $order_detail[$key]['product_name'] = $product_name;

      }

      return $order_detail;
    }

    function order_main_det($order_id){
      $order_main_sql = ' SELECT * FROM tbl_order_main WHERE id = "'.$order_id.'"  ';
      $order_main_query = $this->db->query($order_main_sql);
      $order_main_detail  = $order_main_query->row_array();
        ///////////  get shipping address   ///////
        $address_sql = ' SELECT * FROM tbl_customer_shipping WHERE id = "'.$order_main_detail['shipping_address'].'" ';
        $address_query = $this->db->query($address_sql)->row_array();

        // $country_name = $this->my_custom_functions->get_particular_field_value('tbl_country', 'name', 'AND country_id="'.$address_query['country'].'"');
        // $shipping_add = $address_query['contact_person'] . '<br>'
        //                 . $address_query['address'] . '<br>'
        //                 . $address_query['city'] . '<br>'
        //                 . $country_name . '<br>'
        //                 . $address_query['zip_code'] . '<br>'
        //                 . $country_name . '<br>'
        //                 . 'Email: '.$address_query['email'] . '<br>'
        //                 . 'Phone: '.  $address_query['mobile'];
        $order_main_detail['shipping_address'] = $address_query;
      return $order_main_detail;
    }

    function order_search($sql){
      $sql = base64_decode($sql);
      $offset = 4;
      $this->load->library('pagination');
        $config['base_url'] = site_url() . 'admin/orders/manage_orders/';
        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        $order_detail = $query->result_array();
        return $this->customer_address($order_detail);
    }


    function ajax_get_member_name($member_name) {

          $rs= array();
          $sql = 'select distinct (orders.customer_id), customer.* from tbl_club_members customer
                  join tbl_order_main orders
                  on customer.id = orders.customer_id
                  where customer.name like "%'.$member_name.'%"
                  group by orders.customer_id';
          //echo $sql;die;
          $record = $this->db->query($sql)->result_array();

          if(!empty($record)){
              foreach($record as $row){
                   $rs[$row['id']]['label'] = $row['name'];
                   $rs[$row['id']]['id'] = $row['customer_id'];

                   //$rs[$row['id']] = $row['name'];
              }
          }
          return $rs;
     }

     public function get_all_admin()
     {
       $this->db->where('admin_type',1);
       $query = $this->db->get('tbl_admins');
       return $query->result_array();
     }

     function get_all_currency(){
         $query=$this->db->get('tbl_currency');
         return $query->result_array();
     }

     function get_currency_by_id($id){
          $this->db->where('id', $id);
         $query=$this->db->get('tbl_currency');
         return $query->row_array();
     }
     function get_admin_by_id($id){
          $this->db->where('admin_id', $id);
         $query=$this->db->get('tbl_admins');
         return $query->row_array();
     }



     function update_currency_data($data, $id){
       $this->db->where('id', $id);
       return $this->db->update('tbl_currency', $data);

    }

    function update_admin_data($data, $admin_id){
      $this->db->where('admin_id', $admin_id);
      return $this->db->update('tbl_admins', $data);

   }









    ///////////////////////////////////////////////////////////////////////////

}

?>
