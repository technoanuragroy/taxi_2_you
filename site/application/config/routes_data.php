<?php
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route["admin"] = "admin/main";
$route["admin/logout"] = "admin/main/logout";

$route["manufacturer"] = "manufacturer/main";
$route["manufacturer/logout"] = "manufacturer/main/logout";

$route["club_member"] = "club_member/main";
$route["driver/main/(:any)/(:any)/(:any)"] = "driver/main/index/$1/$2/$3";
$route["rider/main/(:any)/(:any)/(:any)"] = "driver/main/index/$1/$2/$3";
//$route["driver/main/(:any)/(:any)"] = "driver/main/$1/$2";
?>
