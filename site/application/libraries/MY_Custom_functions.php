<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Custom_functions {

    function __construct() {

        //parent::__construct();

    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    public function get_particular_field_value($tablename, $fieldname, $where = "") {

        $CI = & get_instance();
        $str = "select " . $fieldname . " from " . $tablename . " where 1=1 " . $where;

        $query = $CI->db->query($str);
        $record = "";
        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $field_arr = explode(" as ", $fieldname);
                if (count($field_arr) > 1) {
                    $fieldname = $field_arr[1];
                } else {
                    $fieldname = $field_arr[0];
                }
                $record = $row[$fieldname];
            }
        }
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function get_details_from_id($id = "", $table, $extra = array()) {

        $CI = & get_instance();
        if ($id != "") {
            $CI->db->where("id", $id);
        }
        if (count($extra) > 0) {
            $CI->db->where($extra);
        }
        $query = $CI->db->get($table);

        $record = array();
        if ($query->num_rows() > 0) {
            $record = $query->row_array();
        }
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function get_multiple_data($table = "", $where = "") {

        $CI = & get_instance();
        $str = "select * from " . $table . " where 1=1 " . $where;

        $query = $CI->db->query($str);

        $record = array();
        $count = 0;
        foreach ($query->result_array() as $row) {
            $record[$count] = $row;
            $count++;
        }
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    public function get_perticular_count($tablename, $where = "") {

        $CI = & get_instance();
        $str = "select * from " . $tablename . " where 1=1 " . $where;

        $query = $CI->db->query($str);
        $record = $query->num_rows();
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function insert_data($data, $table) {

        $CI = & get_instance();
        $CI->db->insert($table, $data);
        return 1;
    }

    function insert_data_last_id($data, $table) {

        $CI = & get_instance();
        $CI->db->insert($table, $data);
        return $CI->db->insert_id();
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function delete_data($table, $where = array()) {

        $CI = & get_instance();
        $CI->db->where($where);
        $CI->db->delete($table);

        return 1;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function update_data($data, $table, $where) {

        $CI = & get_instance();
        $CI->db->where($where);
        $CI->db->update($table, $data);
        // echo $CI->db->last_query(); die;
        return 1;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function propercase($string) {

        $string = ucwords(strtolower($string));

        foreach (array('-', '\'', '(', ',', ', ') as $delimiter) {
            if (strpos($string, $delimiter) !== false) {
                $string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
            }
        }
        return $string;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function aasort(&$array, $key) {

        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
        return $array;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function encrypt_string($string) {

        return openssl_digest($string, 'sha512');
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    public function check_admin_security() {

        $CI = & get_instance();

        if (!$CI->session->userdata('admin_id')) {
            $CI->session->set_userdata("REDIRECT_PAGE_ADMIN", current_url());
            redirect('admin');
        }
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    public function check_rider_security() {

        $CI = & get_instance();

        if (!$CI->session->userdata('rider_id')) {
            redirect('rider/main/rider_login');
        }
    }

    public function check_driver_security() {

        $CI = & get_instance();

        if (!$CI->session->userdata('driver_id')) {
            redirect('driver/main/login');
        }
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */


    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function CreateEmail($addressing_user, $message, $thankyou_text, $unsubscribe) {

        $CI = & get_instance();

        $CI->load->helper('file');

        $mail_header = read_file("application/views/mail_template/mail_template_header.php");
        $mail_body = read_file("application/views/mail_template/mail_template_body.php");
        $mail_footer = read_file("application/views/mail_template/mail_template_footer.php");
        $mail_unsubscribe = read_file("application/views/mail_template/mail_template_unsubscribe.php");

        return $mail_header . sprintf($mail_body, $addressing_user, $message, $thankyou_text) . $mail_footer . sprintf($mail_unsubscribe, $unsubscribe);
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function SendEmail($from, $to, $subject, $email_message, $attachment = "", $alternative_text = "") {

        $CI = & get_instance();

        $from_array = explode(',', $from);
        $from_email = $from_array[0];
        $from_name = $from_array[1];

        $CI->load->library("email");

        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';

        $CI->email->initialize($config);

        $CI->email->from($from_email, $from_name);
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($email_message);

        if ($attachment != '') {
            $CI->email->attach($attachment);
        }

        if ($alternative_text != '') {
            $CI->email->set_alt_message($alternative_text);
        }

        //$CI->email->set_header('List-Unsubscribe', '<https://web.twib.online>');

        if($CI->email->send()) {
            return true;
        } else {
            return false;
        }
    }

    function get_mysqli() {

        $db = (array) get_instance()->db;
        return mysqli_connect('localhost', $db['username'], $db['password'], $db['database']);
    }

    function clean($string) {

        $string = preg_replace('/[^A-Za-z0-9\- ]/', '', $string); // Removes special chars.
        $string = preg_replace('/\s+/', '-', $string); // Replaces all whitespaces with hyphens.

        return rtrim($string, '-');
    }

    function CreateFixedSizedImage($source, $dest, $width, $height) {

        $source_path = $source;
        //echo $source_path;die;

        list( $source_width, $source_height, $source_type ) = getimagesize($source_path);

        switch ($source_type) {
            case IMAGETYPE_GIF:
                $source_gdim = imagecreatefromgif($source_path);
                break;

            case IMAGETYPE_JPEG:
                $source_gdim = imagecreatefromjpeg($source_path);
                break;

            case IMAGETYPE_PNG:
                $source_gdim = imagecreatefrompng($source_path);
                break;
        }



        $source_aspect_ratio = $source_width / $source_height;
        $desired_aspect_ratio = $width / $height;

        if ($source_aspect_ratio > $desired_aspect_ratio) {
            //
            // Triggered when source image is wider
            //
            $temp_height = $height;
            $temp_width = (int) ( $height * $source_aspect_ratio );
        } else {
            //
            // Triggered otherwise (i.e. source image is similar or taller)
            //
            $temp_width = $width;
            $temp_height = (int) ( $width / $source_aspect_ratio );
        }

        //
        // Resize the image into a temporary GD image
        //

        $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
        imagecopyresampled(
                $temp_gdim, $source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height
        );

        //
        // Copy cropped region from temporary image into the desired GD image
        //

        $x0 = ( $temp_width - $width ) / 2;
        $y0 = ( $temp_height - $height ) / 2;
        //$y0 = 0; //if the cropping needs to be done form top

        $desired_gdim = imagecreatetruecolor($width, $height);
        imagecopy(
                $desired_gdim, $temp_gdim, 0, 0, $x0, $y0, $width, $height
        );

        //
        // Render the image
        // Alternatively, you can save the image in file-system or database
        //

        //header( 'Content-type: image/jpeg' );
        imagejpeg($desired_gdim, $dest);






        //
        // Add clean-up code here
    }

    function resize_image($file, $w, $h, $crop = FALSE, $dest) {

        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width - ($width * abs($r - $w / $h)));
            } else {
                $height = ceil($height - ($height * abs($r - $w / $h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w / $h > $r) {
                $newwidth = $h * $r;
                $newheight = $h;
            } else {
                $newheight = $w / $r;
                $newwidth = $w;
            }
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        imagejpeg($src, $dest);
        return $dst;
    }

    function RotateJpg($filename = '', $angle = 0, $savename = false) {
        // Your original file
        $original = imagecreatefromjpeg($filename);
        // Rotate
        $rotated = imagerotate($original, $angle, 0);
        // If you have no destination, save to browser
        if ($savename == false) {
            header('Content-Type: image/jpeg');
            imagejpeg($rotated);
        } else
        // Save to a directory with a new filename
            imagejpeg($rotated, $savename);

        // Standard destroy command
        imagedestroy($rotated);
    }

    ///////////////////////////////////////////////////////////////////////////////////
    /// ORDER PROCEDURE FUNCTION STRATS HERE
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    /// Encrypt function
    ///////////////////////////////////////////////////////////////////////////////////
    function ablEncrypt($string) {
        $output = false;
        $key = hash("sha256", SECRET_KEY);
        $iv = substr(hash("sha256", SECRET_IV), 0, 16);

        $output = openssl_encrypt($string, ENCRYPT_METHOD, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    /// Decrypt function
    ///////////////////////////////////////////////////////////////////////////////////
    function ablDecrypt($string) {

        $output = false;
        $key = hash("sha256", SECRET_KEY);
        $iv = substr(hash("sha256", SECRET_IV), 0, 16);

        $output = base64_decode($string);
        $output = openssl_decrypt($output, ENCRYPT_METHOD, $key, 0, $iv);

        return $output;
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Get random code for discount coupon
    ///////////////////////////////////////////////////////////////////////////////
    function get_random_discount_code($chars_min = 16, $chars_max = 16, $use_upper_case = true, $include_numbers = true) {

        $length = rand($chars_min, $chars_max);
        $selection = 'AEUOYIBCDFGHJKLMNPQRSTVWXZ';
        if ($include_numbers) {
            $selection .= "1234567890";
        }

        $discount_code = "";
        for ($i = 0; $i < $length; $i++) {
            $current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
            $discount_code .= $current_letter;
        }

        $CI = & get_instance();
        $CI->load->model("admin/payment_model");

        $return = $CI->payment_model->validate_discount_code($discount_code);

        if ($return) {
            $discount_code = $CI->my_custom_functions->get_random_discount_code();
        }

        return $discount_code;
    }

    // Time format is UNIX timestamp or
    // PHP strtotime compatible strings
    function dateDiff($time1, $time2, $precision = 6) {
        // If not numeric then convert texts to unix timestamps
        if (!is_int($time1)) {
            $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
            $time2 = strtotime($time2);
        }

        // If time1 is bigger than time2
        // Then swap time1 and time2
        if ($time1 > $time2) {
            $ttime = $time1;
            $time1 = $time2;
            $time2 = $ttime;
        }

        // Set up intervals and diffs arrays
        $intervals = array('year', 'month', 'day', 'hour', 'minute', 'second');
        $diffs = array();

        // Loop thru all intervals
        foreach ($intervals as $interval) {
            // Create temp time from time1 and interval
            $ttime = strtotime('+1 ' . $interval, $time1);
            // Set initial values
            $add = 1;
            $looped = 0;
            // Loop until temp time is smaller than time2
            while ($time2 >= $ttime) {
                // Create new temp time from time1 and interval
                $add++;
                $ttime = strtotime("+" . $add . " " . $interval, $time1);
                $looped++;
            }

            $time1 = strtotime("+" . $looped . " " . $interval, $time1);
            $diffs[$interval] = $looped;
        }

        $count = 0;
        $times = array();
        // Loop thru all diffs
        foreach ($diffs as $interval => $value) {
            // Break if we have needed precission
            if ($count >= $precision) {
                break;
            }
            // Add value and interval
            // if value is bigger than 0
            if ($value > 0) {
                // Add s if value is not 1
                if ($value != 1) {
                    $interval .= "s";
                }
                // Add value and interval to times array
                $times[] = $value . " " . $interval;
                $count++;
            }
        }

        // Return string with times
        return implode(", ", $times);
    }

    /////////   ----------------   GENERATE THE RANDOM LOGIN-ID    ------------------   //////////

    function get_random_login_id($chars_min = 10, $chars_max = 10) {

        $CI = & get_instance();
        $login_id = mt_rand(1000000000,9999999999);

        $login_count = $CI->my_custom_functions->get_perticular_count('tbl_login_details', 'and login_id="'.$login_id.'"');

        if ($login_count > 0) {
            $login_id = $CI->my_custom_functions->get_random_login_id();
        } else {
            return $login_id;
        }
    }

    /////////   ----------------   GENERATE THE RANDOM VOUCHER CODE    ------------------   //////////

    function get_random_voucher_code($chars_min = 10, $chars_max = 10, $use_upper_case = true, $include_numbers = true) {

        $length = rand($chars_min, $chars_max);
        $selection = 'AEUOYIBCDFGHJKLMNPQRSTVWXZ';
        if ($include_numbers) {
            $selection .= "1234567890";
        }

        $discount_code = "";
        for ($i = 0; $i < $length; $i++) {
            $current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
            $discount_code .= $current_letter;
        }

        $CI = & get_instance();

        $return = $CI->my_custom_functions->get_perticular_count('tbl_vouchers', " AND coupon LIKE binary '".trim($discount_code)."'");

        if ($return > 0) {
            $discount_code = $CI->my_custom_functions->get_random_discount_code();
        }

        return $discount_code;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    function get_country_dropdown_data() {

        $CI = & get_instance();

        $str = "SELECT * FROM tbl_country WHERE 1=1";
        $query = $CI->db->query($str);

        $record[""] = "-- Select Country --";
        foreach ($query->result_array() as $row) {
            $record[$row['country_id']] = $row['name'];
        }
        return $record;
    }

    function get_country_list() {

        $CI = & get_instance();

        $str = "SELECT * FROM tbl_country WHERE 1=1";
        $query = $CI->db->query($str);
        return $query->result_array();
    }
    function get_country_dropdown() {

        $CI = & get_instance();

        $str = "SELECT * FROM tbl_country WHERE 1=1";
        $query = $CI->db->query($str);

        return $query->result_array();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    function get_product_dropdown_data($manufacturerId = "") {

        $CI = & get_instance();

        if($manufacturerId != "") {
            $str = "SELECT * FROM tbl_products WHERE manufacturer_id='".$manufacturerId."'";
        } else {
            $str = "SELECT * FROM tbl_products";
        }
        $query = $CI->db->query($str);

        $record[""] = "-- Select Product --";
        foreach ($query->result_array() as $row) {
            $record[$row['id']] = $row['name'];
        }
        return $record;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    function get_active_product_categories_dropdown_data() {

        $CI = & get_instance();

        $str = "SELECT * FROM tbl_product_categories WHERE status=1 ORDER BY category_name ASC";
        $query = $CI->db->query($str);

        $record[""] = "-- Select Product Category --";
        foreach ($query->result_array() as $row) {
            $record[$row['id']] = $row['category_name'];
        }
        return $record;
    }

    ////////   -------  TO INSERT/UPDATE INTO LOGIN DETAILS TABLE  ------   ////////


    ////////////////////////////////////////////////////////////////////////////////////////////

    function clean_alise($input_string) {

        $input_string = str_replace("_", "", $input_string);
        $input_string = str_replace(" ", "-", $input_string);
        $input_string = str_replace("'", "", $input_string);
        $input_string = str_replace("&#39;", "", $input_string);
        $input_string = str_replace(",", "-", $input_string);
        $input_string = str_replace("*", "", $input_string);
        $input_string = str_replace("&quot;", "", $input_string);
        $input_string = str_replace("&amp;", "&", $input_string);
        $input_string = str_replace("&#039;", "", $input_string);

        return $input_string;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    function get_total_cart_items() {

        $CI = & get_instance();

        $sql = "SELECT COUNT(id) AS total_cart_items FROM tbl_cart WHERE customer_id='".$CI->session->userdata("customer_id")."'";
        $query = $CI->db->query($sql);
        $result = $query->row_array();

        $total_items = ($result['total_cart_items'] != NULL) ? $result['total_cart_items'] : 0;

        return $total_items;
    }


    function check_security_key(){
       $CI = & get_instance();
       //$last = $CI->uri->total_segments();
       //$record_num = $CI->uri->segment($last);

        $security_key=$CI->session->userdata('security_key');

       if($security_key==SECURITY_KEY){

            return true;

       }else{

           die();

       }

    }


    function get_driver_network_status($driver_id)
    {
      $CI = & get_instance();

      $status = "SELECT * FROM tbl_driver WHERE id= $driver_id";
      $query = $CI->db->query($status);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////


    //////////    -------------  #END #END #END #END   ------------   /////////////
}

?>
