<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->check_rider_security();
        $this->load->model("rider/Rider_model");
        $last = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);
        if($record_num != TAXI_APP_DRIVER_SECURITY_KEY) {
            die();
        }
    }

    /////////////// ------------  DASHBOARD  -----------//////////////////
    public function rider_dashboard() {

        $data['details'] = $this->my_custom_functions->get_details_from_id("", "tbl_rider", array("id" => $this->session->userdata("rider_id")));
        $data['place_details'] = $this->my_custom_functions->get_multiple_data($table = "tbl_rider_save_place", $where = "");
        //$data['orders'] = $this->Rider_model->order_summary($this->session->userdata("rider_id"));
        //echo "<pre>";print_r($data['place_details']);die;
        $this->load->view('rider_new/rider_dashboard',$data);
    }

    /////////////// ------------  DASHBOARD  REQUEST -----------//////////////////
    public function rider_dashboard_request() {

        $data['details'] = $this->my_custom_functions->get_details_from_id("", "tbl_rider", array("id" => $this->session->userdata("rider_id")));
        $data['place_details'] = $this->my_custom_functions->get_multiple_data($table = "tbl_rider_save_place", $where = "");
        $this->load->view('rider_new/rider_dashboard_request',$data);
    }
    /////////////// ------------  TRIP CONFRIM -----------//////////////////
    public function trip_confirm() {
        $travel_mode = $this->input->post('travel_mode');
        $wallet_balance = $this->input->post('final_wallet_balance');
        //echo $wallet_balance;die;
        $trip_data = array(
            'distance'          =>  $this->input->post('distance'),
            'amount'        =>  $this->input->post('amount'),
            'travel_mode'        =>  $this->input->post('travel_mode')

        );
        //echo $wallet_balance;die;
        $data['search_driver']=$this->Rider_model->search_driver($travel_mode);
        //echo "<pre>";print_r($data['search_driver']);die;
        $insert = $this->Rider_model->send_driver_request_save($data['search_driver'],$trip_data);
        if ($insert && $wallet_balance!='') {
          $wallet_data = array(
              'wallet_balance'=> $wallet_balance,
          );
          $where = array(
              'rider_id'=> $this->session->userdata("rider_id"),
          );
          $update = $this->my_custom_functions->update_data($wallet_data,'tbl_wallet',$where);
        }
        $data['details'] = $this->my_custom_functions->get_details_from_id("", "tbl_rider", array("id" => $this->session->userdata("rider_id")));
        //$this->load->view('rider_new/trip_confirm',$data);
        $this->session->set_userdata("act_rides_id", $insert);
        echo $insert;
    }
    public function trip_confirmation() {

        $this->load->view('rider_new/trip_confirm');
    }

        /////////////// ------------  Save Places  -----------//////////////////
        public function save_places() {

            $data['details'] = $this->my_custom_functions->get_details_from_id("", "tbl_rider", array("id" => $this->session->userdata("rider_id")));
            //$data['orders'] = $this->Rider_model->order_summary($this->session->userdata("rider_id"));
            //echo "<pre>";print_r($data['orders']);die;

            $this->load->view('rider/save_places',$data);

    }
      /////////////// ------------  Save Places  -----------//////////////////
    public function place_location_session_store() {

        $frm_latitude = $this->input->post('frm_latitude');
        $frm_longitude = $this->input->post('frm_longitude');
        $to_latitude = $this->input->post('to_latitude');
        $to_longitude = $this->input->post('to_longitude');
        $ses_origin = $this->input->post('ses_origin');
        $ses_destination = $this->input->post('ses_destination');
        //echo $frm_latitude;die;
        $this->session->set_userdata("frm_latitude", $frm_latitude);
        $this->session->set_userdata("frm_longitude", $frm_longitude);
        $this->session->set_userdata("to_latitude", $to_latitude);
        $this->session->set_userdata("to_longitude", $to_longitude);
        $this->session->set_userdata("ses_origin", $ses_origin);
        $this->session->set_userdata("ses_destination", $ses_destination);

    }
    public function save_places_register() {

            $data['place_details'] = $this->my_custom_functions->get_multiple_data($table = "tbl_rider_save_place", $where = "");
            $this->form_validation->set_rules('place_title','Title','required');
            // $this->form_validation->set_rules('place_address','Address','required');
            // $this->form_validation->set_rules('latitude','Latitude','required');
            // $this->form_validation->set_rules('longitude','Longitude','required');

            if ($this->form_validation->run()==FALSE)
                    {
                        echo "fail1";


                        }
                    else
                    {
                        if (count($data['place_details'])==4) {
                          echo "fail3";
                        }else{
                        $sql=$this->Rider_model->save_place();
                        if ($sql) {
                            echo "success";
                        }else{
                            echo "fail2";
                        }
                        }
                    }




    }


    /////////////// ------------  EDIT PROFILE  -----------   //////////////////
    public function edit_profile() {

        if($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r ($_POST);die;

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');

            if ($this->form_validation->run() == false) { // Form validation failed

                $this->session->set_flashdata('validation_message', validation_errors('<p class="e_message">'));
                redirect('rider/user/edit_profile/'.TAXI_APP_DRIVER_SECURITY_KEY);

            } else {

                $this->Rider_model->update_rider_data();

                ///////////    uploading photo ID of club member   //////////

                if(!empty($_FILES)) {

                    $this->load->library('image_lib');
                    $config = array(
                        'image_library'=>'gd2',
                        'allowed_types'=>'gif|jpg|png|jpeg',
                        'overwrite'=>true,
                        'maintain_ratio'=>true,
                        'source_image'=>$_FILES['formal_id']['tmp_name'],
                        'new_image'=>"uploads/rider/". $this->session->userdata('rider_id') .".jpg",
                    );

                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                }

                $this->session->set_flashdata('s_message', 'You Have Successfully Updated Your Profile');
                redirect('rider/user/edit_profile/'.TAXI_APP_DRIVER_SECURITY_KEY);

            }
        } else {

            $data['title'] = 'Edit Profile';
            $data['details'] = $this->my_custom_functions->get_details_from_id("", "tbl_rider", array("id" => $this->session->userdata("rider_id")));
            $this->load->view('rider/edit_profile', $data);
        }
    }

    /////////////// ------------  CHANGE PASSWORD   -----------   //////////////////
    public function change_password() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->load->library("form_validation");

            /// tbl_companies contents
            $this->form_validation->set_rules("o_password", "Old Password", "trim|required");
            $this->form_validation->set_rules("n_password", "New Password", "trim|required|min_length[6]");
            $this->form_validation->set_rules("c_password", "Confirm Password", "trim|required|min_length[6]|matches[n_password]");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("rider/user/change_password/".TAXI_APP_DRIVER_SECURITY_KEY);

            } else {

                $o_password = $this->input->post('o_password');
                $password = $this->my_custom_functions->get_particular_field_value("tbl_rider", "password", " and id='".$this->session->userdata('rider_id')."'");

                if(password_verify($o_password, $password)) {

                    if($o_password != $this->input->post("n_password")) {

                        $password = password_hash($this->input->post("n_password"), PASSWORD_DEFAULT);
                        $data = array(
                            "password" => $password
                        );

                        $table = "tbl_rider";

                        $where = array(
                            "id" => $this->session->userdata('rider_id')
                        );
                        $password_updated = $this->my_custom_functions->update_data($data, $table, $where);

                        $this->session->set_flashdata("s_message", "Password Has Been Updated Successfully");
                        redirect("rider/user/change_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                    } else {
                        $this->session->set_flashdata("e_message", "Unable To Update The Password.New And Old Password Can Not Be Same. Use Different Password To Update");
                        redirect("rider/user/change_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                    }

                } else {
                    $this->session->set_flashdata("e_message", "Old Password Is Incorrect");
                    redirect("rider/user/change_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                }
            }
        } else {

            $data['title'] = "Change Password";
            $this->load->view("rider/change_password", $data);
        }
    }
    /////////////// ------------  Save Places  -----------//////////////////
    public function past_trip_records() {

        $data['details'] = $this->Rider_model->past_trip_records();;
        //$where ='".rider_id => $this->session->userdata('rider_id')."'
        //echo "<pre>";print_r($data['details']);die;
        $this->load->view('rider_new/past_trip_records',$data);

    }

    public function pending_request() {

        $data['details'] = $this->Rider_model->get_pending_request();
        $this->load->view('rider_new/pending_request',$data);

    }

    public function getAcceptDriverdetails()
     {
       //echo 'here';die;
       $rides_id = $this->input->post('rides_id');
       //echo $rides_id;die;
       $query = $this->Rider_model->getAcceptDriverdetails($rides_id);
       echo $query;
     }

     public function CancelRidesRider()
     {
         //echo 'here';die;
       $rides_id = $this->input->post('rides_id');
       //echo $rides_id;die;
       $query = $this->Rider_model->CancelRidesRider($rides_id);
       echo $query;
     }
     public function payment_status() {

         //$data['details'] = $this->Rider_model->get_pending_request();
         $this->load->view('rider_new/payment_status');

     }
     public function payment_confirm() {

       $data = array(
           "payment_status" => 1,
           "payment_mode" => 1,
       );

       $table = "tbl_rides";

       $where = array(
           "rides_id" => $this->uri->segment(4)
       );
       $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
       $data['details'] = $this->my_custom_functions->get_details_from_id("", "tbl_rides", array("rides_id" => $this->uri->segment(4)));
       $this->session->unset_userdata("act_rides_id");
       //echo "<pre>";print_r($data);die;
         //$data['details'] = $this->Rider_model->get_pending_request();
         $this->load->view('rider_new/payment_confirm',$data);

     }
     public function rides_rating() {

         if ($this->input->post("submit") && $this->input->post("submit") != "") {
           $data = array(
               "rating" => $this->input->post("rating")
           );

           $table = "tbl_rides";

           $where = array(
               "rides_id" => $this->uri->segment(4)
           );
           $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
           $data['details'] = $this->my_custom_functions->get_details_from_id("", "tbl_rides", array("rides_id" => $this->uri->segment(4)));
           $this->load->view('rider_new/driver_rating',$data);

         }
     }
     public function driver_rating() {

         if ($this->input->post("submit") && $this->input->post("submit") != "") {
           if ($this->input->post("rides_comment")!="") {
             $comment=$this->input->post("rides_comment");
           }else{
             $comment='';
           }
           $data = array(
               "rides_id" => $this->uri->segment(4),
               "driver_id" => $this->input->post("driver_id"),
               "rating" => $this->input->post("rating"),
               "rides_comment" =>$comment
           );

           $table = "tbl_driver_rating";

           $password_updated = $this->my_custom_functions->insert_data($data, $table);
           redirect('rider/user/rider_dashboard/'.TAXI_APP_DRIVER_SECURITY_KEY);

         }
     }
     public function currency_setting() {

       if ($this->input->post("submit") && $this->input->post("submit") != "") {
         $data = array(
             "currency_id" => $this->input->post("currency_id")
         );
         $where = array(
             "id" => $this->session->userdata('rider_id')
         );
         $this->my_custom_functions->update_data($data,'tbl_rider', $where);
         $this->session->set_flashdata("s_message", "Currency Successfully Updated");
         redirect('rider/user/currency_setting/'.TAXI_APP_DRIVER_SECURITY_KEY);

       }else {
         $this->load->view('rider_new/change_car_type');
       }

     }

     function save_referral_details(){
       $rider_id =$this->session->userdata('rider_id');
       $email_id =$this->input->post('email_id');
       $email_exist_check = $this->my_custom_functions->get_particular_field_value("tbl_referral_details", "email_id", " and email_id ='".$email_id ."'");
       if($email_exist_check == ""){
       $data = array(
           "rider_id" => $rider_id,
           "email_id" => $email_id,
           "date_time" => time()
       );
       //echo '<pre>';print_r($data);die;
       $table = "tbl_referral_details";
       $insert = $this->my_custom_functions->insert_data($data, $table);
       if($insert){
       echo 'success';
       }else{
         echo 'fail';
       }
       }else{
         echo 'exist';
       }
     }


     // function add_wallet_balance_referral()
     // {
     //   $query = $this->Rider_model->get_eligible_customer();
     //   echo '<pre>';print_r($query);
     //   if(!empty($query)){
     //     foreach($query as $data){
     //       if($data['COUNTS'] > 9){
     //          $rider_id = $data['rider_id'];
     //
     //          //echo '<pre>';print_r($data);die;
     //          $table = "tbl_wallet";
     //          $wallet_balance_exist = $this->my_custom_functions->get_particular_field_value("tbl_wallet", "wallet_balance", " and rider_id ='".$rider_id ."'");
     //          if($wallet_balance_exist == ""){
     //          $data_new = array(
     //              "rider_id" => $rider_id,
     //              "wallet_balance" => 50,
     //          );
     //          $insert = $this->my_custom_functions->insert_data($data_new, $table);
     //          }else{
     //          $where = array(
     //              "rider_id" => $rider_id
     //          );
     //          //echo '<pre>';print_r($where);
     //          $data_update = array(
     //              "wallet_balance" => ($wallet_balance_exist+50),
     //          );
     //          $this->my_custom_functions->update_data($data_update, $table, $where);
     //          }
     //       }
     //     }
     //   }
     // }

    /////////  -------  #END  #END  #END   #END   --------   ///////////
}
