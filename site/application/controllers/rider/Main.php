<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("rider/Rider_model");
        $last = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);
        if($record_num != TAXI_APP_DRIVER_SECURITY_KEY) {
            die();
        }
    }

    /////////////// ------------  RIDER REGISTRATION   -----------   //////////////////
    public function index() {

        if($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r ($_POST);die;

            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('phone', 'Mobile Number','required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tbl_rider.email]');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
            //$this->form_validation->set_rules('promocode', 'Promocode', 'required');

            if ($this->form_validation->run() == false) { // Form validation failed

                $this->session->set_flashdata('validation_message', validation_errors('<p class="e_message">'));
                redirect('rider/main/'.TAXI_APP_DRIVER_SECURITY_KEY);
            } else {

                $password = password_hash($this->input->post("confirm_password"), PASSWORD_DEFAULT);
                $refer_email = $this->input->post('email');
                $refer_status = $this->my_custom_functions->get_details_from_id("", "tbl_referral_details", array("email_id" =>$refer_email));
                //echo "<pre>";print_r ($refer_status);die;
                //echo $refer_status;die;
                if (count($refer_status)>0) {
                  $refer_count = $this->my_custom_functions->get_particular_field_value("tbl_wallet", "referral_count", " and rider_id ='".$refer_status['rider_id'] ."'");
                  $data_wallet = array(
                      "referral_count" => $refer_count+1
                  );
                  $where_wallet = array(
                      "rider_id" => $refer_status['rider_id']
                  );
                  $update_wallet = $this->my_custom_functions->update_data($data_wallet,"tbl_wallet", $where_wallet);
                  $data_refer = array(
                      "paid_status" => 1
                  );
                  $update_refer = $this->my_custom_functions->update_data($data_refer,"tbl_referral_details", $where_wallet);
                }
                $refer_count_final = $this->my_custom_functions->get_details_from_id("", "tbl_wallet", array("rider_id" =>$refer_status['rider_id']));
                if ($refer_count_final['referral_count']==2) {
                  $data_wallet = array(
                      "wallet_balance" => $refer_count_final['wallet_balance']+100,
                      "referral_count" =>0
                  );
                  $where_wallet = array(
                      "rider_id" => $refer_status['rider_id']
                  );
                  $update_wallet_final = $this->my_custom_functions->update_data($data_wallet,"tbl_wallet", $where_wallet);
                  for ($i=1; $i <= 2 ; $i++) {

                    $delete_refer = $this->my_custom_functions->delete_data("tbl_referral_details","rider_id ='".$refer_status['rider_id'] ."' and paid_status ='1'");
                  }

                }
                $rider_data = array(
                    'first_name'          =>  $this->input->post('first_name'),
                    'last_name'        =>  $this->input->post('last_name'),
                    'phone'         =>  $this->input->post('phone'),
                    'email'         =>  $this->input->post('email'),
                    'username'         =>  $this->input->post('email'),
                    'password'      =>  $password,
                    'country_id'    =>  $this->input->post('country'),
                    'promocode'      =>  $this->input->post('promocode'),
                    'status'        =>  1
                );

                $rider_id= $this->Rider_model->insert_rider_data($rider_data);///////  insert to rider table

                ///////////    uploading photo ID of rider   //////////

                if(!empty($_FILES)) {

                    $this->load->library('image_lib');

                    $config = array(
                        'image_library'=>'gd2',
                        'allowed_types'=>'gif|jpg|png|jpeg',
                        'overwrite'=>true,
                        'maintain_ratio'=>true,
                        'source_image'=>$_FILES['formal_id']['tmp_name'],
                        'new_image'=>"uploads/rider/". $rider_id .".jpg",
                    );

                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                }

                ///////////   ----------  SEND MAIL TO USER AFTER REGISTRATION IS COMPLETE   ----------   ////////

                $rider_details = $this->my_custom_functions->get_details_from_id("", "tbl_rider", array("id" => $rider_id));

                $this->load->helper('file');

                $message = "";
                $message .= read_file("application/views/mail_template/mail_template_header.php");
                $message .= 'Hello '.$rider_details['name'].',<br><br>';
                $message .= 'Thanks for registering in the '.SITE_NAME.'.<br><br>';
                $message .= read_file("application/views/mail_template/mail_template_footer.php");

                $this->my_custom_functions->SendEmail(SITE_EMAIL.','.SITE_NAME, $rider_details['email'], SITE_NAME.' : Registration Successful', $message);

                $this->session->set_flashdata('s_message', 'You Have Successfully Registered Yourselves On '.SITE_NAME);
                redirect('rider/main/rider_login/'.TAXI_APP_DRIVER_SECURITY_KEY);
            }
        } else {
            $data['title'] = 'Rider';
            $this->load->view('rider_new/rider_registration', $data);
        }
    }

    /////////////// ------------  RIDER LOGIN   -----------   //////////////////

    public function rider_login() {
      //echo "string";die;

        if($this->input->post('submit') && $this->input->post('submit') != ''){

            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('username', 'Username', 'required');

            if ($this->form_validation->run() == false) { // Form validation failed

                $this->session->set_flashdata('validation_message', validation_errors('<p class="e_message">'));
                redirect('rider/main/rider_login/'.TAXI_APP_DRIVER_SECURITY_KEY);

            } else {

                $response = $this->Rider_model->login_check();

                if ($response == "1") { // If credentials match with system

                    redirect('rider/user/rider_dashboard/'.TAXI_APP_DRIVER_SECURITY_KEY);
                } else { // If credentials don't match

                    $this->session->set_flashdata('e_message', 'Invalid Login Credentials');
                    redirect('rider/main/rider_login/'.TAXI_APP_DRIVER_SECURITY_KEY);
                }
            }
        } else {

          if($this->session->userdata('rider_id')) { /// If the club member is already logged in, don't show the login page
              redirect('rider/user/rider_dashboard/'.TAXI_APP_DRIVER_SECURITY_KEY);
          }

            $data['title'] = 'Rider Login';
            $this->load->view('rider_new/rider_login', $data);
        }
    }

    ///////////// ------------  RIDER FORGOT PASSWORD   -----------   //////////////////

    public function forgot_password() {

        if($this->input->post('submit') && $this->input->post('submit') != '') {

            $this->form_validation->set_rules('user_name', 'User Name', 'trim|required');

            if($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("rider/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);

            } else {

                $userlogin_id = mysqli_real_escape_string($this->my_custom_functions->get_mysqli(), strip_tags(trim($this->input->post("user_name"))));
                $return = $this->my_custom_functions->get_perticular_count("tbl_rider", " and username ='".$userlogin_id."'");

                if($return) {

                    $rider_id = $this->my_custom_functions->get_particular_field_value("tbl_rider", "id", " and username ='".$userlogin_id."'");
                    $rider_details = $this->my_custom_functions->get_details_from_id("", "tbl_rider", array("id" => $rider_id));
                  //  echo '<pre>';print_r($rider_details);die;
                    $reset_link = base_url() . 'rider/main/reset_password/' . $rider_id . '/' . $this->my_custom_functions->ablEncrypt($rider_details['username']);

                    $this->load->helper('file');

                    $message = "";
                    $message .= read_file("application/views/mail_template/mail_template_header.php");
                    $message .= 'Hello '.$rider_details['name'].',<br><br>';
                    $message .= 'You are receiving this email because you have requested for a change of password.<br><br>';
                    $message .= 'You may <a href="'.$reset_link.'">click here</a> to reset your password now.<br><br>Thank you.';
                    $message .= read_file("application/views/mail_template/mail_template_footer.php");

                    $this->my_custom_functions->SendEmail(SITE_EMAIL.','.SITE_NAME, $rider_details['email'], SITE_NAME.' : Password recovery link', $message);

                    $this->session->set_flashdata("s_message", "A password recovery email has been sent to your email account.");
                    redirect("rider/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);

                } else {

                    $this->session->set_flashdata("e_message", "You are not a registered Rider!");
                    redirect("rider/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                }
            }
        } else {

            $data['title'] = 'Forgot Password?';
            $this->load->view('rider/forgot_password', $data);
        }
    }

    ///////////// ------------  RIDER RESET PASSWORD   -----------   //////////////////
    public function reset_password() {

        if($this->input->post('submit') && $this->input->post('submit') != '') {

            $this->form_validation->set_rules('nw_password', 'New Password', 'trim|required');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[nw_password]');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("rider/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);

            } else {

                $data = array(
                    "password" => password_hash($this->input->post('confirm_password'), PASSWORD_DEFAULT)
                );

                $table = "tbl_rider";

                $where = array(
                    "id" => $this->session->userdata("reset_rider_id")
                );
                $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
                $this->session->unset_userdata("reset_rider_id");

                if ($password_updated) {

                    $this->session->set_flashdata("s_message", 'Password has been changed successfully. <a href="'.base_url().'rider/main/rider_login">Login here</a> to continue.');
                    redirect("rider/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                } else {

                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("rider/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                }
            }
        } else {

            $rider_id = $this->uri->segment(4);
            $code = $this->uri->segment(5);
            if(isset($rider_id)) {
                $rider_details = $this->my_custom_functions->get_details_from_id("", "tbl_rider", array("id" => $rider_id));
                //echo '<pre>';print_r($rider_details);die;
                if ($rider_details['username'] == $this->my_custom_functions->ablDecrypt($code)) {

                    $this->session->set_userdata("reset_rider_id", $rider_id);
                    $data['title'] = 'Reset Password';
                    $this->load->view('rider/reset_password/', $data);
                } else {

                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("rider/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                }
            } else {

                $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                redirect("rider/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);
            }
        }
    }

    /////////// ------------  RIDER LOGOUT  -----------   //////////////////

    public function rider_logout() {
        $session_data = array("rider_id", "rider_name", "rider_email","act_rides_id");
        $this->session->unset_userdata($session_data);

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("rider/main/rider_login/".TAXI_APP_DRIVER_SECURITY_KEY);
    }


    /////////  -------  #END  #END  #END   #END   --------   ///////////
}
