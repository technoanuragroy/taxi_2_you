<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("admin/Admin_model");
        $this->my_custom_functions->check_admin_security();

    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function profile() {

        $data['active_menu'] = '';
        $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", "tbl_admins", array("admin_id" => $this->session->userdata('admin_id')));

        $this->load->view("admin/dashboard", $data);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Change password from admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function change_password() {

        if ($this->input->post("change_password") && $this->input->post("change_password") != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("o_password", "Old Password", "trim|required");
            $this->form_validation->set_rules("n_password", "New Password", "trim|required|min_length[6]");
            $this->form_validation->set_rules("c_password", "Confirm Password", "trim|required|min_length[6]|matches[n_password]");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/change_password");

            } else {

                $o_password = $this->input->post('o_password');
                $password = $this->my_custom_functions->get_particular_field_value("tbl_admins", "password", " and admin_id='".$this->session->userdata('admin_id')."'");

                if(password_verify($o_password, $password)) {

                    $data = array(
                        "password" => password_hash($this->input->post("n_password"), PASSWORD_DEFAULT)
                    );

                    $table = "tbl_admins";

                    $where = array(
                        "admin_id" => $this->session->userdata('admin_id')
                    );
                    $password_updated = $this->my_custom_functions->update_data($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Password has been updated successfully.");
                    redirect("admin/user/change_password");

                } else {
                    $this->session->set_flashdata("e_message", "Old password is incorrect.");
                    redirect("admin/user/change_password");
                }
            }
        } else {

            $data['active_menu'] = '';
            $this->load->view("admin/change_password", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Edit admin details
    ///////////////////////////////////////////////////////////////////////////////
    function edit_profile() {

        if ($this->input->post("save") && $this->input->post("save") != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("email", "Account Email", "trim|required|valid_email");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/edit_profile");

            } else { /// Update admin

                $admin_id = $this->session->userdata('admin_id');
                $update = $this->Admin_model->update_admin($admin_id);

                $this->load->library('image_lib');
                $this->load->library('upload');

                // Upload image for admin
                if ($_FILES AND $_FILES['adminphoto']['name']) {

                    if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                            ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                            ($_FILES['adminphoto']['type'] == 'image/png') ||
                            ($_FILES['adminphoto']['type'] == 'image/gif')) {

                        list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                        $new_width = "";
                        $new_height = "";

                        $ratio = $width / $height;

                        $new_width = 185;
                        $new_height = $new_width / $ratio;

                        $config['image_library'] = 'gd2';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                        $config['new_image'] = "uploads/admin/" . $admin_id . ".jpg";
                        $config['maintain_ratio'] = FALSE;
                        $config['width'] = $new_width;
                        $config['height'] = $new_height;

                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();

                        $this->session->set_flashdata("s_message", "Successfully updated admin details.");
                        redirect("admin/user/edit_profile/");

                    } else {
                        $this->session->set_flashdata('invalid_img_type', 'Only .jpg, .jpeg, .png, .gif types are allowed');
                        redirect("admin/user/edit_profile/");
                    }
                }

                if($update) {
                    $this->session->set_flashdata('s_message', "Successfully updated admin details.");
                    redirect("admin/user/edit_profile");
                } else {
                    $this->session->set_flashdata('e_message', "Error updating admin details.");
                    redirect("admin/user/edit_profile");
                }
            }
        } else {

            $data['active_menu'] = '';
            $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", "tbl_admins", array("admin_id" => $this->session->userdata('admin_id')));
            $data['admin_details']['image'] = "<img src='".base_url()."images/advertisersIcon.png'>";

            $this->load->view("admin/edit_profile", $data);
        }
    }


    function delete_profilePic() {

        $admin_id = $this->session->userdata('admin_id');

        $file = 'uploads/admin/'.$admin_id.'.jpg';
        if(file_exists($file))
        if (is_file($file)) {
            unlink($file);
            $this->session->set_flashdata('s_message', "Profile picture has been deleted successfully.");
        }else{
            $this->session->set_flashdata('e_message', "Unable to delete, profile picture is not available.");
        }

        redirect("admin/user/edit_profile");
    }



    //////////////////---------MANAGE RIDER---------////////////////////////


    public function manage_rider(){
        $data['riders']=$this->Admin_model->get_all_riders();
        $this->load->view('admin/manage_rider',$data);

    }

    function edit_rider(){
        $rider_id = $this->uri->segment(4);
        if($this->input->post('submit') && $this->input->post('submit') != ''){
            //echo "<pre>";print_r($_POST);die;

            $this->form_validation->set_rules('first_name', 'Rider First Name', 'required');
            $this->form_validation->set_rules('last_name', 'Rider Last Name', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');

            if ($this->form_validation->run() == false) { //form validation failed
                $this->session->set_flashdata('e_message', validation_errors());
                redirect('admin/user/edit_rider/'.$rider_id);
            } else {
                $rider_data = array(
                    'first_name'       => $this->input->post('first_name'),
                    'last_name'       => $this->input->post('last_name'),
                    'phone'      => $this->input->post('phone'),
                    'email'      => $this->input->post('email'),
                    'username'   => $this->input->post('username'),
                    'country_id' => $this->input->post('country'),
                    'status'     => $this->input->post('status')

                );
                $this->Admin_model->update_rider_data($rider_data, $rider_id);

                $this->session->set_flashdata('s_message', 'Record updated successfully');
                redirect('admin/user/manage_rider');
            }
        } else {

            $data['riders_data']=$this->Admin_model->get_riders_id($rider_id);
            $this->load->view('admin/edit_rider', $data);
        }
    }



    ////////////////////--------------MANAGE DRIVER-------------///////////////

    function manage_driver(){
        $data['drivers']=$this->Admin_model->total_drivers();
        $this->load->view('admin/manage_driver',$data);
    }

   function edit_driver(){
       $driver_id = $this->uri->segment(4);
        if($this->input->post('submit') && $this->input->post('submit') != ''){
            //echo "<pre>";print_r($_POST);die;

            $this->form_validation->set_rules('name', 'Rider Name', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('vehicle_name', 'Vehicle Model', 'required');
            $this->form_validation->set_rules('vehicle_make', 'Vehicle Make', 'required');
            $this->form_validation->set_rules('vehicle_model', 'Vehicle Model', 'required');
            $this->form_validation->set_rules('promo', 'Promo Code', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('vehicle_color', 'Vehicle color', 'required');
            $this->form_validation->set_rules('vehicle_plate', 'Vehicle Plate', 'required');
            if ($this->form_validation->run() == false) { //form validation failed
                $this->session->set_flashdata('e_message', validation_errors());
                redirect('admin/user/edit_driver/'.$driver_id);
            } else {
                $rider_data = array(
                    'name'           => $this->input->post('name'),
                    'phone'          => $this->input->post('phone'),
                    'email'          => $this->input->post('email'),
                    'username'       => $this->input->post('username'),
                    'vehicle_name'   => $this->input->post('vehicle_name'),
                    'vehicle_make'   => $this->input->post('vehicle_make'),
                    'vehicle_model'  => $this->input->post('vehicle_model'),
                    'promocode'      => $this->input->post('promo'),
                    'country_id'  => $this->input->post('country'),
                    'vehicle_color'  => $this->input->post('vehicle_color'),
                    'vehicle_plate_no'  => $this->input->post('vehicle_plate'),
                    'status'         => $this->input->post('status')

                );
                $this->Admin_model->update_driver_data($rider_data, $driver_id);

                $this->session->set_flashdata('s_message', 'Record updated successfully');
                redirect('admin/user/manage_driver');
            }
        } else {

            $data['driver_data']=$this->Admin_model->get_drivers_id($driver_id);
            $this->load->view('admin/edit_driver', $data);
        }
   }


   public function update_status(){
      $id = $this->input->post('id');
      $status = $this->input->post('status');
      if($status == 0){
        $changed_stat = 1;
      }else{
        $changed_stat = 0;
      }
      $data = array(
        'status' => $changed_stat,
      );
      $return = $this->my_custom_functions->update_data( $data, 'tbl_driver', 'id = "'.$id.'"');
      $status = $this->my_custom_functions->get_particular_field_value('tbl_driver', 'status', 'and id="'.$id.'"');
      if($return){
        echo $status;
      }else{
        echo $status;
      }
    }

    public function manage_admin()
    {
      $data['admin_data']=$this->Admin_model->get_all_admin();
      $this->load->view('admin/manage_admin',$data);
    }

    public function addAdmin()
    {
      if($this->input->post('submit') && $this->input->post('submit') != ''){
          //echo "<pre>";print_r($_POST);die;

          $this->form_validation->set_rules('admin_name', 'Admin Name', 'required');
          $this->form_validation->set_rules('admin_phone', 'Admin Phone', 'required');
          $this->form_validation->set_rules('email', 'E-Mail', 'required');
          $this->form_validation->set_rules('user_name', 'User Name', 'required');
          $this->form_validation->set_rules('password', 'Password', 'required');
          $this->form_validation->set_rules('country', 'Country', 'required');
          if ($this->form_validation->run() == false) { //form validation failed
              $this->session->set_flashdata('e_message', validation_errors());
              redirect('admin/user/addAdmin');
          } else {
            $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
              $data = array(
                  'admin_type' => 1,
                  'name'  => $this->input->post('admin_name'),
                  'phone'  => $this->input->post('admin_phone'),
                  'email'  => $this->input->post('email'),
                  'username'  => $this->input->post('user_name'),
                  'password'  => $password,
                  'date_created' => date('Y-m-d h:i:s'),
                  'country_id'  => $this->input->post('country'),
                  'status' => 1
              );
              $this->my_custom_functions->insert_data($data,"tbl_admins");

              $this->session->set_flashdata('s_message', 'Record Insert successfully');
              redirect('admin/user/manage_admin');
          }
      } else {
        $this->load->view('admin/add_admin');
      }
    }

    public function manage_currency(){
        $data['currency_data']=$this->Admin_model->get_all_currency();
        $this->load->view('admin/manage_currency',$data);

    }
    public function add_currency(){
      if($this->input->post('submit') && $this->input->post('submit') != ''){
          //echo "<pre>";print_r($_POST);die;

          $this->form_validation->set_rules('currency_name', 'Currency Name', 'required');
          $this->form_validation->set_rules('currency_symbol', 'Currency Symbol', 'required');
          $this->form_validation->set_rules('currency_rate', 'Currency Rate', 'required');
          if ($this->form_validation->run() == false) { //form validation failed
              $this->session->set_flashdata('e_message', validation_errors());
              redirect('admin/user/add_currency');
          } else {
              $data = array(
                  'currency_name'  => $this->input->post('currency_name'),
                  'currency_symbol'=> $this->input->post('currency_symbol'),
                  'currency_rate'  => $this->input->post('currency_rate')
              );
              $this->my_custom_functions->insert_data($data,"tbl_currency");

              $this->session->set_flashdata('s_message', 'Record Insert successfully');
              redirect('admin/user/manage_currency');
          }
      } else {
        $this->load->view('admin/add_currency');
      }

    }

    function edit_currency(){
        $cur_id = $this->uri->segment(4);
        if($this->input->post('submit') && $this->input->post('submit') != ''){
            //echo "<pre>";print_r($_POST);die;

            $this->form_validation->set_rules('currency_name', 'Currency Name', 'required');
            $this->form_validation->set_rules('currency_symbol', 'Currency Symbol', 'required');
            $this->form_validation->set_rules('currency_name', 'Currency Rate', 'required');


            if ($this->form_validation->run() == false) { //form validation failed
                $this->session->set_flashdata('e_message', validation_errors());
                redirect('admin/user/edit_currency/'.$rider_id);
            } else {
                $rider_data = array(
                    'currency_name'       => $this->input->post('currency_name'),
                    'currency_symbol'     => $this->input->post('currency_symbol'),
                    'currency_rate'      => $this->input->post('currency_rate'),

                );
                $this->Admin_model->update_currency_data($rider_data, $cur_id);

                $this->session->set_flashdata('s_message', 'Record updated successfully');
                redirect('admin/user/manage_currency');
            }
        } else {

            $data['currency_data']=$this->Admin_model->get_currency_by_id($cur_id);
            //echo '<pre>';print_r($data);
            $this->load->view('admin/edit_currency', $data);
        }
    }
    ////////////////////--------------MANAGE DRIVER-------------///////////////

    function delete_currency(){
    $user_id = $this->uri->segment(4);
    $this->my_custom_functions->delete_data('tbl_currency',array("id" => $this->uri->segment(4)));
    $this->session->set_flashdata('s_message', 'Currency successfully Deleted');
    redirect('admin/user/manage_currency');
     }

    function edit_admin(){
        $admin_id = $this->uri->segment(4);
        if($this->input->post('submit') && $this->input->post('submit') != ''){
            //echo "<pre>";print_r($_POST);die;

            $this->form_validation->set_rules('admin_name', 'Admin Name', 'required');
            $this->form_validation->set_rules('admin_phone', 'Admin Phone', 'required');
            $this->form_validation->set_rules('email', 'E-Mail', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');


            if ($this->form_validation->run() == false) { //form validation failed
                $this->session->set_flashdata('e_message', validation_errors());
                redirect('admin/user/edit_admin/'.$admin_id);
            } else {

              $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
                $data = array(
                    'admin_type' => 1,
                    'name'  => $this->input->post('admin_name'),
                    'phone'  => $this->input->post('admin_phone'),
                    'email'  => $this->input->post('email'),
                    'password'  => $password,
                    'date_created' => date('Y-m-d h:i:s'),
                    'country_id'  => $this->input->post('country'),
                    'status' => 1
                );
                //echo '<pre>';print_r($data);die;
                $this->Admin_model->update_admin_data($data, $admin_id);

                $this->session->set_flashdata('s_message', 'Record updated successfully');
                redirect('admin/user/manage_admin');
            }
        } else {

            $data['admin_data']=$this->Admin_model->get_admin_by_id($admin_id);

            //echo '<pre>';print_r($data);
            $this->load->view('admin/edit_admin', $data);
        }
    }

    //////////////////  END OF CONTROLLER  ////////////////
}
