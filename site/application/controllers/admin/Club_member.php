<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Club_member extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("admin/Admin_model");
        $this->load->model("club_member/Club_member_model");
        $this->my_custom_functions->check_admin_security();

    }


    ////////////////// ---------   MANAGE CLUB MEMBER SECTION  ---------- //////////////////////

    public function manage_club_member(){
      if($this->input->post('submit') AND $this->input->post('submit') == "Search"){
        $search_filter = "";

          if ($this->input->post('email') <> "") {
              $search_filter .= " AND club_mem.email like BINARY '".$this->input->post('email')."'";
          }
          if ($this->input->post('country') <> "") {
              $search_filter .= " AND club_mem.country = '".$this->input->post('country')."' ";
          }
          //echo $search_filter;die;
          $sql = " SELECT * FROM tbl_login_details login INNER JOIN tbl_club_members club_mem ON login.id = club_mem.id WHERE 1=1 AND login.type = '1' " . $search_filter . " ORDER BY join_date ASC" ;
          $sql = base64_encode($sql);
          $get_club_member['club_members'] = $this->Admin_model->club_mem_search($sql);
          $this->load->view('admin/club_member/manage_club_member', $get_club_member);
      }else{
        $get_club_member['club_members'] = $this->Admin_model->get_all_club_member();
        $this->load->view('admin/club_member/manage_club_member', $get_club_member);
      }
    }

    public function view_club_mem_det(){
      $club_mem_id = $this->uri->segment(4);
      $club_mem['club_member_det'] = $this->Admin_model->view_club_mem($club_mem_id);
      //echo "<pre>";print_r($club_mem);die;
      $this->load->view('admin/club_member/view_club_member', $club_mem);
    }

    public function update_status(){
      $id = $this->input->post('id');
      $status = $this->input->post('status');
      if($status == 0){
        $changed_stat = 1;
      }else{
        $changed_stat = 0;
      }
      $data = array(
        'status' => $changed_stat,
      );
      $return = $this->my_custom_functions->update_data( $data, 'tbl_login_details', 'id = "'.$id.'"');
      $status = $this->my_custom_functions->get_particular_field_value('tbl_login_details', 'status', 'and id="'.$id.'"');
      if($return){
        echo $status;
      }else{
        echo $status;
      }
    }







////////////////////    ----------------  #END #END  #END  #END  ----------------   ////////////////////////
}
