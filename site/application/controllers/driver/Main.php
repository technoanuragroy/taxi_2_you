<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->my_custom_functions->check_driver_security();
        $this->load->model("driver/Driver_model");
        //$this->my_custom_functions->check_security_key();
        $last = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);
        if($record_num != TAXI_APP_DRIVER_SECURITY_KEY) {
            die();
        }
    }

    /////////////// ------------  driver REGISTRATION   -----------   //////////////////
    public function index() {
        if($this->input->post('submit') && $this->input->post('submit') == 'sign up') {
            //echo "<pre>";print_r ($_POST);die;
            //  echo 'here';die;
            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('mobile', 'Mobile Number','required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            //$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

            if ($this->form_validation->run() == false) { // Form validation failed

                $this->session->set_flashdata('validation_message', validation_errors('<p class="e_message">'));
                redirect('driver/main/'.TAXI_APP_DRIVER_SECURITY_KEY);
            }
            else {
                $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
                $driver_data = array(
                  'car_type' => $this->input->post('car_type'),
                    'first_name'   =>  $this->input->post('first_name'),
                    'last_name'   =>  $this->input->post('last_name'),
                    'phone'        =>  $this->input->post('mobile'),
                    'email'         =>  $this->input->post('email'),
                    'username'      =>  $this->input->post('email'),
                    'password'      =>  $password,
                    'country_id'    =>  $this->input->post('country'),
                    'promocode'    =>  $this->input->post('promocode'),
                    'profile_completed_steps'=>1,
                );
              //  echo '<pre>';print_r($driver_data);die;
                $driver_id= $this->Driver_model->insert_driver_data($driver_data);///////  insert to driver table
                redirect('driver/main/'.$driver_id.'/2/'.TAXI_APP_DRIVER_SECURITY_KEY);
              }
            }else {
                  $data['title'] = 'Driver';
                  $this->load->view('driver/driver_registration_one', $data);
            }

          if($this->input->post('submit') && $this->input->post('submit') == 'upload') {

            if(!empty($_FILES)) {
             //echo '<pre>';print_r($_FILES['driving_licence']['tmp_name']);die;

           $this->load->library('image_lib');

           $config = array(
               'image_library'=>'gd2',
               'allowed_types'=>'gif|jpg|png|jpeg',
               'overwrite'=>true,
               'maintain_ratio'=>true,
               'source_image'=>$_FILES['id_proof']['tmp_name'],
               'new_image'=>"uploads/driver/idproof/". $this->uri->segment(3) .".jpg",
           );

           $this->image_lib->initialize($config);
           $this->image_lib->resize();
           $config = array(
               'image_library'=>'gd2',
               'allowed_types'=>'gif|jpg|png|jpeg',
               'overwrite'=>true,
               'maintain_ratio'=>true,
               'source_image'=>$_FILES['driving_licence']['tmp_name'],
               'new_image'=>"uploads/driver/licence/". $this->uri->segment(3) .".jpg",
           );

           $this->image_lib->initialize($config);
           $this->image_lib->resize();
           redirect('driver/main/'.$this->uri->segment(3).'/3/'.TAXI_APP_DRIVER_SECURITY_KEY);
       }
          }
          if($this->input->post('submit') && $this->input->post('submit') == 'register') {
              //echo "<pre>";print_r ($_POST);die;
              //  echo 'here';die;
              $this->form_validation->set_rules('vehicle_name', 'Vehicle Name', 'required');
              $this->form_validation->set_rules('vehicle_make', 'Vehicle Maker', 'required');
              $this->form_validation->set_rules('vehicle_model', 'Vehicle model','required');
              $this->form_validation->set_rules('vehicle_color', 'Vehicle Color', 'required');
              $this->form_validation->set_rules('vehicle_plate_no', 'Vehicle Plate No.', 'required');
              //$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

              if ($this->form_validation->run() == false) { // Form validation failed

                  $this->session->set_flashdata('validation_message', validation_errors('<p class="e_message">'));
                  redirect('driver/main'.$this->uri->segment(3).'/3/'.TAXI_APP_DRIVER_SECURITY_KEY);
              }
              else {

                //echo "<pre>";print_r($_FILES);die;
                  //$password = password_hash($this->input->post("confirm_password"), PASSWORD_DEFAULT);
                  $vehicle_data = array(
                      'vehicle_name'   =>  $this->input->post('vehicle_name'),
                      'vehicle_make'   =>  $this->input->post('vehicle_make'),
                      'vehicle_model'        =>  $this->input->post('vehicle_model'),
                      'vehicle_color'         =>  $this->input->post('vehicle_color'),
                      'vehicle_plate_no'      =>  $this->input->post('vehicle_plate_no'),
                      'profile_completed_steps'=>3,
                  );
                  //echo '<pre>';print_r($_FILES);die;
                  $this->Driver_model->update_vehicle_data($vehicle_data);///////  insert to driver table
                  if(!empty($_FILES)) {
                   //echo '<pre>';print_r($_FILES['vehicle_photo']['tmp_name']);die;

                 $this->load->library('image_lib');

                 $config = array(
                     'image_library'=>'gd2',
                     'allowed_types'=>'gif|jpg|png|jpeg',
                     'overwrite'=>true,
                     'maintain_ratio'=>true,
                     'source_image'=>$_FILES['vehicle_photo']['tmp_name'],
                     'new_image'=>"uploads/driver/vehicle_photo/". $this->uri->segment(3) .".jpg",
                 );
                 $this->image_lib->initialize($config);
                 $this->image_lib->resize();


                // $config['upload_path']          = 'uploads/driver/vehicle_video';
                // $config['allowed_types']        = 'mp4';
                // $config['max_size']             = 100000;
                $video_config1 = array(
                    'upload_path' => 'uploads/driver/vehicle_video',
                    'allowed_types' => 'mp4',
                    'max_size' => 6000,
                    'overwrite'=>TRUE,
                    'file_name'=> $this->uri->segment(3) ."_1.mp4"
                );

                $this->load->library('upload', $video_config1);
                $this->upload->initialize($video_config1);

                if ($_FILES['vehicle_video_1']['size'] > 5500000) {
                    $this->session->set_flashdata('e_message', 'File size exceeded of Vehicle video 1! Try again');
                    redirect('driver/main/'.$this->uri->segment(3).'/3/'.TAXI_APP_DRIVER_SECURITY_KEY);
                }

                if ( ! $this->upload->do_upload('vehicle_video_1'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->session->set_flashdata('e_message', 'Upload mp4 video! Try again');
                        redirect('driver/main/'.$this->uri->segment(3).'/3/'.TAXI_APP_DRIVER_SECURITY_KEY);
                }

                $video_config2 = array(
                    'upload_path' => 'uploads/driver/vehicle_video',
                    'allowed_types' => 'mp4',
                    'max_size' => 6000,
                    'overwrite'=>TRUE,
                    'file_name'=> $this->uri->segment(3) ."_2.mp4"

                );

                $this->load->library('upload', $video_config2);
                $this->upload->initialize($video_config2);

                if ($_FILES['vehicle_video_2']['size'] > 5500000) {
                    $this->session->set_flashdata('e_message', 'File size exceeded of Vehicle video 2! Try again');

                    redirect('driver/main/'.$this->uri->segment(3).'/3/'.TAXI_APP_DRIVER_SECURITY_KEY);
                }

                if ( ! $this->upload->do_upload('vehicle_video_2'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->session->set_flashdata('e_message', 'Upload mp4 video! Try again');
                        redirect('driver/main/'.$this->uri->segment(3).'/3/'.TAXI_APP_DRIVER_SECURITY_KEY);
                }



                  $this->session->set_flashdata('e_message', 'Registration successfully');
                  redirect('driver/main/login/'.TAXI_APP_DRIVER_SECURITY_KEY);
                }
              }


            ///////////uploading photo ID of driver   //////////

}

    }

    /////////////// ------------  DRIVER LOGIN   -----------   //////////////////
    public function login() {
      //echo "string";die;
        if($this->input->post('submit') && $this->input->post('submit') != ''){

            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('username', 'Username', 'required');

            if ($this->form_validation->run() == false) { // Form validation failed

                $this->session->set_flashdata('validation_message', validation_errors('<p class="e_message">'));
                redirect('driver/main/login/'.TAXI_APP_DRIVER_SECURITY_KEY);

            } else {

                $response = $this->Driver_model->login_check();
                if ($response == "1") { // If credentials match with system
                    redirect('driver/user/dashboard/'.TAXI_APP_DRIVER_SECURITY_KEY);
                } else { // If credentials don't match
                    $this->session->set_flashdata('e_message', 'Invalid Login Credentials');
                    redirect('driver/main/login/'.TAXI_APP_DRIVER_SECURITY_KEY);
                }
            }
        } else {

          if($this->session->userdata('driver_id')) { /// If the club member is already logged in, don't show the login page
              redirect('driver/user/dashboard/'.TAXI_APP_DRIVER_SECURITY_KEY);
          }

            $data['title'] = 'driver Login';
            $this->load->view('driver/login', $data);
        }
    }

    ///////////// ------------  DRIVER FORGOT PASSWORD   -----------   //////////////////
    public function forgot_password() {

        if($this->input->post('submit') && $this->input->post('submit') != '') {

            $this->form_validation->set_rules('user_name', 'User Name', 'trim|required');

            if($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("driver/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);

            } else {

                $userlogin_id = mysqli_real_escape_string($this->my_custom_functions->get_mysqli(), strip_tags(trim($this->input->post("user_name"))));
                $return = $this->my_custom_functions->get_perticular_count("tbl_driver", " and username ='".$userlogin_id."'");

                if($return) {

                    $driver_id = $this->my_custom_functions->get_particular_field_value("tbl_driver", "id", " and username ='".$userlogin_id."'");
                    $driver_details = $this->my_custom_functions->get_details_from_id("", "tbl_driver", array("id" => $driver_id));
                  //  echo '<pre>';print_r($driver_details);die;
                    $reset_link = base_url() . 'driver/main/reset_password/' . $driver_id . '/' . $this->my_custom_functions->ablEncrypt($driver_details['username']);

                    $this->load->helper('file');

                    $message = "";
                    $message .= read_file("application/views/mail_template/mail_template_header.php");
                    $message .= 'Hello '.$driver_details['name'].',<br><br>';
                    $message .= 'You are receiving this email because you have requested for a change of password.<br><br>';
                    $message .= 'You may <a href="'.$reset_link.'">click here</a> to reset your password now.<br><br>Thank you.';
                    $message .= read_file("application/views/mail_template/mail_template_footer.php");

                    $this->my_custom_functions->SendEmail(SITE_EMAIL.','.SITE_NAME, $driver_details['email'], SITE_NAME.' : Password recovery link', $message);

                    $this->session->set_flashdata("s_message", "A password recovery email has been sent to your email account.");
                    redirect("driver/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);

                } else {

                    $this->session->set_flashdata("e_message", "You are not a registered driver!");
                    redirect("driver/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                }
            }
        } else {

            $data['title'] = 'Forgot Password?';
            $this->load->view('driver/forgot_password', $data);
        }
    }

    ///////////// ------------  CLUB MEMBER RESET PASSWORD   -----------   //////////////////
    public function reset_password() {

        if($this->input->post('submit') && $this->input->post('submit') != '') {

            $this->form_validation->set_rules('nw_password', 'New Password', 'trim|required');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[nw_password]');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("driver/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);

            } else {

                $data = array(
                    "password" => password_hash($this->input->post('confirm_password'), PASSWORD_DEFAULT)
                );

                $table = "tbl_driver";

                $where = array(
                    "id" => $this->session->userdata("reset_driver_id")
                );
                $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
                $this->session->unset_userdata("reset_driver_id");

                if ($password_updated) {

                    $this->session->set_flashdata("s_message", 'Password has been changed successfully. <a href="'.base_url().'driver/main/login">Login here</a> to continue.');
                    redirect("driver/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                } else {

                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("driver/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                }
            }
        } else {

            $driver_id = $this->uri->segment(4);
            $code = $this->uri->segment(5);
            if(isset($driver_id)) {
                $driver_details = $this->my_custom_functions->get_details_from_id("", "tbl_driver", array("id" => $driver_id));
                //echo '<pre>';print_r($driver_details);die;
                if ($driver_details['username'] == $this->my_custom_functions->ablDecrypt($code)) {

                    $this->session->set_userdata("reset_driver_id", $driver_id);
                    $data['title'] = 'Reset Password';
                    $this->load->view('driver/reset_password', $data);
                } else {

                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("driver/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                }
            } else {

                $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                redirect("driver/main/forgot_password/".TAXI_APP_DRIVER_SECURITY_KEY);
            }
        }
    }

    /////////// ------------  CLUB MEMBER LOGOUT   -----------   //////////////////
    public function driver_logout() {

        $session_data = array("driver_id", "driver_name", "driver_email","rides_reject_id");
        $this->session->unset_userdata($session_data);

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("driver/main/login/".TAXI_APP_DRIVER_SECURITY_KEY);
    }

    /////////  -------  #END  #END  #END   #END   --------   ///////////
}
