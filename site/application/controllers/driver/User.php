<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->check_driver_security();
        $this->load->model("driver/Driver_model");
        $last = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);
        if($record_num != TAXI_APP_DRIVER_SECURITY_KEY) {
            die();
        }
    }

    /////////////// ------------  DASHBOARD   -----------   //////////////////
    public function dashboard() {
        //echo '<pre>';print_r($this->session->userdata);die;
        $data['details'] = $this->my_custom_functions->get_details_from_id("", "tbl_driver", array("id" => $this->session->userdata("driver_id")));
        //$data['orders'] = $this->driver_model->order_summary($this->session->userdata("driver_id"));
        //echo "<pre>";print_r($data['details']);die;
        $this->load->view('driver/dashboard',$data);
    }

    /////////////// ------------  EDIT PROFILE   -----------   //////////////////
    public function edit_profile() {

        if($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r ($_POST);die;

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');

            if ($this->form_validation->run() == false) { // Form validation failed

                $this->session->set_flashdata('validation_message', validation_errors('<p class="e_message">'));
                redirect('driver/user/edit_profile/'.TAXI_APP_DRIVER_SECURITY_KEY);

            } else {
                //echo '<pre>';print_r($this->input->post());die;
                $this->Driver_model->update_driver_data();

                ///////////    uploading photo ID of club member   //////////

                if(!empty($_FILES)) {

                    $this->load->library('image_lib');
                    $config = array(
                        'image_library'=>'gd2',
                        'allowed_types'=>'gif|jpg|png|jpeg',
                        'overwrite'=>true,
                        'maintain_ratio'=>true,
                        'source_image'=>$_FILES['formal_id']['tmp_name'],
                        'new_image'=>"uploads/driver/". $this->session->userdata('driver_id') .".jpg",
                    );

                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                }

                $this->session->set_flashdata('s_message', 'You Have Successfully Updated Your Profile');
                redirect('driver/user/edit_profile/'.TAXI_APP_DRIVER_SECURITY_KEY);

            }
        } else {

            $data['title'] = 'Edit Profile';
            $data['details'] = $this->my_custom_functions->get_details_from_id("", "tbl_driver", array("id" => $this->session->userdata("driver_id")));
            $this->load->view('driver/edit_profile', $data);
        }
    }

    /////////////// ------------  CHANGE PASSWORD   -----------   //////////////////
    public function change_password() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->load->library("form_validation");

            /// tbl_companies contents
            $this->form_validation->set_rules("o_password", "Old Password", "trim|required");
            $this->form_validation->set_rules("n_password", "New Password", "trim|required|min_length[6]");
            $this->form_validation->set_rules("c_password", "Confirm Password", "trim|required|min_length[6]|matches[n_password]");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("driver/user/change_password/".TAXI_APP_DRIVER_SECURITY_KEY);

            } else {

                $o_password = $this->input->post('o_password');
                $password = $this->my_custom_functions->get_particular_field_value("tbl_login_details", "password", " and id='".$this->session->userdata('club_member_id')."'");

                if(password_verify($o_password, $password)) {

                    if($o_password != $this->input->post("n_password")) {

                        $password = password_hash($this->input->post("n_password"), PASSWORD_DEFAULT);
                        $data = array(
                            "password" => $password
                        );

                        $table = "tbl_driver";

                        $where = array(
                            "id" => $this->session->userdata('driver_id')
                        );
                        $password_updated = $this->my_custom_functions->update_data($data, $table, $where);

                        $this->session->set_flashdata("s_message", "Password Has Been Updated Successfully");
                        redirect("driver/user/change_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                    } else {
                        $this->session->set_flashdata("e_message", "Unable To Update The Password.New And Old Password Can Not Be Same. Use Different Password To Update");
                        redirect("driver/user/change_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                    }

                } else {
                    $this->session->set_flashdata("e_message", "Old Password Is Incorrect");
                    redirect("driver/user/change_password/".TAXI_APP_DRIVER_SECURITY_KEY);
                }
            }
        } else {

            $data['title'] = "Change Password";
            $this->load->view("driver/change_password", $data);
        }
    }



    public function update_status(){
       $id = $this->session->userdata('driver_id');

       $status = $this->input->post('driver_status');
       if($status == 0){
         $changed_stat = 1;
       }else{
         $changed_stat = 0;
       }
       $data = array(
         'driver_status' => $changed_stat,
       );
       $return = $this->my_custom_functions->update_data( $data, 'tbl_driver', 'id = "'.$id.'"');
       $status = $this->my_custom_functions->get_particular_field_value('tbl_driver', 'driver_status', 'and id="'.$id.'"');
       if($return){
         echo $status;
       }else{
         echo $status;
       }
     }


     public function getDriverRequest()
     {
       $driver_id = $this->input->post('driver_id');
      // echo $driver_id;die;
       $query = $this->Driver_model->get_driver_request($driver_id);
       echo $query;
     }

     public function driver_reject_request_status_update()
     {
       $driver_id = $this->input->post('driver_id');
       $rides_id = $this->input->post('rides_id');
       //echo $driver_id;die;
       $data = array('request_status'=> 2);
       $chk_status_list = $this->Driver_model->driver_chk_status_list($driver_id,$rides_id);
       if($chk_status_list){
            $query = $this->Driver_model->driver_reject_request_status_update($rides_id,$data);
            //echo $rides_id;die;
            //$this->session->set_userdata('rides_reject_id',$rides_id);
            echo 'yes';
       }else{
           echo 'notexist';
       }

     }

     public function driver_reject_request_status_update_html()
     {
       $driver_id = $this->input->post('driver_id');
       $rides_id = $this->input->post('rides_id');
       //echo $rides_id;die;

       $query = $this->Driver_model->driver_reject_request_status_update($driver_id,$rides_id);
       echo $query;
       //$query = $this->Driver_model->get_driver_request($driver_id);
       //echo $query;
     }

     function accept_rider_request(){
        $rides_id = $this->input->post('rides_id');
        $data = array(
            'request_status' => 1,
            'status' => 1
          );
        $return = $this->my_custom_functions->update_data( $data, 'tbl_rides', 'rides_id = "'.$rides_id.'"');
        if($return){
            echo 'success';
        }else{
            echo 'failed';
        }
     }

     public function customer_pickup() {
        $rides_id = $this->uri->segment(5);
        //echo $rides_id;
        $data['rides_details'] =  $this->Driver_model->get_rider_details_from_segment($rides_id);
        //echo '<pre>';print_r($data['rides_details']);die;
        $this->load->view('driver/customer_pickup',$data);
    }

    // public function complete_trip()
    // {
    //   $rides_id = $this->input->post('rides_id');
    //   $data = array(
    //       'status' => 2
    //     );
    //   $return = $this->my_custom_functions->update_data( $data, 'tbl_rides', 'rides_id = "'.$rides_id.'"');
    //   if($return){
    //       echo 'success';
    //   }else{
    //       echo 'failed';
    //   }
    // }

    public function driver_complete_trip()
    {
      $rides_id = $this->uri->segment(5);
      //echo $rides_id;
      $update = array(
          'trip_start_status' => 1
        );
      $return = $this->my_custom_functions->update_data( $update, 'tbl_rides', 'rides_id = "'.$rides_id.'"');
      $data['rides_details'] =  $this->Driver_model->get_rider_details_from_segment($rides_id);
      //echo '<pre>';print_r($data['rides_details']);die;
      $this->load->view('driver/driver_complete_trip',$data);
    }

    // public function change_trip_status()
    // {
    //   $rides_id = $this->input->post('rides_id');
    //   $data = array(
    //       'trip_start_status' => 1
    //     );
    //   $return = $this->my_custom_functions->update_data( $data, 'tbl_rides', 'rides_id = "'.$rides_id.'"');
    //   if($return){
    //       echo 'success';
    //   }else{
    //       echo 'failed';
    //   }
    // }
    public function payment_request()
    {
      $rides_id =  $this->uri->segment(5);
			$driver_id = $this->uri->segment(4);
      $update = array(
          'status' => 2
        );
      $return = $this->my_custom_functions->update_data( $update, 'tbl_rides', 'rides_id = "'.$rides_id.'"');
      $data['request_payment'] = $this->Driver_model->get_payment($driver_id, $rides_id);
      $this->load->view('driver/request_payment',$data);
    }

    function pendingTrips()
    {
      $driver_id = $this->session->userdata('driver_id');
      $data['get_rides'] = $this->Driver_model->get_pending_trips($driver_id);
      //echo "<pre>";print_r($data);die;
      $this->load->view('driver/pending_trips',$data);
    }

    function completedTrips()
    {
      $driver_id = $this->session->userdata('driver_id');
      $data['get_completed_rides'] = $this->Driver_model->get_completed_trips($driver_id);
      //echo "<pre>";print_r($data);die;
      $this->load->view('driver/completed_trips',$data);
    }

    function rejectedTrips()
    {
      $driver_id = $this->session->userdata('driver_id');
      $data['get_rejected_rides'] = $this->Driver_model->get_rejected_trips($driver_id);
      //echo "<pre>";print_r($data);die;
      $this->load->view('driver/rejected_trips',$data);
    }

    public function requestAmount()
    {
      $rides_id = $this->input->post('rides_id');
      //echo $rides_id;die;
        $data = array(
            'pay_driver' => 1
          );
        $return = $this->my_custom_functions->update_data( $data, 'tbl_rides', 'rides_id = "'.$rides_id.'"');
        if($return){
            echo 'success';
        }else{
            echo 'failed';
        }
    }
    public function update_driver_location()
    {
      $id = $this->session->userdata('driver_id');
      $lat = $this->input->post('lat');
      $lon = $this->input->post('lon');
      $data = array(
        'latitude' => $lat,
        'longitude' => $lon
        );
      $return = $this->my_custom_functions->update_data( $data, 'tbl_driver', 'id = "'.$id.'"');
      if ($return) {
        echo "success";
      }
      else {
        echo "something went wrong";
      }
    }





    /////////  -------  #END  #END  #END   #END   --------   ///////////
}
